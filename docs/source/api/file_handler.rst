Files 
=========
This section specifies all routes concerning ``files`` resources.

.. autoflask:: app:create_app('default')
    :undoc-static: 
    :modules: app.api.v1.routes.file_handler    