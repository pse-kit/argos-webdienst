Messages 
=========
This section specifies all routes concerning ``messages`` resources.

.. autoflask:: app:create_app('default')
    :undoc-static: 
    :modules: app.api.v1.routes.message_handler   