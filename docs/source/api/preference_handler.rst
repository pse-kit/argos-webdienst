Preferences
===================
This section specifies all routes that set/get preferences of users.

.. autoflask:: app:create_app('default')
    :undoc-static:
    :modules: app.api.v1.routes.preference_handler 
