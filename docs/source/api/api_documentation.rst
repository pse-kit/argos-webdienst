Argos API Documentation
==========================

.. toctree::
    :maxdepth: 2

    authentication
    endpoint_handler
    task_handler
    file_handler
    user_handler
    group_handler
    preference_handler
    message_handler
    schemas
