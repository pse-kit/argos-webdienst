Endpoints
==================
This section specifies all routes concerning ``endpoints`` resources.

.. autoflask:: app:create_app('default') 
    :undoc-static:
    :modules: app.api.v1.routes.endpoint_handler