Users
============
This section specifies all routes concerning ``users`` resource.

.. autoflask:: app:create_app('default')
    :undoc-static:
    :modules: app.api.v1.routes.user_handler 
