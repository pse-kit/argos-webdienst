Authentication
==================
This section specifies all routes concerning the authentication of users and admins.

.. autoflask:: app:create_app('default') 
    :undoc-static:
    :modules: app.api.v1.authenticator.authentication
 