.. _schemas:

Schemas
=========

.. _user-schema:

User Schema
--------------

    :user_id: ID of user.
    :name: Name of user.
    :email_address: Email of user.

.. literalinclude:: /schema/user.json
    :language: json


.. _group-schema:

Group Schema
-------------

    :group_id: ID of group.
    :name: Name of group.
    :description: Description of group. (*optional*)

.. literalinclude:: /schema/group.json
    :language: json



.. _endpoint-schema:

Endpoint Schema
----------------

Response schema
***************
    :endpoint_id: Unique ID of endpoint.
    :name: Name of endpoint.
    :instantiable_status: Boolean to indicate if endpoint is instantiable.  
    :deletable_status: Boolean to indicate if completeted tasks of this endpoint are deletable by its creator.
    :email_notification_status: Boolean to indicate if users that are assigned to review a task receive a notification email.
    :sequential_status: Boolean to indicate if endpoint is sequential (accesses have to be signed in specified order) or parallel.
    :signature_type: Signature type, valid values: ``checkbox``, ``button``, ``checkboxAfterDownload``, ``buttonAfterDownload`` (tasks with one of the last two signature types have to be downloaded by the user before they can be signed)
    :allow_additional_additional_access: Boolean to indicate if additional accesses can be specified by user.
    :additional_access_insert_before_status: Boolean to indicate if accesses specified by user at task creation are inserted before or after ``given_accesses``. (*optional*, default: ``true``)
    :given_accessses: Specifies accesses that have to sign a task of this endpoint. Sorted by ``access_order`` (in sequential mode).

        * **user** User to sign in :ref:`user-schema`
        * **group** Group to sign in :ref:`group-schema`
        * **access_order** Access order in sequential task.

    :fields: Specifies fields that can be filled in by the user at task creation.

        * **field_id** ID of field.
        * **name**: Name of field.
        * **field_type** Type of field. Either ``checkbox``, ``datetime``, ``file``, ``textfield``.
          ``datetime`` has to be in format ISO 8601 ``yyyy-mm-ddThh:mm:ss.nnnnnn+|-hh:mm``
        * **required_status** Indicates if field is required or not. (*optional*, default: ``true``)

    :active_tasks: Number of this endpoint's pending tasks.
    :description: Description of endpoint. (*optional*)

.. literalinclude:: /schema/endpoints/endpoint_dump.json
    :language: json

Request schema
***************
    The request schema contains all attributes listed above, excluding ``endpoint_id``, ``instantiable_status``, ``active_tasks``.

.. literalinclude:: /schema/endpoints/endpoint_load.json
    :language: json
  

.. _task-schema: 

Task Schema
---------------

Response schema
******************

    :task_id: Unique ID of the task.
    :task_status: Status of task (``pending``, ``withdrawn``, ``completed``, ``denied``)
    :endpoint_id: ID of endpoint.
    :name: Name of task.
    :creation_date: Creation date and time. Format ISO 8601: ``yyyy-mm-ddThh:mm:ss.nnnnnn+|-hh:mm``
    :tasks_fields: Mapping of field specified by endpoint to its value.

        * **field** Associated field.
            * **field_id** ID of associated field.
            * **name**: Name of associated field.
            * **field_type** Type of associated field. 
                Either ``checkbox``, ``datetime``, ``file``, ``textfield``. ``datetime`` has to be in format ISO 8601 ``yyyy-mm-ddThh:mm:ss.nnnnnn+|-hh:mm``

        * **value** Value of task_field.

    :creator: Creator of this task as a user object.
    :accesses: Accesses assigned to this task as objects. Given accesses from the task's endpoint are included. 
        Sorted by ``access_order`` (in sequential mode).

        * **user** User to sign in :ref:`user-schema`
        * **group** Group to sign in :ref:`group-schema`
        * **access_order** Access order in sequential task.
        * **access_status** Status of access. Either ``pending``, ``viewed``, ``downloaded``, ``signed`` or ``denied``
        * **event_date** Date of last change of ``access_status``. Format: ISO 8601 ``yyyy-mm-ddThh:mm:ss.nnnnnn+|-hh:mm``.


.. literalinclude:: /schema/tasks/task_dump.json
    :language: json

Request schema
******************
    The request schema contains all attributes listed above, excluding ``task_id``, ``task_status``, 
    ``creation_date``, ``creator``.

.. literalinclude:: /schema/tasks/task_load.json
    :language: json


.. _comment-schema:

Comment Schema
--------------------

Response schema
********************
    :comment_id: Unique ID of the comment.
    :task_id: Task this comment belongs to.
    :comment_date: Creation date. Format ISO 8601: ``yyyy-mm-ddThh:mm:ss.nnnnnn+|-hh:mm``
    :text: Comment text.
    :creator: User object of creator in :ref:`user-schema`.

.. literalinclude:: /schema/comments/comment_dump.json
    :language: json

Request schema
****************
    The request schema only contains the attribute ``text``.


.. _message-schema:

MessageSchema
--------------------

Response schema
********************
    :message_id: Unique ID of the message.
    :recipient_id: ID of user receiving the message.
    :message_date: Creation date. Format ISO 8601: ``yyyy-mm-ddThh:mm:ss.nnnnnn+|-hh:mm``
    :text: Message text.

.. literalinclude:: /schema/message.json
    :language: json



