Groups
=========
This section specifies all routes concerning ``groups`` resources.

.. autoflask:: app:create_app('default')
    :undoc-static: 
    :modules: app.api.v1.routes.group_handler    