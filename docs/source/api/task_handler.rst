Tasks
===================
This section specifies all routes concerning ``tasks`` resources. 

.. autoflask:: app:create_app('default')
    :undoc-static: 
    :modules: app.api.v1.routes.task_handler   