.. _configuration:

Configuration
==============

Environment variables
^^^^^^^^^^^^^^^^^^^^^

.. table::
    :widths: 35 65

    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``FLASK_CONFIG``               | The config object to use (development, testing or **production**), defaults to ``development``                                     |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``SECRET_KEY``                 | **Required**: Secret Key used by Flask and Flask-JWT-Extended, defaults to ``NEVER_USE_THIS_STRING_IN_PRODUCTION``                 |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LOG_TO_FILE``                | Enable logging to logs/argos.log instead of STDERR, defaults to ``False``                                                          |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``SECURE_LINK_SECRET``         | **Required**: The secret key configured in nginx.conf                                                                              |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_HOST``                  | Hostname of your LDAP Server,  defaults to ``localhost``                                                                           |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_PORT``                  | Port number of your LDAP server, defaults to ``389``                                                                               |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_USE_SSL``               | Specify whether the LDAP server connection should use SSL, defaults to ``False``                                                   |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_BASE_DN``               | **Required**: Base DN of your directory, defaults to ``''``                                                                        |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_USER_DN``               | Users DN to be prepended to the Base DN, defaults to ``ou=users``                                                                  |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_USER_RDN_ATTR``         |  The RDN attribute for your user schema on LDAP,  defaults to ``uid``                                                              |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_USER_OBJECT_FILTER``    | Specify object filter to apply when searching for users, defaults to ``(objectclass=inetOrgPerson)``                               |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_USER_LOGIN_ATTR``       | The attribute you want users to authenticate to LDAP with (e.g. ``mail``), defaults to ``uid``                                     |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_GROUP_DN``              | Groups DN to be prepended to the Base DN, defaults to ``ou=groups``                                                                |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_GROUP_MEMBERS_ATTR``    | The attribute for group members on LDAP, defaults to ``uniqueMember``                                                              |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_BIND_USER_DN``          | The Username to bind to LDAP with, defaults to ``None`` (Anonymous connection)                                                     |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``LDAP_BIND_USER_PASSWORD``    | The Password to bind to LDAP with, defaults to ``None``                                                                            |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``SYNC_USER_USERNAME_ATTR``    | The LDAP user attribute to use as username for users in DB, defaults to ``uid``                                                    |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``SYNC_USER_NAME_ATTR``        | The LDAP user attribute to use as name for users in DB, defaults to ``cn``                                                         |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``SYNC_GROUP_NAME_ATTR``       | The LDAP group attribute to use as name for groups in DB, defaults to ``cn``                                                       |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_SERVER``                | The hostname or IP address of the SMTP server, defaults to ``localhost``                                                           |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_PORT``                  | The port of the SMTP server, defaults to ``25``                                                                                    |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_USE_TLS``               | Enable STARTTLS, defaults to ``False``                                                                                             |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_USE_SSL``               | Enable SSL/TLS, defaults to ``False``                                                                                              |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_USERNAME``              | The username of the email account, defaults to ``None``                                                                            |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_PASSWORD``              | The password of the email account, defaults to ``None``                                                                            |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_SUBJECT_PREFIX``        | Specify the prefix of subjects, defaults to ``[ARGOS]``                                                                            |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAIL_DEFAULT_SENDER``        | Specify the FROM email address to use, defaults to ``Argos <argos@localhost>``                                                     |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``TASK_DETAIL_PAGE``           | Specify the link to the task detail page (task_id is appended) used in email templates, defaults to ``http://example.com/tasks/``  |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``CELERY_TIMEZONE``            | Specify the timezone for scheduled tasks, defaults to ``Europe/Berlin``                                                            |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``BROKER_URL``                 | The URL of the Celery message broker, defaults to ``redis://localhost:6379/0``                                                     |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``CELERY_RESULT_BACKEND``      | The URL of the Celery result backend, defaults to ``redis://localhost:6379/0``                                                     |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``BABEL_DEFAULT_LOCALE``       | The fallback language (used if language of user is not in ``SUPPORTED_LOCALES``), defaults to ``de``                               |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``DATABASE_URL``               | The production database URI,  defaults to ``sqlite:///data.sqlite``                                                                |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``DEV_DATABASE_URL``           | The development database URI, defaults to ``sqlite:///data-dev.sqlite``                                                            |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``TEST_DATABASE_URL``          | The unittest database URI, defaults to ``sqlite://`` (in memory)                                                                   |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+

Additional variables in config.py module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. table::
    :widths: 35 65

    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``UPLOAD_FOLDER``              | The folder to which files are uploaded to, defaults to ``uploads``                                                                 |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``DOWNLOAD_FOLDER``            | The directory in which ZIPs are stored, defaults to ``files``                                                                      |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``ALLOWED_EXTENSIONS``         | Specify the allowed file extensions, defaults to ``['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'docx', 'xlsx', 'pptx']``           |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``MAX_CONTENT_LENGTH``         | The maximum allowed request size, defaults to ``100 * 1024 * 1024`` (100M)                                                         |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``JWT_ACCESS_TOKEN_EXPIRES``   | How long an access token should live before it expires, defaults to ``datetime.timedelta(minutes=15)``                             |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``JWT_REFRESH_TOKEN_EXPIRES``  | How long a refresh token should live before it expires, defaults to ``datetime.timedelta(days=1)``                                 |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
    | ``SUPPORTED_LOCALES``          | Specify supported languages in Jinja2 email templates/event messages, defaults to ``['en', 'de']``                                 |
    +--------------------------------+------------------------------------------------------------------------------------------------------------------------------------+
