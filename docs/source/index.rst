Welcome to Argos' documentation!
=================================

.. toctree::
    :maxdepth: 2

    installation
    configuration
    cli
    api/api_documentation
    customization