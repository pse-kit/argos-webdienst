Installation
============

Prerequisites
-------------
* Debian or Ubuntu Server

* Python 3.6.X

* Database server (e.g. MariaDB, `Installation <https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-debian-9/>`_): Create a new user and grant privileges on a new database (`Tutorial <http://www.daniloaz.com/en/how-to-create-a-user-in-mysql-mariadb-and-grant-permissions-on-a-specific-database/>`_).

* LDAP server (e.g. OpenLDAP, `Setup <https://weichbrodt.me/tutorial:ldap:installopenldap/>`_): If you set up a new LDAP server, we recommend using the Object Class ``GroupOfUniqueNames`` for groups and ``inetOrgPerson`` for users. In our setup the user RDN is ``uid``.

.. figure:: _static/ldap.png
   :scale: 50 %

   Our LDAP structure

* Mail server (e.g. Postfix as a send-only server, `Installation guide <https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-as-a-send-only-smtp-server-on-debian-9/>`_, `Setting up an SPF record <https://www.digitalocean.com/community/tutorials/how-to-use-an-spf-record-to-prevent-spoofing-improve-e-mail-reliability/>`_): Your ISP has to add a rDNS-Entry, otherwise servers will decline sent mails.

.. note:: For testing purposes you can use `Google's SMTP. <https://www.digitalocean.com/community/tutorials/how-to-use-google-s-smtp-server/>`_

Setting up Argos
----------------
**ssh** into your machine::

    $ ssh user@server_ip_address

Create a new sudo user::

    $ adduser --gecos "" argos
    $ usermod -aG sudo argos
    $ su argos

Download the source code from our **GitLab** repository::

    $ git clone https://gitlab.com/pse-kit/argos-webdienst.git
    $ cd argos-webdienst/argos-flask/

Create a new virtual environment and install all the Python packages::

    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install wheel
    $ pip install -r requirements.txt

Installing base dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`Install Redis <https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-debian-9/>`_, the message broker for Celery, and change the ``supervised`` directive from ``no`` to ``systemd``. In this way the systemd init system can manage Redis as a service::

    $ sudo apt install redis-server
    $ sudo nano /etc/redis/redis.conf

`Setup a firewall with ufw <https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-debian-9/>`_::

    $ sudo apt install ufw
    $ sudo ufw default deny incoming
    $ sudo ufw default allow outgoing
    $ sudo ufw allow ssh
    $ sudo ufw enable
    $ sudo ufw status verbose

It is recommended to run Flask applications without direct access and have a fast web server (nginx) that accepts all requests from clients (reverse proxy) instead.
nginx will serve static files directly, but forwards requests for the application to the internal server (Gunicorn). `Install nginx <https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04/>`_::

    $ sudo apt install nginx nginx-extras

Increase ``client_max_body_size`` to ``100M`` in nginx.conf (create directive if it does not exist)::

    $ sudo nano /etc/nginx/nginx.conf

`nginx configuration overview <https://www.linode.com/docs/web-servers/nginx/how-to-configure-nginx/>`_:

* ``/etc/nginx/sites-available/``: All server block configuration files are located in this directory. nginx will use these configuration files only if they are linked to the sites-enabled directory.

* ``/etc/nginx/sites-enabled/``: The directory where enabled server blocks are stored.

Remove the default nginx configuration::

    $ sudo rm /etc/nginx/sites-enabled/default

Create a nginx config file for Argos:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. Generate a secret key for the secure link module::

    $ openssl rand -base64 12

#. Create the config file::

    $ sudo nano /etc/nginx/sites-available/argos

#. Replace ``YourSecret`` with the generated key and ``api.example.com`` with your domain::

    server {
        # listen on port 80 (http)
        listen 80;
        server_name api.example.com;

        # write access and error logs to /var/log/nginx
        access_log /var/log/nginx/argos_access.log;
        error_log /var/log/nginx/argos_error.log;

        location / {
            # forward application requests to the gunicorn server
            proxy_pass http://127.0.0.1:8000;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }

        location /files {
        # handle zips directly
            alias /home/argos/argos-webdienst/argos-flask/files;

            # set connection secure link
            secure_link $arg_st,$arg_e;
            secure_link_md5 "YourSecret$uri$secure_link_expires";

            # bad hash
            if ($secure_link = "") {
                return 403;
            }

            # link expired
            if ($secure_link = "0") {
                return 410;
            }
        }
    }

   .. note::
    It is also possible to bind to a `Unix socket <https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-16-04/>`_, but TCP is more flexible.

#. Now it is time to enable the file by creating a link to the ``sites-enabled`` directory. First, make sure that your file contains no syntax errors::

    $ sudo nginx -t
    $ sudo ln -s /etc/nginx/sites-available/argos /etc/nginx/sites-enabled/
    $ sudo ufw allow 'Nginx HTTP'
    $ sudo systemctl reload nginx

#. Our server runs behind a F5 Load Balancer that enforces ``https`` and manages the TLS certificates. If your server has to take care of this, follow the steps below to `obtain Let’s Encrypt TLS certificates <https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-18-04/>`_:

  .. note::
     If you plan to access Argos in your local network only, follow `this guide <https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-on-debian-9/>`_ instead.

* Add the repository and allow ``https`` through the firewall::

    $ sudo add-apt-repository ppa:certbot/certbot
    $ sudo apt install python-certbot-nginx
    $ sudo ufw allow 'Nginx Full'

* Setup an **A record** for api.example.com pointing to your server's public IP address. Run ``certbot``::

    $ sudo certbot --authenticator standalone --installer nginx -d api.example.com --pre-hook "systemctl stop nginx" --post-hook "systemctl start nginx"

Setup Gunicorn and the Flask app:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. Install the Gunicorn webserver in your enabled ``venv``::

    $ pip install gunicorn

#. If you are using a MySQL Server, install the PyMySQL library for connecting to the database::

    $ pip install pymysql

#. Create a ``.env`` file and set the environment variables for your setup (see :ref:`configuration`)::

    $ nano .env

   Sample .env file (LDAP and db servers are running on another machine):

   .. code-block:: python

    # Use the production config
    FLASK_CONFIG=production

    # Secret Key used by Flask and Flask-JWT-Extended
    # How to generate good secret keys: python -c 'import os; print(os.urandom(16))'
    SECRET_KEY=generate_a_secret_key

    # Enable logging to logs/argos.log instead of STDERR
    LOG_TO_FILE=True

    # The secret key configured in nginx.conf
    SECURE_LINK_SECRET=YourSecret

    # https://docs.sqlalchemy.org/en/latest/core/engines.html
    # dialect+driver://username:password@host:port/database
    DATABASE_URL=mysql+pymysql://argos:password@192.255.10.10/argos

    # Hostname of your LDAP Server
    LDAP_HOST=192.255.10.10

    # Base DN of your directory
    LDAP_BASE_DN=dc=example,dc=com

    # The Username to bind to LDAP with
    LDAP_BIND_USER_DN=cn=admin,dc=example,dc=com

    # The Password to bind to LDAP with
    LDAP_BIND_USER_PASSWORD=password

    # Specify the FROM email address to use
    MAIL_DEFAULT_SENDER ="Argos <no_reply@example.com>"

    # Specify the link to the task detail page (task_id is appended) used in email templates
    TASK_DETAIL_PAGE=https://example.com/tasks/

#. Set the ``FLASK_APP`` variable to enable the ``flask`` commands to work::

    $ echo "export FLASK_APP=argos.py" >> ~/.profile

#. Compile the translations, initialize the database and add an admin (for backend login)::

    $ flask translate compile
    $ flask db init
    $ flask db migrate
    $ flask db upgrade
    $ flask add_admin John Doe test@example.com username password

#. Run Gunicorn with a worker count of ``(2 Workers * CPU Cores) + 1``::

    $ gunicorn -b 127.0.0.1:8000 --workers=9 argos:app --access-logfile '-'

#. From your local machine, check if nginx and Gunicorn are set up properly::

    $ curl --include -X OPTIONS https://api.your.domain/api/v1/login

   It should output something like::

    Server: nginx/1.10.3
    Date: Wed, 30 Jan 2019 13:36:03 GMT
    Content-Type: text/html; charset=utf-8
    Content-Length: 0
    Connection: keep-alive
    Allow: POST, OPTIONS
    Access-Control-Allow-Origin: *

#. Next, verify that ``Celery`` works, and thus the Synchronizer. Run a worker::

    $ celery -A celery_worker:celery worker --loglevel=DEBUG

   Open another terminal session with your venv activated and run ``celery beat`` for periodic tasks (schedules the Synchronizer every 15 min.)::

    $ celery -A celery_worker:celery beat --loglevel=DEBUG

   Check the debug output to verify that all necessary LDAP variables were set in ``.env`` for the Synchronizer.

Setup Supervisor:
^^^^^^^^^^^^^^^^^^
Because it is not practical to open all the terminal windows after each server restart, you should use the `supervisor <http://supervisord.org/>`_ tool.

* Install the supervisor package::

    $ sudo apt-get install supervisor

* Create a config file::

    $ nano /etc/supervisor/conf.d/argos.conf

  Add the following::

    [program:gunicorn]
    command=/home/argos/argos-webdienst/argos-flask/venv/bin/gunicorn -b 127.0.0.1:8000 --workers=9 argos:app
    directory=/home/argos/argos-webdienst/argos-flask/
    user=argos
    autostart=true
    autorestart=true
    stopasgroup=true
    killasgroup=true
    stderr_logfile=/var/log/gunicorn.err.log
    stdout_logfile=/var/log/gunicorn.out.log

    [program:celery-worker]
    command=/home/argos/argos-webdienst/argos-flask/venv/bin/celery -A celery_worker:celery worker --loglevel=INFO
    directory=/home/argos/argos-webdienst/argos-flask/
    user=argos
    autostart=true
    autorestart=true
    startsecs=10
    stopwaitsecs=60
    stopasgroup=true
    stdout_logfile=/var/log/celery_worker.out.log
    stderr_logfile=/var/log/celery_worker.err.log

    [program:celery-beat]
    command=/home/argos/argos-webdienst/argos-flask/venv/bin/celery -A celery_worker:celery beat --loglevel=INFO
    directory=/home/argos/argos-webdienst/argos-flask
    user=argos
    autostart=true
    autorestart=true
    startsecs=10
    stopasgroup=true
    stdout_logfile=/var/log/celery_beat.out.log
    stderr_logfile=/var/log/celery_beat.err.log


* Reload the supervisor::

    $ sudo supervisorctl reread
    $ sudo service supervisor restart
