Command Line Interface
=======================

In an activated ``virtualenv`` the following commands are available:

Add an admin
^^^^^^^^^^^^^
.. code-block:: console

    $ flask add_admin FIRST_NAME LAST_NAME EMAIL_ADDRESS USERNAME

Clear expired tokens in database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. code-block:: console

    $ flask clear_expired_tokens