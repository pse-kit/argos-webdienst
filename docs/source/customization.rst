Customization
=========================

Adding a language
^^^^^^^^^^^^^^^^^^^^^^^^^^

Currently, ARGOS supports the languages English and German (which is the default) for messages and mails. 
Add translation support for a new language by using the translate commands provided by ARGOS.

Extract strings to translate and create the new translation file ``app/translations/xy/LC_MESSAGES/messages.po``::
    
    $ flask translate init xy

Replace ``xy`` with the code of the language you want to add.

Edit the translation file (e.g. with `poedit <https://poedit.net/>`_) and compile your translation::

    $ flask translate compile

You can find the list ``SUPPORTED_LOCALES``, which contains all supported languages, in the file ``config.py``. 
Append the list with the new language code::
    
    # add the new language code to this list
    SUPPORTED_LOCALES = ['en', 'de']


Creating custom messages
^^^^^^^^^^^^^^^^^^^^^^^^^^

Messages for all basic events are defined in the ``helper`` module. Add your own method for a customized message and call it whenever
you need a user to receive such a message. 
Use the command ``gettext`` of ``pybabel`` for translation support::
    
    def create_custom_message(recipient):
        # set the correct user for g object
        g.user = recipient
        # translate message to the recipient's language
        message_text = gettext('custom text')
        # create a Message object and save to the database
        Message(recipient=recipient, text=message_text).save()
        refresh()  # reset current locale

Make sure to update the translation file(s) for the new message text by using the translate commands provided by ARGOS::

    $ flask translate update

Compile the translation file(s) after editing::

    $ flask translate compile


Modifying the response JSON
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For certain requests you may need to modify the JSON response by adding/excluding attributes to/from the JSON.

The decision which properties of a requested object are sent to the client is made when instantiating the schema. 
This usually happens at the beginning of a handler module, where the needed schemas are globally defined
for usage in the routes. Define a modified schema here and use the keyword ``exclude`` to prevent an attribute from being dumped::

    # examples of global (modified) task and comment schemas to use in routes of the task_handler
    task_schema_user = TaskSchema()
    task_schema_admin = TaskSchema(exclude=('tasks_fields', 'accesses'))
    comment_schema = CommentSchema()


See the `marshmallow <https://marshmallow.readthedocs.io>`_ documentation for further information. 

If you want to add an attribute which is not covered by the object's schema, add the attributes name and its value as a 
key-value-pair to the dumped schema (which is a simple dictionary).


Adding a signature type
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The possible values of the signature type of a task are listed in the ``Endpoint`` model. 
Append this list with the name of the new signature type::

    # add the new signature type to this list
    signature_type_values = ['checkbox', 'button', 'checkboxAfterDownload', 'buttonAfterDownload']

Now, requests for creating an endpoint with this signature type will not raise an error anymore. 

Check if the signing conditions for a task with the new signature type are met by extending the method ``can_sign`` in the ``User`` model::
        
    def can_sign(self, task, accesses):
        # no current accesses involving the requesting user
        if not accesses:
            return False

        # check conditions of specified signature_type
        if task.endpoint.signature_type in ['checkboxAfterDownload', 'buttonAfterDownload']:
            # ...
        # add your customized condition here
        elif task.endpoint.signature_type == 'new_signature_type':
            # ...
        else:
            return True

You can return a boolean to indicate whether a task can be signed or not by the requesting user. However, you may want to specify 
the cause why a user might not sign the task just yet. For this purpose you can return any string you like.


Adding a field type
^^^^^^^^^^^^^^^^^^^^^^^^^^

All possible values for field types are defined in the list ``field_type_values`` which can be found in the ``Field`` model. Simply extend 
this list with the customized field type::

    # add the new field type to this list
    field_type_values = ['textfield', 'datetime', 'checkbox', 'file']


Need to validate any values given by the creator of a task for this field type? Modify the method ``validate_schema`` of the 
``TaskFieldSchema`` by adding the new case and the validation you want to perform::

    @validates_schema
    def validate_schema(self, data):
        field = Field.query.get(data['field_id'])
        # validate values for different field types
        if field.field_type == 'checkbox':
            # ...
        # add your customized validation here
        elif field.field_type == 'new_field_type':
            # ...

Raise a marshmallow ``ValidationError`` if the given value of the task_field stored in ``data['value']`` is invalid.


Disable email notifications
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Add ``MAIL_SUPPRESS_SEND = True`` to the Config Base class in ``config.py``.