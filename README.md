# Argos - Flask
"Webservice zur Definition und Durchsetzung des Mehr-Augen-Prinzips", PSE-Projekt im WS2018/19 von Julius Häcker, Moritz Leitner, Nicolas Schuler, Noah Wahl und Wendy Yi

## Docs
[View the documentation online](https://pse-kit.gitlab.io/argos-webdienst/) or build them from the sphinx sources in the docs folder:
```console
$ cd argos-flask/
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ pip install Sphinx
$ pip install sphinxcontrib-httpdomain
$ cd ../docs/
$ make html
```

## Run the tests
```console
$ cd argos-flask/
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ export FLASK_APP="argos.py"
$ flask test --coverage
```

## Quickstart
**Warning:**
You should not use the built-in development server `flask run` for hosting Argos, see [Installation](https://pse-kit.gitlab.io/argos-webdienst/installation.html) for production deployment instead!
```console
$ cd argos-flask/
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ export FLASK_APP="argos.py"
```

#### Create a .env file and set environment variables as needed ([see Configuration](https://pse-kit.gitlab.io/argos-webdienst/configuration.html))
```console
$ nano .env
```

#### Run the Flask application
```console
$ flask run
```

#### Run redis (docker)
```console
$ docker run -d --name redis -p 6379:6379 redis
```

#### Run a celery worker
```console
$ celery -A celery_worker:celery worker --loglevel=INFO
```

#### Run celery beat for periodic tasks
```console
$ celery -A celery_worker:celery beat --loglevel=INFO
```
