"""Creates the api blueprint."""

from flask import Blueprint

api = Blueprint('api', __name__)

# import blueprint modules - avoid circular dependencies
from app.api.v1 import errors  # noqa: F401
from app.api.v1.authenticator import authentication  # noqa: F401
from app.api.v1.routes import (user_handler, group_handler, endpoint_handler,  # noqa: F401
                               message_handler, preference_handler, task_handler,  # noqa: F401
                               file_handler)  # noqa: F401
