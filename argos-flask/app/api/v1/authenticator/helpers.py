"""Contains helper functions for authentication such as custom decorators for Flask-JWT-Extended."""

from datetime import datetime
from functools import wraps

from flask import current_app
from flask_jwt_extended import decode_token, verify_jwt_in_request, get_jwt_claims
from sqlalchemy.orm.exc import NoResultFound

from app import jwt_manager
from app.api.v1.errors import Forbidden, InternalServerError
from app.models.models import Token


class Requestor:
    """This class represents a requestor."""

    def __init__(self, id, role):
        """Initialize a requestor object.

        Args:
            id (int): ID of requestor (user_id or admin_id).
            role (str): Role of requestor (user or admin).

        """
        self.id = id
        self.role = role


def add_user_token_to_database(encoded_token):
    """Add encoded JWT from user to tokens table.

    Args:
        encoded_token (str): JSON Web Token.

    """
    decoded_token = decode_token(encoded_token)
    jti = decoded_token['jti']
    token_type = decoded_token['type']
    user_id = decoded_token[current_app.config['JWT_IDENTITY_CLAIM']]
    expiration_date = datetime.fromtimestamp(decoded_token['exp'])
    token = Token(jti=jti, token_type=token_type, user_id=user_id,
                  revoked_status=False, expiration_date=expiration_date)
    token.save()


def add_admin_token_to_database(encoded_token):
    """Add encoded JWT from admin to tokens table.

    Args:
        encoded_token (str): JSON Web Token.

    """
    decoded_token = decode_token(encoded_token)
    jti = decoded_token['jti']
    token_type = decoded_token['type']
    admin_id = decoded_token[current_app.config['JWT_IDENTITY_CLAIM']]
    expiration_date = datetime.fromtimestamp(decoded_token['exp'])
    token = Token(jti=jti, token_type=token_type, admin_id=admin_id,
                  revoked_status=False, expiration_date=expiration_date)
    token.save()


def revoke_token(jti):
    """Revoke the token with given jti in tokens table.

    Args:
        jti (str): JSON Web Token Identifier.

    Raises:
        InternalServerError: If no token was found with given jti.

    """
    try:
        token = Token.query.filter_by(jti=jti).one()
        token.revoked_status = True
        token.save()
    except NoResultFound:
        raise InternalServerError('Error revoking the token')


@jwt_manager.token_in_blacklist_loader
def check_if_token_revoked(decoded_token):
    """Check if a token has been revoked or not. Callback function for Flask-JWT-Extended.

    Args:
        decoded_token (str): Decoded JSON Web Token.

    Returns:
        bool: Indicates whether token was revoked. True -> revoked token, False -> valid token.

    """
    jti = decoded_token['jti']
    try:
        token = Token.query.filter_by(jti=jti).one()
        return token.revoked_status
    except NoResultFound:
        return True


@jwt_manager.user_claims_loader
def add_claims_to_access_token(requestor):
    """Add custom claims to the access token. Allows distinction of user and admin tokens.

    Will be called whenever create_access_token is used.

    Args:
        role (str): Role of requestor (user or admin).

    Returns:
        dict: Claims to be added to the access token.

    """
    return {'id': requestor.id, 'role': requestor.role}


@jwt_manager.user_identity_loader
def user_identity_lookup(requestor):
    """Define what the identity of the access token should be.

    Will be called whenever create_access_token is used.

    Args:
        requestor (Requestor): Token requestor.

    Returns:
        int: Identity of the access token.

    """
    return requestor.id


def admin_required(fn):
    """Admin required decorator.

    Custom decorator that verifies the JWT is present in the request, as well as insuring that
    this requestor has a role of admin in the access token.

    Args:
        fn (function): Function to be decorated.

    Raises:
        Forbidden: If requestor is not an admin.

    Returns:
        function: Decorated function.

    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if claims['role'] != 'admin':
            raise Forbidden('Admins only!')
        else:
            return fn(*args, **kwargs)

    return wrapper


def user_required(fn):
    """User required decorator.

    Custom decorator that verifies the JWT is present in the request, as well as insuring that
    this requestor has a role of user in the access token.

    Args:
        fn (function): Function to be decorated.

    Raises:
        Forbidden: If requestor is not a user.

    Returns:
        function: Decorated function.

    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if claims['role'] != 'user':
            raise Forbidden('Users only!')
        else:
            return fn(*args, **kwargs)

    return wrapper
