"""This module contains the authentication routes."""

from flask import request, jsonify
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required,
                                jwt_refresh_token_required, get_raw_jwt, get_jwt_claims)
from flask_ldap3_login import AuthenticationResponseStatus

from app import ldap3_manager
from app.api.v1 import api
from app.api.v1.authenticator.helpers import (Requestor, add_user_token_to_database,
                                              add_admin_token_to_database, revoke_token)
from app.api.v1.errors import BadRequest, Unauthorized, InternalServerError
from app.models.models import User, Admin


@api.route('/login', methods=['POST'])
def login():
    """Authenticate the user or admin using the provided username and password.

    :<json string username: Username.
    :<json string password: Password.

    :>json string access_token: New access token.
    :>json string refresh_token: New refresh token.

    :status 201: Token have been created successfully.
    :status 400: Missing JSON data.
    :status 401: Invalid username or password.
    :status 500: LDAP-Authentication successful, but user was not found in database.
        Run the Synchronizer to sync LDAP with database.

    """
    # get role of requestor
    role = request.args.get('role', default='user', type=str)

    if request.json is None:
        raise BadRequest('Missing JSON data')
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if username is None or password is None:
        raise Unauthorized('Username or password field empty')

    if role == 'user':
        # verify password
        ldap_response = ldap3_manager.authenticate(username, password)
        if ldap_response.status is not AuthenticationResponseStatus.success:
            raise Unauthorized('Invalid username or password')

        # get current user in db
        user = User.query.filter_by(username=username).first()
        if user is None:
            raise InternalServerError('LDAP-Authentication successful, but user was not found in '
                                      'database. Run the Synchronizer to sync LDAP with database.')

        # create JWTs and add them to db
        requestor = Requestor(user.user_id, 'user')
        access_token = create_access_token(identity=requestor)
        refresh_token = create_refresh_token(identity=requestor)
        add_user_token_to_database(access_token)
        add_user_token_to_database(refresh_token)

    elif role == 'admin':
        # get current admin in db and verify passowrd
        admin = Admin.query.filter_by(username=username).first()
        if admin is None or not admin.verify_password(password):
            raise Unauthorized('Invalid username or password')

        # create JWTs and add them to db
        requestor = Requestor(admin.admin_id, 'admin')
        access_token = create_access_token(identity=requestor)
        refresh_token = create_refresh_token(identity=requestor)
        add_admin_token_to_database(access_token)
        add_admin_token_to_database(refresh_token)

    else:
        raise BadRequest('Invalid query string')

    response = {
        'access_token': access_token,
        'refresh_token': refresh_token
    }
    return jsonify(response), 201


@api.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    """Create a new access token.

    :reqheader Authorization: Required JWT (refresh token).

    :>json string access_token: New access token.

    :status 201: Access token has been created successfully.

    """
    # get claims from refresh token
    claims = get_jwt_claims()
    requestor = Requestor(claims['id'], claims['role'])

    # create new access token and save it in db
    access_token = create_access_token(identity=requestor)
    if (requestor.role == 'user'):
        add_user_token_to_database(access_token)
    else:
        add_admin_token_to_database(access_token)

    response = {
        'access_token': access_token,
    }
    return jsonify(response), 201


@api.route('/logout/access', methods=['DELETE'])
@jwt_required
def logout_access():
    """Invalidate the provided access token.

    :reqheader Authorization: Required JWT.

    :>json string msg: 'Successfully logged out' if deleting access token has been successful.
    :status 200: No errors.

    """
    jti = get_raw_jwt()['jti']
    revoke_token(jti)
    return jsonify({"msg": "Successfully logged out"}), 200


@api.route('/logout/refresh', methods=['DELETE'])
@jwt_refresh_token_required
def logout_refresh():
    """Invalidate the provided refresh token.

    :reqheader Authorization: Required JWT (refresh token).

    :>json string msg: 'Successfully logged out' if deleting refresh token has been successful.
    :status 200: No errors.

    """
    jti = get_raw_jwt()['jti']
    revoke_token(jti)
    return jsonify({"msg": "Successfully logged out"}), 200
