"""This module contains the error handlers."""

from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES

from app.api.v1 import api


class APIError(Exception):
    """Custom base class for API errors.

    Args:
        Exception (Exception): Base class.

    """

    def __init__(self, message, status_code=500):
        """Initialize the API error.

        Args:
            message (str): Error message.
            status_code (int, optional): HTTP response status code, defaults to 500.

        """
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code

    def to_dict(self):
        """Create dictionary representation of the error.

        Returns:
            dict: Dictionary representation of the error.

        """
        dict = {'error': HTTP_STATUS_CODES.get(self.status_code, 'Unknown error'),
                'msg': self.message}
        return dict


class BadRequest(APIError):
    """Custom bad request error class.

    Args:
        APIError (APIError): Base class.
    """

    def __init__(self, message):
        """Initialize the bad request error.

        Args:
            message (str): Error message.
        """
        super().__init__(message, 400)


class Unauthorized(APIError):
    """Custom unauthorized error class.

    Args:
        APIError (APIError): Base class.
    """

    def __init__(self, message):
        """Initialize the unauthorized error.

        Args:
            message (str): Error message.
        """
        super().__init__(message, 401)


class Forbidden(APIError):
    """Custom forbidden error class.

    Args:
        APIError (APIError): Base class.
    """

    def __init__(self, message):
        """Initialize the forbidden error.

        Args:
            message (str): Error message.
        """
        super().__init__(message, 403)


class InternalServerError(APIError):
    """Custom unauthorized error class.

    Args:
        APIError (APIError): Base class.
    """

    def __init__(self, message):
        """Initialize the unauthorized error.

        Args:
            message (str): Error message.
        """
        super().__init__(message, 500)


@api.errorhandler(APIError)
def handle_api_error(error):
    """Catch API errors, serialize into JSON, and respond with appropriate status code.

    Args:
        error (APIError): API error to be handled.

    Returns:
        flask.Response: Flask response object.

    """
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
