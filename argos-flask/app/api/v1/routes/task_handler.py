"""This module contains the task_handler routes."""

import json

from flask import abort, jsonify, request, url_for
from flask_jwt_extended import get_jwt_claims, jwt_required
from sqlalchemy import and_, or_

from app.api.v1 import api
from app.api.v1.authenticator.helpers import user_required
from app.api.v1.errors import BadRequest, Forbidden
from app.api.v1.routes.file_handler import handle_files, save_files_as_zip
from app.api.v1.routes.helper import (add_prev_next, paginate, create_message_task_event,
                                      create_message_task_event_by,
                                      create_message_task_tosign_parallel,
                                      create_message_task_tosign_sequential)
from app.mailer.email import (send_task_creation_email, send_task_completed_email,
                              send_task_denied_email, send_task_commented_email,
                              send_task_sequential_notification_email,
                              send_task_parallel_notification_email)
from app.models.models import Access, Task, User, Endpoint
from app.models.schemas import TaskSchema, CommentSchema

# global task and comment schemas to use in routes
task_schema_user = TaskSchema()
task_schema_admin = TaskSchema(exclude=('tasks_fields', 'accesses'))
comment_schema = CommentSchema()


@api.route('/tasks/<int:task_id>', methods=['GET'])
@jwt_required
def get_task(task_id):
    """Get task by ID.

    :reqheader Authorization: Required JWT.

    :>json task: Task with given ``task_id`` in :ref:`task-schema`.
    :>json string can_sign: Indicates if requesting user is allowed to sign requested task.
        Either ``True``, ``False`` or ``afterDownload``.
    :>json string signature_type: ``signature_type`` of endpoint of requested task.
    :>json boolean sequential_status: ``sequential_status`` of endpoint of requested task.

    .. note:: For a requesting admin, the attributes ``tasks_fields``, ``accesses``, ``can_sign`,
        ``signature_type`` and ``sequential_status`` are omitted.

    :status 200: No error.
    :status 401: Requester is not logged in.
    :status 403: (If requester is a user) Task with given ``task_id`` is deleted, does not
                 exist or user is not allowed to view the task. Only the creator of the task,
                 users who had been assigned to review the task or who are allowed to sign now can
                 get the task.
    :status 404: (If requester is an admin) Task with given ``task_id`` is deleted or does not
                 exist.

    **Example response JSON (user requesting):**

    .. literalinclude:: /schema/tasks/task_get_task.json
        :language: json

    """
    claims = get_jwt_claims()
    schema = get_task_schema_for_role(claims['role'])
    task = get_task_if_valid(claims, task_id)
    response = schema.dump(task)

    if claims['role'] == 'user':
        user = User.query.get(claims['id'])
        user.view_task(task)
        # add attributes
        (accesses, can_sign) = user.get_accesses_to_sign(task)
        response['can_sign'] = str(can_sign)
        response['signature_type'] = task.endpoint.signature_type
        response['sequential_status'] = task.endpoint.sequential_status

    return jsonify(response), 200


@api.route('/tasks/', methods=['GET'])
@jwt_required
def get_tasks_by_type():
    """Get tasks of given type or all pending tasks if an admin is requester.

    :query string type: Type of tasks to get (default: ``OWNED``)

                 * ``OWNED``: All pending tasks created by requesting user.
                 * ``TOSIGN``: All tasks the requesting user is assigned to review now.
                 * ``ARCHIVED``: All completed/denied tasks created by requesting user and
                                 all tasks that the user (or another member of an assigned group)
                                 had signed or denied.

    .. note:: ``type`` is ignored if requester is an admin.

    :query int page: Requested page of tasks. Defaults to 1 if no such query parameter or if given
                 parameter is not an integer greater than 1.
    :query int limit: Requested number of tasks. Defaults to 20 if no such query parameter or if
                  given parameter is not an integer greater than 0.

    :reqheader Authorization: Required JWT.
    :>json tasks: Tasks in :ref:`task-schema`

                  * *Admin*: All pending tasks. Attributes ``tasks_fields`` and ``accesses``
                    are omitted.
                  * *User*: All tasks of given type.
                  * **Note**: Might be empty if no tasks of given type or if page number is
                    higher than total amount of pages.

    :>json string previous: URL to previous page. ``null``, if on first page.
    :>json string next: URL to next page. ``null``, if on last page.

    :status 200: No error.
    :status 400: Given type is ``OWNED``, ``TOSIGN`` or ``ARCHIVED``.
    :status 401: Requester is not logged in.

    """
    claims = get_jwt_claims()

    # if requestor is admin: all active tasks
    if claims['role'] == 'admin':
        query = Task.query.filter_by(task_status='pending')

    else:
        task_type = request.args.get('type', 'OWNED', type=str)
        user = User.query.get(claims['id'])
        if task_type == 'OWNED':
            query = user.get_query_tasks_owned()
        elif task_type == 'TOSIGN':
            query = user.get_query_tasks_to_sign()
        elif task_type == 'ARCHIVED':
            query = user.get_query_tasks_archived()
        else:
            raise BadRequest('Not a valid task type.')

    # paginate query
    pagination = paginate(request, query.order_by(Task.creation_date.desc()))
    response_data = get_task_schema_for_role(claims['role']).dump(pagination.items, many=True)

    # add deletable attribute if requested task type is archived
    if claims['role'] == 'user' and task_type == 'ARCHIVED':
        response_data = add_deletable(response_data, user)

    # add urls for previous and next page
    if claims['role'] == 'user':
        response_data = add_prev_next('api.get_tasks_by_type', {'tasks': response_data}, pagination,
                                      type=task_type)
    else:
        response_data = add_prev_next('api.get_tasks_by_type', {'tasks': response_data}, pagination)
    return jsonify(response_data), 200


@api.route('/tasks/', methods=['POST'])
@user_required
def create_task():
    """Create new task.

    Send mail and message to the user if creating the task has been successful.

    .. warning:: Files must be transferred with the Content-Type ``multipart/form-data`` and the
        key ``file`` in the request body. Since a POST-Request can not have mutliple content-types,
        the JSON object must be inserted as a .json file with the ``json`` key in the form data
        section if files belong to the task.

    :reqheader Authorization: Required JWT of a user.
    :resheader Content-Type: ``multipart/form-data`` if files are included otherwise
        ``application/json`` is possible.
    :resheader Location: URL of created task.

    :<json task: Data for creating a new task in :ref:`task-schema`.
    :form json: Data for creating a new task in :ref:`task-schema` as .json.
    :form file: Files belonging to this task.
    :>json task: Created task in :ref:`task-schema`.

    :statuscode 201: Task has been created successfully.
    :status 400: * JSON attributes are not as expected (type, name, amount).
                 * With given IDs referenced objects do not exist (``endpoint_id`` or ``user_id``,
                   ``group_id`` in ``given_accesses`` or ``field_id`` in ``tasks_fields``).
                 * Endpoint with ``endpoint_id`` is not instantiable.
                 * ``given_accesses`` of task's endpoint and ``accesses`` are both empty
                 * additional accesses are not allowed and ``accesses`` is not empty
                 * ``sequential_status`` is true and access orders of ``accesses`` are not
                   specified or not integers from 0 to n.
                 * User ID and group ID in accesses of ``accesses`` are both or both not set.
                   (One of them has to be ``null`` or not appear in the request JSON at all.)
                 * Requesting user adds himself or a group with only himself as member as a signee
                 * ``field_id`` in ``tasks_fields`` does not belong to a field of the task's
                   endpoint.
                 * Regarding files:

                    * Value of ``tasks_fields`` of type ``file`` is defined.
                    * Required fields are not specified.
                    * Files are uploaded when it is not allowed.
                    * No files are uploaded when it is required.
                    * File extensions are not valid.

    :status 401: Requester is not a logged-in user.

    .. note:: * If ``sequential_status`` is false, ``access_order`` of ``accesses`` are
                ignored. (``access_order`` are not validated).
              * Empty lists or attributes with value ``null`` are ignored.
              * ``tasks_fields`` with value ``null`` are ignored.

    **Example request JSON:**

    .. literalinclude:: /schema/tasks/task_load.json
        :language: json

    """
    claims = get_jwt_claims()
    # get JSON data
    if 'json' in request.files:
        json_data = json.load(request.files['json'])
    elif request.json:
        json_data = request.json
    else:
        raise BadRequest('Missing JSON')

    # set creator of task and load task
    json_data['creator_id'] = claims['id']
    task = task_schema_user.load(json_data)

    # get list of files in this request
    uploaded_files = request.files.getlist("file")

    # get field with type file (if any)
    field_file = Endpoint.query.get(task.endpoint_id).fields.filter_by(field_type='file').first()

    # check if file upload is allowed/required and if uploaded files meet the requirements
    if field_file is not None and field_file.required_status and len(uploaded_files) == 0:
        raise BadRequest('Missing required file.')
    elif not field_file and len(uploaded_files) != 0:
        raise BadRequest('No file upload allowed.')
    else:
        # handle uploaded files
        handle_files(task, uploaded_files, field_file)
        task.save()

        if len(uploaded_files) > 0:
            # create ZIP asynchronously with celery
            save_files_as_zip.delay(task.task_id)

    response = task_schema_user.jsonify(task)

    # send confirmation email
    send_task_creation_email.delay(task.task_id)
    # create confirmation message
    create_message_task_event(task, 'created')

    if task.endpoint.email_notification_status is True:
        # send notification email
        if task.endpoint.sequential_status is True:
            # send to first access
            first_access = task.accesses.filter_by(access_status='pending') \
                .order_by(Access.access_order).first()
            send_task_sequential_notification_email.delay(first_access.access_id)
        else:
            send_task_parallel_notification_email.delay(task.task_id)

    # create notification messages
    if task.endpoint.sequential_status is True:
        create_message_task_tosign_sequential(task)
    else:
        create_message_task_tosign_parallel(task)

    # add url to created task to response
    url = url_for('api.get_task', task_id=task.task_id)
    # location has to contain a relative URL
    response.autocorrect_location_header = False
    return response, 201, {'Location': url}


@api.route('/tasks/<int:task_id>', methods=['DELETE'])
@user_required
def delete_task(task_id):
    """Delete a task with given task_id.

    Send message to the user if deleting the task has been successful.

    Pending and denied tasks can always be deleted.
    A completed task can only be deleted if the endpoint allows it.

    :reqheader Authorization: Required JWT of a user.

    :>json success: ``true`` if task has been deleted successfully.

    :statuscode 200: No error.
    :status 400: Task is not deletable.
    :status 401: Requester is not a logged-in user.
    :status 403: Requesting user is not the creator of the task.
    :status 404: Task with given ``task_id`` is already deleted.

    """
    claims = get_jwt_claims()
    user_id = claims['id']
    task = Task.query.get(task_id)

    # no task with given id or requesting user is not the creator
    if task is None or task.creator_id != user_id:
        raise Forbidden('Access denied.')
    # task does not exist if it is withdrawn
    if task.task_status == 'withdrawn':
        abort(404)

    # set pending or denied task to withdrawn
    if task.task_status == 'pending':
        task.task_status = 'withdrawn'
        # create message that task has been wthdrawn
        create_message_task_event(task, 'withdrawn')
    elif task.task_status == 'denied':
        task.task_status = 'withdrawn'
        # create message that task has been deleted
        create_message_task_event(task, 'deleted')

    # completed task can only be deleted if endpoint allows it
    else:
        if not task.endpoint.deletable_status:
            raise BadRequest('Task cannot be deleted.')
        task.task_status = 'withdrawn'
        create_message_task_event(task, 'deleted')
    task.save()
    response = jsonify(success=True)
    return response, 200


@api.route('/tasks/<int:task_id>/status', methods=['PUT'])
@user_required
def update_task(task_id):
    """Update status of a task with given ``task_id``.

    * Send message to creator of task if task is signed or denied. (Only if signee is not the
        creator himself).
    * Send email and message to creator of task if task is completed or denied.
    * Send email (if ``email_notification_status`` of concerned users is ``true``) and message to
      users that have to sign next (if task sequential).

    :reqheader Authorization: Required JWT of a user.
    :<json event: Event that triggers updating.

                  * ``signed``: Requesting user signs the task. If all accesses are signed, the
                                task is completed.
                  * ``denied``: Requesting user denies the task.

    :>json success: ``true`` if task has been updated successfully.

    :statuscode 200: No error.
    :status 400: * JSON attributes are not as expected.
                 * User cannot sign/deny the task (because it is not the user's turn to sign/deny
                   or because he has not met the conditions specified in the ``signature_type`` of
                   the task's endpoint).
                 * A creator cannot sign his own task if there is someone else who is allowed to
                   sign it. (Example: A group, which the user is a member of, is assigned to review
                   the task. If there are other users in the group, the creator cannot sign it.)

    :status 403: Task with given ``task_id`` is deleted, does not
                 exist or user is not allowed to view the task. Only the creator of the task,
                 users who had been assigned to review the task or who are allowed to sign now can
                 get the task.

    """
    # get and validate JSON
    if not request.json:
        raise BadRequest('Event is missing.')
    event = request.json.get('event', None)
    if not event:
        raise BadRequest('Invalid event.')

    claims = get_jwt_claims()
    user = User.query.get(claims['id'])

    task = get_task_if_valid(claims, task_id)

    if event == 'signed':
        if not user.sign_task(task):
            raise BadRequest('Cannot sign task.')

        # only notify creator if signee was not the creator himself
        if user.user_id != task.creator_id:
            # create message that task has been signed
            create_message_task_event_by(task, 'signed', user)

        # task completed if all accesses signed
        if not task.accesses.filter(Access.access_status != 'signed').first():
            task.task_status = 'completed'
            task.save()
            # send task completed email
            send_task_completed_email.delay(task.task_id)
            # create message that task has been completed
            create_message_task_event(task, 'completed')
        else:
            if task.endpoint.email_notification_status and task.endpoint.sequential_status:
                # send notification email to next access
                next_access = task.accesses.filter_by(access_status='pending') \
                    .order_by(Access.access_order).first()
                send_task_sequential_notification_email.delay(next_access.access_id)
            # create notification message to next access
            if task.endpoint.sequential_status:
                create_message_task_tosign_sequential(task)
    elif event == 'denied':
        if not user.deny_task(task):
            raise BadRequest('Cannot deny task.')
        task.task_status = 'denied'
        task.save()
        # send task denied email
        send_task_denied_email.delay(task.task_id)
        # create message that task has been denied
        create_message_task_event_by(task, 'denied', user)
    else:
        raise BadRequest('Invalid event.')

    return jsonify(success=True), 200


@api.route('/tasks/<int:task_id>/comments/<int:comment_id>', methods=['GET'])
@jwt_required
def get_comment(task_id, comment_id):
    """Get a comment with ``comment_id`` of a task with ``task_id``.

    :reqheader Authorization: Required JWT of a user.
    :>json comment: Comment with given ``comment_id`` in :ref:`comment-schema`.

    :status 200: No error.
    :status 400: Comment with given ``comment_id`` does not belong to task with given ``task_id``.
    :status 401: Requester is not a logged-in user.
    :status 403: Task with given ``task_id`` is deleted, does not
                 exist or user is not allowed to view the task. Only the creator of the task,
                 users who had been assigned to review the task or who are allowed to sign now can
                 get the task.

    **Example response**

    .. literalinclude:: /schema/comments/comment_dump.json
        :language: json

    """
    claims = get_jwt_claims()
    task = get_task_if_valid(claims, task_id)
    comment = task.comments.filter_by(comment_id=comment_id).first()
    if comment is None:
        raise BadRequest('Comment does not belong to task.')
    response = comment_schema.jsonify(comment)
    return response, 200


@api.route('/tasks/<int:task_id>/comments', methods=['GET'])
@user_required
def get_comments(task_id):
    """Get all comments of a task with given ``task_id``.

    :query int page: Requested page of comments. Defaults to 1 if no such query parameter or if
                     given parameter is not an integer greater than 1.
    :query limit: Requested number of comments. Defaults to 20 if no such query parameter or if
                  given parameter is not an integer greater than 0.
    :reqheader Authorization: Required JWT of a user.

    :>json comments: All comments belonging to task with given ``task_id`` in :ref:`comment-schema`.
                     **Note**: Might be empty if no existing comments or if page number is
                     higher than total amount of pages.
    :>json string previous: URL to previous page. ``null``, if on first page.
    :>json string next: URL to next page. ``null``, if on last page.

    :status 200: No error.
    :status 401: Requester is not a logged-in user.
    :status 403: Task with given ``task_id`` is deleted, does not
                 exist or user is not allowed to view the task. Only the creator of the task,
                 users who had been assigned to review the task or who are allowed to sign now can
                 get the task.

    """
    claims = get_jwt_claims()
    task = get_task_if_valid(claims, task_id)

    # paginate query and dump paginated items
    pagination = paginate(request, task.comments)
    response_data = comment_schema.dump(pagination.items, many=True)

    # add previous and next URLs
    response_data = add_prev_next('api.get_comments', {'comments': response_data}, pagination,
                                  task_id=task_id)
    return jsonify(response_data), 200


@api.route('/tasks/<int:task_id>/comments', methods=['POST'])
@user_required
def create_comment(task_id):
    """Create a comment to a task with given ``task_id``.

    Send email and message to the creator of the task if task has been commented. (Only if
    commentator is not the creator himself).

    :reqheader Authorization: Required JWT.
    :resheader Location: URL of created comment.

    :<json string text: Text of comment.
    :>json comment: Created comment in :ref:`comment-schema`.

    :status 201: Comment has been created successfully.
    :status 400: Status of task is not pending.
    :status 401: Requester is not a logged-in user.
    :status 403: Task with given ``task_id`` is deleted, does not
                 exist or user is not allowed to view the task. Only the creator of the task,
                 users who had been assigned to review the task or who are allowed to sign now can
                 get the task.
    """
    claims = get_jwt_claims()
    task = get_task_if_valid(claims, task_id)

    if task.task_status != 'pending':
        raise BadRequest('Only pending tasks can be commented.')

    json_data = request.json
    # set creator and task of comment and create comment
    json_data['creator_id'] = claims['id']
    json_data['task_id'] = task.task_id
    comment = comment_schema.load(json_data)
    comment.save()

    # only notify creator if commentator is not creator himself
    if claims['id'] != task.creator_id:
        # create message that task has been commented
        create_message_task_event_by(task, 'commented', User.query.get(claims['id']))
        # send task commented email
        send_task_commented_email.delay(task.task_id)

    response = comment_schema.jsonify(comment)
    # add url to created task to response
    url = url_for('api.get_comment', comment_id=comment.comment_id, task_id=task_id)
    # location has to contain a relative URL
    response.autocorrect_location_header = False
    return response, 201, {'Location': url}


def get_task_schema_for_role(role):
    """Get individual task schema for a role.

    Args:
        role (str): Role of requestor. Either admin or user.

    Returns:
        TaskSchema: A TaskSchema with fields depending on role of the requestor.

    """
    return task_schema_admin if role == 'admin' else task_schema_user


def get_task_if_valid(claims, task_id):
    """Get task if access is valid.

    Args:
        claims (Array of str: obj): Contains id and role of requestor.
        task_id (int): ID of task.

    Raises:
        Forbidden: If requestor is not allowed to access task.

    Returns:
        Task: Requested task.

    """
    task = Task.query.get(task_id)

    if claims['role'] == 'admin':
        if task is None or task.task_status == 'withdrawn':
            abort(404)
        else:
            return task

    # raise Forbidden error if task does not exist
    if task is None or task.task_status == 'withdrawn':
        raise Forbidden('Access denied.')

    # check if user is allowed to access task with given task_id
    user = User.query.get(claims['id'])

    # task is created by user
    if task.creator_id == user.user_id:
        return task

    # sqlalchemy does not support _in with relations (yet), work around: use foreign IDs as keys
    group_ids = [group.group_id for group in user.groups.all()]

    # check if requesting user was allowed to sign the task or if user is allowed to view the task
    if task.accesses.filter(and_(Access.access_status != 'pending',
                                 or_(Access.user == user, Access.group_id.in_(group_ids)))).first():
        return task
    elif user.get_unsigned_accesses_to_view(task):
        return task
    raise Forbidden('Access denied')


def add_deletable(data, user):
    """Add deletable attribute to each dumped task in data.

    'deletable' is true if requesting user can delete task.

    Args:
        data (dic of str : obj): Dictionary containing dumped tasks.
        user (User): Requesting user.

    Returns:
        dic of str : obj: Data with attributes 'deletable' for each task.

    """
    for task in data:
        task['deletable'] = ((task['creator']['user_id'] == user.user_id and
                              Endpoint.query.get(task['endpoint_id']).deletable_status) and
                             task['task_status'] != 'pending')
    return data
