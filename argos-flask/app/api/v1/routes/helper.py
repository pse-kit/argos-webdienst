"""This module contains helper methods for the routes module."""

from flask import url_for, g
from flask_babel import gettext, refresh

from app.models.models import Message, Access

# default value of number of items per page
LIMIT = 20


def paginate(request, query):
    """Get pagination object of given query according to request parameters.

    Args:
        request (Request): A Request object with parameters page (default: 1), limit (default: 20).
        query (BaseQuery): A Query object to paginate.

    Returns:
        Pagination: Pagination of query.

    """
    # get page number and limit from request (default: page=1, limit=20)
    page = request.args.get('page', 1, type=int)
    limit = request.args.get('limit', LIMIT, type=int)
    pagination = query.paginate(page, limit, False)
    return pagination


def add_prev_next(url, data, page, **kwargs):
    """Add URLs for next and previous page to given data.

    Args:
        url (str): URL of current (Flask) endpoint.
        data (dic of str : list) Dictionary to which URLs are added.
        page (Pagination): Pagination object of current page.
        kwargs (dic of str : obj) Other needed arguments for the endpoint.

    Returns:
        dic of str: obj: Contains data and URLs for previous and next page.
                         Value of previous/next ist None if previous/next page does not exist.

    """
    # set url to previous/next page if they exist (else: None)
    if page.has_next:
        next_url = url_for(url, page=page.next_num, limit=page.per_page, **kwargs)
    else:
        next_url = None
    if page.has_prev:
        prev_url = url_for(url, page=page.prev_num, limit=page.per_page, **kwargs)
    else:
        prev_url = None
    data['previous'] = prev_url
    data['next'] = next_url
    return data


def create_message_task_event(task, event):
    """Create message for creator of given task that given event has occured.

    Args:
        task (Task): Updated task.
        event (str): Event that has occured.

    """
    # translate event message
    g.user = task.creator
    if event == 'created':
        event = gettext('created')
    elif event == 'completed':
        event = gettext('completed')
    elif event == 'deleted':
        event = gettext('deleted')
    elif event == 'withdrawn':  # pragma: no branch
        event = gettext('withdrawn')
    text = gettext('Your task "%(task_name)s" has been %(event)s.', task_name=task.name,
                   event=event)
    refresh()
    Message(text=text, recipient=task.creator).save()


def create_message_task_event_by(task, event, user):
    """Create message for creator of given task that given event been triggered by given user.

    Args:
        task (Task): Updated task.
        event (str): Event that has occured.
        user (User): User that has triggered the event.

    """
    # translate event message
    g.user = task.creator
    if event == 'signed':
        event = gettext('signed')
    elif event == 'denied':
        event = gettext('denied')
    elif event == 'commented':  # pragma: no branch
        event = gettext('commented')
    text = gettext('Your task "%(task_name)s" has been %(event)s by %(user_name)s.',
                   task_name=task.name, event=event, user_name=user.name)
    refresh()
    Message(text=text, recipient=task.creator).save()


def create_message_task_tosign_sequential(task):
    """Create message for all users that can sign the given sequential task.

    Args:
        task (Task): Sequential task that can be signed.

    """
    # get access who has to sign next
    next_access = task.accesses.filter_by(access_status='pending') \
                               .order_by(Access.access_order).first()

    # send to single user or to each user of a group
    if next_access.user:
        users = [next_access.user]
    else:
        users = next_access.group.users.all()
        # creator of a task does not receive a message if he is not allowed to sign
        if len(users) > 1 and task.creator in users:
            users.remove(task.creator)

    for user in users:
        g.user = user
        text = gettext('You were assigned by %(creator_name)s to review the task "%(task_name)s".',
                       creator_name=task.creator.name, task_name=task.name)
        refresh()
        Message(text=text, recipient=user).save()


def create_message_task_tosign_parallel(task):
    """Create message for all users that can sign the given parallel task.

    Args:
        task (Task): Parallel task that can be signed.

    """
    # get all users who can sign the task (send only once)
    accesses = task.accesses.all()
    users = []
    for access in accesses:
        if access.user:
            users += [access.user]
        else:
            group = access.group.users.all()
            if len(group) > 1 and task.creator in group:
                # task creator receives no message if he cannot sign the task
                group.remove(task.creator)
            users += group

    for user in set(users):
        g.user = user
        text = gettext('You were assigned by %(creator_name)s to review the task "%(task_name)s".',
                       creator_name=task.creator.name, task_name=task.name)
        refresh()
        Message(text=text, recipient=user).save()
