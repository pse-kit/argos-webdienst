"""This module contains the message routes."""

from flask import jsonify, request
from flask_jwt_extended import get_jwt_claims

from app.api.v1 import api
from app.api.v1.authenticator.helpers import user_required
from app.api.v1.errors import Forbidden
from app.api.v1.routes.helper import add_prev_next, paginate
from app.models.models import Message
from app.models.schemas import MessageSchema

# global message schema to use in routes
message_schema = MessageSchema()


@api.route('/messages/', methods=['GET'])
@user_required
def get_messages():
    """Get all unread messages of requesting user ordered by id.

    :query int page: Requested page of messages. Defaults to 1 if no such query parameter or if
                     given parameter is not an integer greater than 1.
    :query int limit: Requested number of messages. Defaults to 20 if no such query parameter or if
                  given parameter is not an integer greater than 0.

    :reqheader Authorization: Required JWT of a user.

    :>json messages: All unread messages of requesting user in :ref:`message-schema`.
                     **Note**: Might be empty if no existing messages or if page number is
                     higher than total amount of pages.
    :>json previous: URL to previous page. ``null``, if on first page.
    :>json next: URL to next page. ``null``, if on last page.

    :status 200: No error.
    :status 401: Requester is not a logged-in user.

    """
    claims = get_jwt_claims()

    # paginate query according to parameters in request
    query = Message.query.filter_by(recipient_id=claims['id']).filter_by(read_status=False) \
                         .order_by(Message.message_id.desc())
    pagination = paginate(request, query)
    response_data = MessageSchema().dump(pagination.items, many=True)

    # add urls for previous and next page to response
    response = add_prev_next('api.get_messages', {'messages': response_data}, pagination)

    return jsonify(response), 200


@api.route('/messages/<int:message_id>', methods=['GET'])
@user_required
def get_message(message_id):
    """Get message by id. Read status of requested message is set to true.

    :reqheader Authorization: Required JWT of a user.
    :>json message: Message with given ``message_id`` in :ref:`message-schema`.

    :status 200: No error.
    :status 401: Requester is not a logged-in user.
    :status 403: Recipient of requested message is not requesting user.

    **Example response JSON:**

    .. literalinclude:: /schema/message.json
        :language: json

    """
    claims = get_jwt_claims()
    message = get_message_if_valid(claims, message_id)
    response = message_schema.jsonify(message)
    message.read_status = True
    message.save()
    return response, 200


@api.route('/messages/<int:message_id>/status', methods=['PUT'])
@user_required
def update_read_status(message_id):
    """Set read status of message with given ``message_id`` to ``true``.

    :reqheader Authorization: Required JWT of a user.
    :>json success: ``true`` if read status of message has been set successfully.

    :status 200: No error.
    :status 401: Requester is not a logged-in user.
    :status 403: Recipient of requested message is not requesting user.

    """
    claims = get_jwt_claims()
    message = get_message_if_valid(claims, message_id)
    message.read_status = True
    message.save()
    return jsonify(success=True), 200


def get_message_if_valid(claims, message_id):
    """Get message if user is allowed to access message.

    Args:
        claims (Array of obj): Contains ID and role of requestor.
        message_id (int): ID of message to be accessed.

    Raises:
        Forbidden: If requested message does not belong to requesting user or if message is already
                   read.

    Returns:
        Message: Requested message.

    """
    message = Message.query.get(message_id)
    if message is None or message.recipient_id != claims['id'] or message.read_status:
        raise Forbidden('Access denied.')
    return message
