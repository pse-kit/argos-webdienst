"""This module contains the user_handler routes."""

from flask import jsonify, request
from flask_jwt_extended import get_jwt_claims, jwt_required
from sqlalchemy import or_

from app.api.v1 import api
from app.api.v1.authenticator.helpers import user_required
from app.api.v1.errors import BadRequest
from app.models.models import User
from app.models.schemas import UserSchema

# global user schema to use in routes
user_schema = UserSchema()


@api.route('/users/search/', methods=['GET'])
@jwt_required
def search_user():
    """Search for users.

    Return users whose names or usernames contain the given search term or whose user_id is matches
    exactly the given search term.

    :query string search: String to look for in names, usernames, ID of user.
    :reqheader Authorization: Required JWT.

    :>json users: Found users in :ref:`user-schema`

    :statuscode 200: No error.
    :statuscode 400: Unexpected query parameters.
    :status 401: Requester is not logged in.

    **Example response:**

    .. literalinclude:: /schema/search_user.json
        :language: json

    """
    search_term = request.args.get('search', None)
    if search_term is None:
        raise BadRequest('No search term given.')

    # return empty list if search term is empty
    if len(search_term) == 0 or search_term == " ":
        return jsonify({'users': []}), 200

    # cast search_term to int if it is a number
    if search_term.isdigit():
        search_term = int(search_term)

    users = User.query.filter(or_(User.name.contains(search_term),
                                  or_(User.user_id == search_term,
                                      User.username.contains(search_term)))).all()
    response = user_schema.dump(users, many=True)
    return jsonify({'users': response}), 200


@api.route('/users/current/', methods=['GET'])
@user_required
def get_current_user():
    """Get current user.

    :reqheader Authorization: Required JWT of a user.

    :>json user: Current user in :ref:`user-schema`.

    :statuscode 200: No error.

    """
    claims = get_jwt_claims()
    user = User.query.get(claims['id'])

    return user_schema.jsonify(user), 200
