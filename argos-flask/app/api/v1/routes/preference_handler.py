"""This module contains the group_handler routes."""

from flask import jsonify, request
from flask_jwt_extended import get_jwt_claims

from app.api.v1 import api
from app.api.v1.authenticator.helpers import user_required
from app.api.v1.errors import BadRequest
from app.models.models import User


@api.route('/preferences/language/', methods=['GET'])
@user_required
def get_language():
    """Get language of requesting user.

    :reqheader Authorization: Required JWT of a user.
    :>json language: Language of requesting user.

    :status 200: No error.
    :status 401: Requester is not a logged-in user.

    """
    claims = get_jwt_claims()
    user = User.query.get(claims['id'])
    return jsonify(language=user.language), 200


@api.route('/preferences/language/', methods=['PUT'])
@user_required
def set_language():
    """Set language of requesting user.

    :reqheader Authorization: Required JWT. of a user.
    :<json language: Language to set.
    :>json success: ``true`` if setting language has been successful.

    :status 200: Setting language has been successful.
    :status 400: No JSON given or unexpected JSON attributes.
    :status 401: Requester is not a logged-in user.
    .. note:: Every string is accepted as a valid language. Argos uses the default language if
              there is no suitable translation.

    """
    # check if request is valid
    if request.json is None:
        raise BadRequest('Invalid language.')
    language = request.json.get('language', None)
    if language is None:
        raise BadRequest('Invalid language.')

    # set user language
    claims = get_jwt_claims()
    user = User.query.get(claims['id'])
    user.language = language
    user.save()
    return jsonify({'success': True}), 200


@api.route('/preferences/notifications/', methods=['GET'])
@user_required
def get_email_notification_status():
    """Get preferences on receiving email notifications of requesting user.

    :reqheader Authorization: Required JWT. of a user.
    :>json notification_status: ``true`` if email notifications are enabled (user receives email
                                notification if he is assigned to signing a task), else ``false``.
    :status 200:  No errors.
    :status 401: Requester is not a logged-in user.

    """
    claims = get_jwt_claims()
    user = User.query.get(claims['id'])
    return jsonify(notification_status=user.email_notification_status), 200


@api.route('/preferences/notifications/', methods=['PUT'])
@user_required
def set_email_notification_status():
    """Set preference on email notifications of requesting user.

    :reqheader Authorization: Required JWT. of a user.
    :<json notification_status: Notification status to set. (``true``: User will receive
                                notifications if he is assigned to signing a task, ``false``: User
                                will not receive such notifications).
    :>json success: ``true`` if setting language has been successful.

    :status 200: Setting language has been successful.
    :status 400: No JSON given or unexpected JSON attributes.
    :status 401: Requester is not a logged-in user.

    .. note:: This setting does not affect emails that are received after creation, denial or
              completion of a task.

    """
    # check if request is valid
    if request.json is None:
        raise BadRequest('Invalid notification status.')
    notification_status = request.json.get('notification_status', None)
    if notification_status is None or not isinstance(notification_status, bool):
        raise BadRequest('Invalid notification status.')

    # set notification status
    claims = get_jwt_claims()
    user = User.query.get(claims['id'])
    user.email_notification_status = notification_status
    user.save()
    return jsonify(success=True), 200
