"""This module contains the endpoint routes."""

from flask import jsonify, request, url_for, abort
from flask_jwt_extended import get_jwt_claims, jwt_required

from app.api.v1 import api
from app.api.v1.authenticator.helpers import admin_required
from app.api.v1.errors import BadRequest
from app.api.v1.routes.helper import add_prev_next, paginate
from app.models.models import Endpoint, Task
from app.models.schemas import EndpointSchema

# global endpoint schema to use in routes
endpoint_schema = EndpointSchema()


@api.route('/endpoints', methods=['GET'])
@jwt_required
def get_endpoints_active():
    """Get all active endpoints ordered by name.

    :query int page: Requested page of endpoints. Defaults to 1 if no such query parameter or if
                     given parameter is not an integer greater than 1.
    :query int limit: Requested number of endpoints. Defaults to 20 if no such query parameter or if
                  given parameter is not an integer greater than 0.
    :reqheader Authorization: Required JWT.
    :>json endpoints: Active endpoints in :ref:`endpoint-schema` ordered by name.

                      * *Admin*: Instantiable endpoints and not-instantiable endpoints with active
                        tasks.
                      * *User*: Instantiable endpoints. Attribute ``active_tasks`` is omitted.
                      * **Note**: Might be empty if no active endpoints or if page number is
                        higher than total amount of pages.

    :>json string previous: URL to previous page. ``null``, if on first page.
    :>json string next: URL to next page. ``null``, if on last page.

    :statuscode 200: No error.
    :status 401: Requester is not logged in.

    **Example response (admin requesting):**

    .. literalinclude:: /schema/endpoints/get_endpoints_active.json
        :language: json

    """
    # get requester
    claims = get_jwt_claims()

    instantiable = Endpoint.query.filter(Endpoint.instantiable_status)
    if claims['role'] == 'admin':
        # admin also sees not-instantiable endpoints that still have pending tasks
        not_instantiable = Endpoint.query.filter_by(instantiable_status=False) \
                                   .filter(Endpoint.tasks.any(Task.task_status == 'pending'))
        query = instantiable.union(not_instantiable).order_by(Endpoint.name)
    else:
        query = instantiable.order_by(Endpoint.name)

    # paginate query according to parameters in request
    pagination = paginate(request, query)
    response_data = endpoint_schema.dump(pagination.items, many=True)

    # delete attribute active tasks if a user is requesting
    response_data = modify_output_for_role(claims['role'], response_data)

    # add urls for previous and next page to response
    response = add_prev_next('api.get_endpoints_active', {'endpoints': response_data}, pagination)

    return jsonify(response), 200


@api.route('/endpoints/<int:endpoint_id>', methods=['GET'])
@jwt_required
def get_endpoint(endpoint_id):
    """Get endpoint by ID.

    :reqheader Authorization: Required JWT.
    :>json endpoint: Endpoint with given ``endpoint_id`` in :ref:`endpoint-schema`. For a
                     requesting user the attribute ``active_tasks`` is omitted.

    :statuscode 200: No error.
    :status 401: Requester is not logged in.
    :status 404: * No endpoint with given ``endpoint_id`` exists.
                 * Admin is requester and endpoint is not instantiable and active tasks of this
                   endpoint are 0.
                 * User is requester and endpoint is not instantiable.

    **Example response JSON (admin requesting):**

    .. literalinclude:: /schema/endpoints/endpoint_dump.json
        :language: json

    """
    claims = get_jwt_claims()
    endpoint = get_endpoint_by_id(claims, endpoint_id)
    response_data = endpoint_schema.dump(endpoint)

    # delete attribute active_tasks if a user is requesting
    response_data = modify_output_for_role(claims['role'], response_data)

    return jsonify(response_data), 200


@api.route('/endpoints/', methods=['POST'])
@admin_required
def create_endpoint():
    """Create new endpoint.

    :reqheader Authorization: Required JWT of an admin.
    :resheader Location: URL of created endpoint.
    :<json endpoint: Data for creating a new endpoint in :ref:`endpoint-schema`.
    :>json endpoint: Created endpoint in :ref:`endpoint-schema`.

    :statuscode 201: Endpoint has been created successfully.
    :status 400: * JSON attributes are not as expected (type, name, amount).
                 * With given IDs referenced objects do not exist (``user_id``, ``group_id`` in
                   ``given_accesses``).
                 * JSON attribute ``given_accesses`` is empty and ``allow_additional_accesses`` is
                   ``false``.
                 * ``sequential_status`` is true and access orders of ``given_accesses`` are not
                   specified or not integers from 0 to n.
                 * User ID and group ID in accesses of ``given_accesses`` are both set or not set.
                   (Only one of these two can appear in JSON or one of them is null.)
                 * More than one field is of type ``file``.
                 * ``signature_type`` is ``checkboxAfterDownload`` or ``buttonAfterDownload`` but
                   no required field of type ``file`` is defined.

    :status 401: Requester is not a logged-in admin.

    .. note:: * If ``sequential_status`` is false, ``access_order`` of ``given_accesses`` and
                ``additional_access_insert_before_status`` are ignored (``access_order`` are not
                validated).
              * Empty lists or attributes with value 'null' are ignored.

    **Example request JSON:**

    .. literalinclude:: /schema/endpoints/endpoint_load.json
        :language: json

    """
    if not request.json:
        raise BadRequest('Missing JSON')

    endpoint = endpoint_schema.load(request.json)
    endpoint.save()
    response = endpoint_schema.jsonify(endpoint)

    # add url to created endpoint to response
    url = url_for('api.get_endpoint', endpoint_id=endpoint.endpoint_id)
    # location has to contain a relative URL
    response.autocorrect_location_header = False
    return response, 201, {'Location': url}


@api.route('/endpoints/<int:endpoint_id>', methods=['DELETE'])
@admin_required
def set_endpoint_deprecated(endpoint_id):
    """Set endpoint with given ``endpoint_id`` to not instantiable.

    :reqheader Authorization: Required JWT of an admin.
    :>json success: ``true`` if endpoint has been labeled as not-instantiable successfully.
    :statuscode 200: No error.
    :status 400: Endpoint cannot be labeled as not-instantiable.
    :status 401: Requester is not a logged-in admin.
    :status 404: Endpoint with given ``endpoint_id`` does not exist.

    """
    claims = get_jwt_claims()
    endpoint = get_endpoint_by_id(claims, endpoint_id)
    if not endpoint.instantiable_status:
        raise BadRequest('Endpoint is already not instantiable.')
    endpoint.instantiable_status = False
    endpoint.save()
    return jsonify(success=True), 200


def modify_output_for_role(role, data):
    """Modify output for roles after dumping schema.

    Args:
        role (str): Either admin or user. Role for which output is modified.
        data (list of dict of str : obj): Output data to be modified.

    Returns:
        list of dict of str : obj: Modified data.

    """
    # delete attribute active_tasks if user is requesting
    if role == 'user':
        if isinstance(data, list):
            for endpoint in data:
                endpoint.pop('active_tasks')
        else:
            data.pop('active_tasks')
    return data


def get_endpoint_by_id(claims, endpoint_id):
    """Get endpoint with given endpoint_id.

    Args:
        claims (Array of str: obj): Contains id and role of requestor.
        endpoint_id (int): ID of endpoint to return.

    Returns:
        Endpoint: Endpoint with given endpoint_id.

    Raises:
        404 Error: If admin requests a not-instantiable endpoint with no active tasks.
                   If user requests a not-instantiable endpoint.

    """
    endpoint = Endpoint.query.get(endpoint_id)
    if not endpoint:
        abort(404)
    if claims['role'] == 'admin':
        # endpoint does not exist anymore if it is not instantiable and if no active tasks exists
        if not endpoint.instantiable_status and \
                len(endpoint.tasks.filter_by(task_status='pending').all()) == 0:
            abort(404)
    else:
        if not endpoint.instantiable_status:
            abort(404)
    return endpoint
