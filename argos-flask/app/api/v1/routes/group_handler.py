"""This module contains the group_handler routes."""

from flask import jsonify, request
from flask_jwt_extended import jwt_required

from app.api.v1 import api
from app.api.v1.errors import BadRequest
from app.models.models import Group
from app.models.schemas import GroupSchema

# global group schema to use in routes
group_schema = GroupSchema()


@api.route('/groups/search/', methods=['GET'])
@jwt_required
def search_group():
    """Search for groups.

    Return groups whose names contain the given search term.

    :query string search: String to look for in group names.
    :reqheader Authorization: Required JWT.
    :>json groups: Found groups in :ref:`group-schema`

    :statuscode 200: No error.
    :statuscode 400: Unexpected query parameters.
    :status 401: Requester is not logged in.

    **Example response JSON:**

    .. literalinclude:: /schema/search_group.json
        :language: json

    """
    search_term = request.args.get('search', None, type=str)
    if search_term is None:
        raise BadRequest('No search term given.')

    # return empty list if search term is empty
    if len(search_term) == 0 or search_term == " ":
        return jsonify({'groups': []}), 200

    # filter groups by by search term and dump found groups
    groups = Group.query.filter(Group.name.contains(search_term)).all()
    response = group_schema.dump(groups, many=True)

    return jsonify({'groups': response}), 200
