"""This module contains the file_handler routes and some helper functions."""

import base64
import datetime
import hashlib
import os
import uuid
import zipfile
from calendar import timegm
from pathlib import Path

from celery.utils.log import get_task_logger
from flask import current_app, jsonify
from flask_jwt_extended import get_jwt_claims
from werkzeug.utils import secure_filename

from app import db, celery
from app.api.v1 import api
from app.api.v1.authenticator.helpers import user_required
from app.api.v1.errors import BadRequest
from app.models.models import Task_Field, Task, User

logger = get_task_logger(__name__)


def is_extension_allowed(filename):
    """Check if file extension is allowed.

    Args:
        filename (str): Filename to check.

    Returns:
        bool: True -> Extension allowed, False -> Extension forbidden.

    """
    if '.' in filename:
        if filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']:
            return True
    return False


@celery.task
def save_files_as_zip(task_id):
    """Save the files belonging to the task as compressed ZIP archive.

    Args:
        task_id (int): ID of the task.

    """
    # create output folder if it does not exist
    if not os.path.exists(current_app.config['DOWNLOAD_FOLDER']):
        os.mkdir(current_app.config['DOWNLOAD_FOLDER'])

    task = Task.query.get(task_id)
    # get tasks_fields containing the filenames
    fields = Task_Field.query.filter_by(task=task).join(Task_Field.field) \
        .filter_by(field_type='file').all()

    zip_dir = current_app.config['DOWNLOAD_FOLDER']
    task_hash = hashlib.sha1(f"{task.creator.name}{task.creation_date}"
                             .encode('utf-8')).hexdigest()
    secure_task_name = secure_filename(task.name)
    zip_filename = os.path.join(zip_dir, f'{secure_task_name}-{task_hash}.zip')

    # create ZIP
    with zipfile.ZipFile(zip_filename, 'w', zipfile.ZIP_DEFLATED) as zip:
        # writing each file one by one
        for field in fields:
            uuid_filename = field.value
            filepath = os.path.join(current_app.config['UPLOAD_FOLDER'], uuid_filename)
            filename = uuid_filename.split('$', 1)[1]
            zip.write(filepath, filename)
            logger.debug(f'File {filepath} added to zip')

            # rename filename in field: remove uuid
            field.value = filename
            field.save()
            # remove file from upload dir
            os.remove(filepath)

        zip.close()
        logger.debug(f'Saved zip in {zip_filename}')


def handle_files(task, files, field):
    """Handle the files of a given task.

    Args:
        task (Task): Current task.
        files (list of File): Files of request.
        fields (Field): The field of type file of the endpoint for this task.

    Raises:
        BadRequest: File extension not allowed.

    """
    # check if extension is allowed
    for file in files:
        if not is_extension_allowed(file.filename):
            raise BadRequest('File extension not allowed: ' + file.filename)

    # create upload folder if it does not exist
    if not os.path.exists(current_app.config['UPLOAD_FOLDER']):
        os.mkdir(current_app.config['UPLOAD_FOLDER'])

    # save to disk
    for index, file in enumerate(files):
        filename = "{uuid}${filename}".format(uuid=str(uuid.uuid4()),
                                              filename=secure_filename(file.filename))
        file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

        # add fields to task (all tasks_fields of type file are mapped to the same field)
        task_field = Task_Field(task=task, field=field, value=filename)
        db.session.add(task_field)


def generate_secure_link(filename):
    """Generate a secure download link for the given filename.

    Args:
        filename (str): Filename.

    Returns:
        str: Download link (expires in 5 minutes)

    """
    folder = current_app.config['DOWNLOAD_FOLDER']
    url_to_file = f"/{folder}/{filename}"
    # link should expire in 5 minutes
    future = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)
    expiry = timegm(future.timetuple())
    secret = current_app.config['SECURE_LINK_SECRET']

    # https://www.nginx.com/blog/securing-urls-secure-link-module-nginx-plus/
    secure_link = f"{secret}{url_to_file}{expiry}".encode('utf-8')

    # create MD5 value
    hash = hashlib.md5(secure_link).digest()
    base64_hash = base64.urlsafe_b64encode(hash)
    str_hash = base64_hash.decode('utf-8').rstrip('=')
    return f"{url_to_file}?st={str_hash}&e={expiry}"


@api.route('/tasks/<int:task_id>/files', methods=['GET'])
@user_required
def get_file_url(task_id):
    """Get a download link for ZIP containing the files of task.

    :reqheader Authorization: Required JWT of a user.
    :>json string link: Download link for requested files.

    :status 200: No errors.
    :status 400: No files belong to task with given ``task_id`` or zipping files has not finished
        yet.
    :status 403: Task with given ``task_id`` is deleted, does not
                exist or user is not allowed to view the task. Only the creator of the task,
                users who had been assigned to review the task or who are allowed to sign now can
                get the task.

    """
    from app.api.v1.routes.task_handler import get_task_if_valid
    claims = get_jwt_claims()
    user = User.query.get(claims['id'])
    # check if user is allowed to view this task
    task = get_task_if_valid(claims, task_id)

    zip_dir = current_app.config['DOWNLOAD_FOLDER']
    task_hash = hashlib.sha1(f"{task.creator.name}{task.creation_date}"
                             .encode('utf-8')).hexdigest()
    secure_task_name = secure_filename(task.name)
    zip_filename = f"{secure_task_name}-{task_hash}.zip"

    zip_path = Path(f"{zip_dir}/{zip_filename}")
    if not zip_path.is_file():  # pragma: no cover
        raise BadRequest('Either no files belong to this task or ZIP has not yet been created')

    # update accesses
    user.download_task(task)

    return jsonify(link=generate_secure_link(zip_filename))
