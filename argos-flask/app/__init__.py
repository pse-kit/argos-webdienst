"""Application Factory and constructor for the application package."""

import logging
import os
import warnings
from logging.handlers import RotatingFileHandler

from celery import Celery
from flask import Flask
from flask.logging import default_handler
from flask_babel import Babel
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_ldap3_login import LDAP3LoginManager
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from app.synchronizer.ldap_sync import Synchronizer
from app.utils import cli
from config import config

db = SQLAlchemy()  # create SQLAlchemy object
ma = Marshmallow()  # create Flask-Marshmallow object
migrate = Migrate()  # create Flask-Migrate object
ldap3_manager = LDAP3LoginManager()  # create LDAP3 Manager object
jwt_manager = JWTManager()  # create JWTManager object
synchronizer = Synchronizer()  # create Synchronizer object
celery = Celery()  # create Celery object
mail = Mail()  # create Flask-Mail object
babel = Babel()  # create Flask-Babel object


def create_app(config_name, custom_config=None):
    """Application factory.

    Args:
        config_name (str): Name of a configuration to use for the application.
        custom_config (dict, optional): Defaults to None. Custom configuration values for
            testing purposes only.

    Returns:
        flask app: Flask application instance.

    """
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    app.url_map.strict_slashes = False

    # update config - for testing purposes
    if custom_config:
        app.config.update(custom_config)

    # get celery config
    celery.config_from_object(app.config)

    # register custom commands
    cli.register(app)

    # model imports - required for Flask-Migrate
    from app.models.models import Admin, Access, Comment, Endpoint, Field  # noqa: F401
    from app.models.models import Given_Access, Group, Message, Task, Task_Field  # noqa: F401
    from app.models.models import Token, User  # noqa: F401

    db.init_app(app)  # initialize SQLAlchemy
    ma.init_app(app)  # initialize Flask-Marshmallow
    migrate.init_app(app, db)  # initialize Flask-Migrate
    jwt_manager.init_app(app)  # initialize JWTManager
    ldap3_manager.init_app(app)  # initialize LDAP3 Manager
    synchronizer.init_app(app, ldap3_manager)  # initialize Synchronizer
    mail.init_app(app)  # initialize Flask-Mail
    babel.init_app(app)  # initialize Flask-Babel

    # register blueprints - avoid circular dependencies
    from app.api.v1 import api as api_v1
    app.register_blueprint(api_v1, url_prefix='/api/v1')

    # Enable CORS and cache CORS preflight requests for 10 minutes
    CORS(app, resources={r"/api/*": {"origins": "*"}}, max_age=600)

    # missing config in production environment
    if not app.testing and not app.debug:  # pragma: no cover
        if 'SECRET_KEY' not in os.environ:
            warnings.warn('Never use the default secret key in production!', UserWarning)

        if 'SECURE_LINK_SECRET' not in os.environ:
            warnings.warn(
                'Download links do not work without secret used in nginx.conf!', UserWarning)

    # setup logging
    if not app.testing:  # pragma: no cover
        if app.config['LOG_TO_FILE']:
            # create logs directory, if it does not exist
            if not os.path.exists('logs'):
                os.mkdir('logs')

            # create RotatingFileHandler, which saves the last ten log files
            file_handler = RotatingFileHandler('logs/argos.log', maxBytes=10240, backupCount=10)
            file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s '
                                                        '[in %(pathname)s:%(lineno)d]'))
            file_handler.setLevel(logging.WARNING)
            app.logger.addHandler(file_handler)
            app.logger.removeHandler(default_handler)

    return app
