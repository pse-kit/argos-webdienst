"""This module contains all email related stuff.

Arguments of celery tasks are intentionally not objects! See:
https://celery.readthedocs.io/en/latest/userguide/security.html#security-serializers
"""

from celery.utils.log import get_task_logger
from flask import current_app, render_template, g
from flask_babel import _, refresh
from flask_mail import Message

from app import celery, mail, babel
from app.models.models import Task, Access

logger = get_task_logger(__name__)


@babel.localeselector
def get_locale():
    """Get locale of current user.

    Returns:
        str: Locale (language code).

    """
    # try to use the locale from the user model
    user = getattr(g, 'user', None)
    if user is not None:
        if user.language in current_app.config['SUPPORTED_LOCALES']:
            return user.language

    # use default language
    return None


def create_message(to, subject, template, **kwargs):
    """Create a new message object that can be sent with Flask-Mail.

    Args:
        subject (str): Email subject.
        to (str): TO Email address.
        template (Jinja2 template): The template to use.

    Returns:
        flask_mail.Message: Message object.

    """
    app = current_app._get_current_object()
    msg = Message(app.config['MAIL_SUBJECT_PREFIX'] + ' ' + subject, recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    return msg


@celery.task
def send_task_creation_email(task_id):
    """Send confirmation email for successful task creation to the creator.

    Args:
        task_id (int): Task ID.

    """
    # get necessary data
    task = Task.query.get(task_id)
    creator = task.creator
    g.user = creator  # set user for localeselector
    link = "{base}{task_id}".format(base=current_app.config['TASK_DETAIL_PAGE'],
                                    task_id=task.task_id)

    # send email
    msg = create_message(creator.email_address, _('Task creation successful'),
                         'mailer/task_creation', creator=creator, task=task, link=link)
    logger.debug('Sending mail to {}'.format(msg.recipients[0]))
    mail.send(msg)

    refresh()  # reset current locale


def send_notification_email_user(user, task):
    """Send task review notification email to provided user.

    Args:
        user (User): User to whom the email should be sent.
        task (Task): Current task.

    """
    if user.email_notification_status is True:
        # get necessary data
        creator = task.creator
        link = "{base}{task_id}".format(base=current_app.config['TASK_DETAIL_PAGE'],
                                        task_id=task.task_id)
        g.user = user  # set user for localeselector

        # send email
        msg = create_message(user.email_address, _('Task waiting for review'),
                             'mailer/task_notification_user', user=user, creator=creator,
                             task=task, link=link)
        logger.debug('Sending mail to {}'.format(msg.recipients[0]))
        mail.send(msg)

        refresh()  # reset current locale


def send_notification_email_group(group, group_members, task):
    """Send task review notification email to provided group members.

    Args:
        group (Group): Current group.
        group_members (list): Group members to which the email should be sent.
        task (Task): Current task.
    """
    creator = task.creator
    link = "{base}{task_id}".format(base=current_app.config['TASK_DETAIL_PAGE'],
                                    task_id=task.task_id)

    # do not send mail to creator if other members of the group can sign the task
    if len(group_members) > 1 and creator in group_members:
        group_members.remove(creator)

    # send to group members
    for user in group_members:
        if user.email_notification_status is True:
            g.user = user  # set current user for localeselector

            # send email
            msg = create_message(user.email_address, _('Task waiting for review'),
                                 'mailer/task_notification_group', user=user, group=group,
                                 creator=creator, task=task, link=link)
            logger.debug('Sending mail to {}'.format(msg.recipients[0]))
            mail.send(msg)

            refresh()  # reset current locale

    return group_members


@celery.task
def send_task_sequential_notification_email(access_id):
    """Send task review notification email to provided access (user or group).

    Args:
        access_id (int): Access ID.

    """
    # get necessary data
    access = Access.query.get(access_id)
    task = access.task

    if access.user is not None:
        send_notification_email_user(access.user, task)

    else:
        send_notification_email_group(access.group, access.group.users.all(), task)


@celery.task
def send_task_parallel_notification_email(task_id):
    """Send task review notification email to all accesses of the provided task.

    Args:
        task_id (int): Task ID.

    """
    # get necessary data
    task = Task.query.get(task_id)
    accesses = task.accesses
    users_notified = []  # user should not get multiple emails

    for access in accesses:
        if access.user is not None:
            if access.user not in users_notified:
                send_notification_email_user(access.user, task)
                users_notified.append(access.user)

        else:
            group_members = [user for user in access.group.users if user not in users_notified]

            # recipients of notification email
            recipients = send_notification_email_group(access.group, group_members, task)
            users_notified.extend(recipients)


@celery.task
def send_task_completed_email(task_id):
    """Send task completed email to creator.

    Args:
        task_id (int): Task ID.

    """
    # get necessary data
    task = Task.query.get(task_id)
    creator = task.creator
    g.user = creator  # set user for localeselector
    link = "{base}{task_id}".format(base=current_app.config['TASK_DETAIL_PAGE'],
                                    task_id=task.task_id)

    # send email
    msg = create_message(creator.email_address, _('Task completed'),
                         'mailer/task_completed', creator=creator, task=task, link=link)
    logger.debug('Sending mail to {}'.format(msg.recipients[0]))
    mail.send(msg)

    refresh()  # reset current locale


@celery.task
def send_task_denied_email(task_id):
    """Send task denied email to creator.

    Args:
        task_id (int): Task ID.

    """
    # get necessary data
    task = Task.query.get(task_id)
    creator = task.creator
    g.user = creator  # set user for localeselector
    link = "{base}{task_id}".format(base=current_app.config['TASK_DETAIL_PAGE'],
                                    task_id=task.task_id)

    # send email
    msg = create_message(creator.email_address, _('Task denied'),
                         'mailer/task_denied', creator=creator, task=task, link=link)
    logger.debug('Sending mail to {}'.format(msg.recipients[0]))
    mail.send(msg)

    refresh()  # reset current locale


@celery.task
def send_task_commented_email(task_id):
    """Send task commented email to creator.

    Args:
        task_id (int): Task ID.

    """
    # get necessary data
    task = Task.query.get(task_id)
    creator = task.creator
    g.user = creator  # set user for localeselector
    link = "{base}{task_id}".format(base=current_app.config['TASK_DETAIL_PAGE'],
                                    task_id=task.task_id)

    # send email
    msg = create_message(creator.email_address, _('Task commented'),
                         'mailer/task_commented', creator=creator, task=task, link=link)
    logger.debug('Sending mail to {}'.format(msg.recipients[0]))
    mail.send(msg)

    refresh()  # reset current locale
