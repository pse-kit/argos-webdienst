{{ _('Dear %(name)s', name=creator.name) }},

{{ _('the task "%(task_name)s" has been denied', task_name=task.name) }}.
{{ _('Task details can be found under the following link') }}:
{{ link }}


----------------------------
{{ _('This email was sent automatically by ARGOS') }}.
