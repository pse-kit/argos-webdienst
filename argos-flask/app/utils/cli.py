"""This module defines custom command-line commands."""

import os
import re
import sys

import click


def register(app):
    """Register commands in application instance.

    Args:
        app (flask.Flask): The flask app to initialise with.

    """
    @app.cli.command('test')
    @click.option('--coverage/--no-coverage', default=False,
                  help='Run tests under code coverage.')
    def test(coverage):  # pragma: no cover
        """Run the unit tests."""
        if coverage and not os.environ.get('FLASK_COVERAGE'):
            import subprocess
            os.environ['FLASK_COVERAGE'] = '1'
            sys.exit(subprocess.call(sys.argv))

        import unittest
        tests = unittest.TestLoader().discover('tests')
        result = unittest.TextTestRunner(verbosity=2).run(tests)

        from argos import COV
        if COV:
            COV.stop()
            COV.save()
            print('\nCoverage Summary:')
            basedir = os.path.dirname(os.path.realpath('argos.py'))
            COV.report()
            covdir = os.path.join(basedir, 'coverage')
            COV.html_report(directory=covdir)
            print('HTML version: file://%s/index.html' % covdir)
            COV.erase()

        # check if all tests have passed
        sys.exit(not result.wasSuccessful())

    @app.cli.command('add_admin')
    @click.argument('first_name')
    @click.argument('last_name')
    @click.argument('email_address')
    @click.argument('username')
    @click.argument('password')
    def add_admin(first_name, last_name, email_address, username, password):
        """Create a new admin.

        Args:
            first_name (str): First name.
            last_name (str): Last name.
            email_address (str): Email address.
            username (str): Username for authentication.
            password (str): Password of admin.

        Raises:
            ValueError: Invalid name, email address or username.

        """
        from app.models.models import Admin
        name = f"{first_name} {last_name}"

        # validate input
        try:
            if not re.match(r"(^[a-zA-Z ,.'-]+$)", name):
                raise ValueError('Error: Invalid name!')
            if not re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email_address):
                raise ValueError('Error: Invalid Email Address!')
            if Admin.query.filter_by(username=username).first() is not None:
                raise ValueError('Error: Username is already taken!')

        except ValueError as e:
            print(str(e))
            return

        admin = Admin(name=name, email_address=email_address,
                      username=username, password=password)
        admin.save()
        print('Admin creation successful')

    @app.cli.command('clear_expired_tokens')
    def clear_expired_tokens():
        """Delete all expired tokens in database."""
        from app.models.models import Token
        Token.delete_expired_tokens()
        print('Deletion of expired tokens successful')

    @app.cli.group()
    def translate():  # pragma: no cover
        """Translation and localization commands."""
        pass

    @translate.command()
    @click.argument('lang')
    def init(lang):  # pragma: no cover
        """Initialize a new language."""
        if os.system('pybabel extract -F babel.cfg -o messages.pot .'):
            raise RuntimeError('extract command failed')  # non zero exit code
        if os.system('pybabel init -i messages.pot -d app/translations -l ' + lang):
            raise RuntimeError('init command failed')  # non zero exit code
        os.remove('messages.pot')

    @translate.command()
    def update():  # pragma: no cover
        """Update all languages."""
        if os.system('pybabel extract -F babel.cfg -o messages.pot .'):
            raise RuntimeError('extract command failed')  # non zero exit code
        if os.system('pybabel update -i messages.pot -d app/translations'):
            raise RuntimeError('update command failed')  # non zero exit code
        os.remove('messages.pot')

    @translate.command()
    def compile():  # pragma: no cover
        """Compile all languages."""
        if os.system('pybabel compile -d app/translations'):
            raise RuntimeError('compile command failed')
