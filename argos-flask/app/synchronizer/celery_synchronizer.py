"""This module contains the Synchronizer celery task."""

from celery.utils.log import get_task_logger

from app import celery, synchronizer

logger = get_task_logger(__name__)


@celery.task
def sync_with_celery():
    """Run the Synchronizer with Celery."""
    logger.info('Start Synchronizer')
    synchronizer.synchronize_ldap_to_db()
    logger.info('Synchronizer task completed')
