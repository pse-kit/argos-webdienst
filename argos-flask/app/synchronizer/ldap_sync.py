"""ldap_sync module. Needed to synchronize users/groups/memberships in LDAP to database."""

import logging

import ldap3

log = logging.getLogger(__name__)


class Synchronizer():
    """This class represents the Synchronizer."""

    def __init__(self, app=None, ldap3_manager=None):
        """Initialize the Synchronizer.

        Args:
            app (flask.Flask, optional): Defaults to None. The flask app to initialize with.
            ldap3_manager (flask_ldap3_login.LDAP3LoginManager, optional): Defaults to None.
                The LDAP3 Manager to initialize with.
        """
        if app is not None and ldap3_manager is not None:  # pragma: no cover
            self.init_app(app, ldap3_manager)

    def init_app(self, app, ldap3_manager):
        """Initialize the Synchronizer with the flask app and the LDAP3 Manager.

        Args:
            app (flask.Flask, optional): Defaults to None. The flask app to initialize with.
            ldap3_manager (flask_ldap3_login.LDAP3LoginManager, optional): Defaults to None.
                The LDAP3 Manager to initialize with.

        """
        # get config variables
        self._bind_user = app.config['LDAP_BIND_USER_DN']
        self._user_object_filter = app.config['LDAP_USER_OBJECT_FILTER']
        self._sync_user_username_attr = app.config['SYNC_USER_USERNAME_ATTR']
        self._sync_group_name_attr = app.config['SYNC_GROUP_NAME_ATTR']

        self._ldap3_manager = ldap3_manager

    def get_all_users(self):
        """Get dn (distinguished name) for all LDAP users.

        Returns:
            list: Distinguished names as list.

        """
        try:
            conn = self._ldap3_manager.connection
            conn.bind()  # connect
            log.debug("Bound to LDAP as '{}'".format(conn.extend.standard.who_am_i()))

            conn.search(search_base=self._ldap3_manager.full_user_search_dn,
                        search_filter=self._user_object_filter)
            log.debug(conn.response)

            results = []
            # filter dn (distinguished names) in response
            for item in conn.response:
                if 'type' not in item or item.get('type') != 'searchResEntry':  # pragma: no cover
                    # don't return non-entry results
                    continue

                user_data = item['dn']
                results.append(user_data)

            return results

        except ldap3.core.exceptions.LDAPInvalidCredentialsResult:
            log.error("Authentication was not successful for user '{}'".format(self._bind_user))
            raise

    def get_user_info(self, dn):
        """Get attributes of user at dn in LDAP.

        Args:
            dn (str): The dn of the user to find.

        Returns:
            dict: Dictionary of the user info.

        """
        log.debug('Getting info about user {}'.format(dn))
        return self._ldap3_manager.get_user_info(dn)

    def get_user_groups(self, dn):
        """Get all groups in LDAP the user at dn is a member of.

        Args:
            dn (str): dn of user to find memberships for.

        Returns:
            list: List of all LDAP groups the user is a member of.

        """
        return self._ldap3_manager.get_user_groups(dn)

    def synchronize_ldap_to_db(self):
        """Synchronize LDAP users/groups/memberships to database."""
        from app.models.schemas import UserSchemaLDAP, GroupSchemaLDAP
        from app.models.models import User, Group
        # get all users and groups
        users = self.get_all_users()

        user_schema = UserSchemaLDAP()
        group_schema = GroupSchemaLDAP()

        for user_dn in users:
            user_info = self.get_user_info(user_dn)
            try:
                username = user_info[self._sync_user_username_attr][0]

                user_in_database = User.query.filter_by(username=username).first()

                if user_in_database is None:
                    # user is currently not in database
                    log.debug("Add user {} to db".format(username))
                    user_in_database = user_schema.make_user(user_info)
                    user_in_database.save()

                # get all groups the user is a member of
                user_groups = self.get_user_groups(user_dn)
                for group in user_groups:
                    try:
                        group_name = group[self._sync_group_name_attr][0]

                        # ---- begin: check if group is in db ----
                        group_in_database = Group.query.filter_by(name=group_name).first()

                        if group_in_database is None:
                            # group is currently not in database
                            log.debug("Add group {} to db".format(group_name))
                            group_in_database = group_schema.make_group(group)
                        # ---- end: check if group is in db ----

                        # ---- begin: check if user is group member in db ----
                        query = user_in_database.groups.filter_by(name=group_name).first()

                        if query is None:
                            # membership is not in database
                            log.debug("Add user {} to group {}".format(username, group_name))
                            group_in_database.users.append(user_in_database)
                            group_in_database.save()
                        # ---- end: check if user is group member in db ----

                    except KeyError as e:
                        # required LDAP-attribute is missing
                        log.error("Error creating group {info} (missing attribute): {e}".format(
                            info=group, e=repr(e)))
                        continue

            except KeyError as e:
                # required LDAP-attribute is missing
                log.error("Error creating user {info} (missing attribute): {e}".format(
                    info=user_info, e=repr(e)))
                continue
