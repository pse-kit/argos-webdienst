"""Defines all models."""

from datetime import datetime

from sqlalchemy import or_, and_
from sqlalchemy.orm import aliased
from werkzeug.security import generate_password_hash, check_password_hash

from app import db
from app.models.helper import users_groups


class BaseMixin(object):
    """This class contains behavior that is needed in almost all models."""

    def save(self):
        """Save object in database. For this purpose, the object is added to the db.session."""
        db.session.add(self)
        db.session.commit()


class Admin(db.Model, BaseMixin):
    """This class represents the admin model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        admin_id (int): Unique ID of the admin.
        name (str): Admin name.
        email_address (str): Email address of admin.
        username (str): Username of admin.
        password_hash (str): Salted-hash-password of admin.

    """

    __tablename__ = 'admins'

    # columns
    admin_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    email_address = db.Column(db.String(255), nullable=True)
    username = db.Column(db.String(150), nullable=False, unique=True)
    password_hash = db.Column(db.String(128), nullable=True)

    @property
    def password(self):
        """Raise error when trying to access password.

        Raises:
            AttributeError: Password is not a readable attribute.

        """
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        """Set password."""
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """Verify that a given password string matches the salted-hash-password.

        Args:
            password (str): Password to check.

        Returns:
            bool: True -> correct password, False -> wrong password.

        """
        return check_password_hash(self.password_hash, password)


class Access(db.Model, BaseMixin):
    """This class represents the access model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        access_id (int): Unique ID of the access.
        access_status (str): Status of access (pending, viewed, downloaded, signed or denied).
            Default: pending
        event_date (datetime): Date of last change of access_status.
        access_order (int): Number of the signature in the order of all required signatures.
        task_id (int): ID of task the access belongs to.
        user_id (int): ID of the user who is assigned to review the task.
        group_id (int): ID of the group that is assigned to review the task.
        user (User): User who is assigned to review the task.
        group (Group): Group that is assigned to review the task.
        task (Task): Task object to which the access belongs.

    """

    __tablename__ = 'accesses'

    # columns
    access_id = db.Column(db.Integer, primary_key=True)
    access_status = db.Column(db.String(50), nullable=False, default='pending')
    event_date = db.Column(db.DateTime, nullable=True)
    access_order = db.Column(db.Integer, nullable=True)
    task_id = db.Column(db.Integer, db.ForeignKey('tasks.task_id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=True)
    group_id = db.Column(db.Integer, db.ForeignKey('groups.group_id'), nullable=True)

    # relationships
    user = db.relationship('User', lazy='joined')
    group = db.relationship('Group', lazy='joined')

    access_status_values = ['pending', 'viewed', 'downloaded', 'signed', 'denied']


class Comment(db.Model, BaseMixin):
    """This class represents the comment model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        comment_id (int): Unique ID of the comment.
        comment_date (datetime): Creation date.
        text (str): Comment text.
        creator_id (int): User ID of creator.
        task_id (int): ID of the task to which comment is submitted.
        creator (User): User object of creator.
        task (Task): Task object to which comment is submitted.

    """

    __tablename__ = 'comments'

    # columns
    comment_id = db.Column(db.Integer, nullable=False, primary_key=True)
    comment_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    text = db.Column(db.String(5000), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    task_id = db.Column(db.Integer, db.ForeignKey('tasks.task_id'), nullable=False)

    # relationships
    creator = db.relationship('User', lazy='joined')


class Endpoint(db.Model, BaseMixin):
    """This class represents the endpoint model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        endpoint_id (int): Unique ID of the endpoint.
        name (str): Endpoint name.
        description (str): String describing the endpoint.
        signature_type (str): Type of signature (checkbox, button, checkboxAfterDownload,
            buttonAfterDownload).
        sequential_status (bool): True -> sequential mode, False -> parallel mode.
        instantiable_status(bool): True -> instantiable, False -> uninstantiable.
        deletable_status (bool): The deletability of a completed task. True -> deletable,
            False -> undeletable.
        email_notification_status (bool): Indicates whether participants should be notified.
            True -> notification enabled, False -> notification disabled.
        allow_additional_accesses_status (bool): Indicates whether creator can add additional
            participants. True -> allowed, False -> forbidden.
        additional_access_insert_before_status (bool): Indicates where additional participants
            should be added (in sequential mode). True -> at the beginning, False -> at the end.
        fields (List of Field): Fields assigned to the endpoint.
        tasks (List of Task): Tasks created for this endpoint.
        given_accesses (List of Given_Access): Given_Access predetermined for this endpoint.

    """

    __tablename__ = 'endpoints'

    # columns
    endpoint_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(5000), nullable=True)
    signature_type = db.Column(db.String(50), nullable=False)
    sequential_status = db.Column(db.Boolean, nullable=False)
    instantiable_status = db.Column(db.Boolean, nullable=False, default=True)
    deletable_status = db.Column(db.Boolean, nullable=False)
    email_notification_status = db.Column(db.Boolean, nullable=False)
    allow_additional_accesses_status = db.Column(db.Boolean, nullable=False)
    additional_access_insert_before_status = db.Column(db.Boolean, nullable=True, default=True)

    # relationships
    fields = db.relationship('Field', backref=db.backref(
        'endpoint', lazy='joined'), lazy='dynamic')
    tasks = db.relationship('Task', backref=db.backref(
        'endpoint', lazy='joined'), lazy='dynamic')
    given_accesses = db.relationship('Given_Access', backref=db.backref(
        'endpoint', lazy='joined'), lazy='dynamic')

    signature_type_values = ['checkbox', 'button', 'checkboxAfterDownload', 'buttonAfterDownload']


class Field(db.Model, BaseMixin):
    """This class represents the field model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        field_id (int): Unique ID of the field.
        name (str): Field name.
        field_type (str): Type of field (textfield, datetime, checkbox, file).
        required_status (bool): Indicates whether this field is required. True -> required,
            False -> optional
        endpoint_id (int): ID of endpoint.
        endpoint (Endpoint): Endpoint object this field belongs to.

    """

    __tablename__ = 'fields'

    # columns
    field_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    field_type = db.Column(db.String(50), nullable=False)
    required_status = db.Column(db.Boolean, nullable=False, default=True)
    endpoint_id = db.Column(db.Integer, db.ForeignKey('endpoints.endpoint_id'), nullable=False)

    field_type_values = ['textfield', 'datetime', 'checkbox', 'file']


class Given_Access(db.Model, BaseMixin):
    """This class represents the given_access model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        given_access_id (int): Unique ID of the given_access.
        access_order (int): Number of the signature in the order of all required signatures.
        endpoint_id (int): ID of endpoint the given_access belongs to.
        user_id (int): ID of the user who is assigned to review the task.
        group_id (int): ID of the group that is assigned to review the task.
        user (User): User who is assigned to review the task.
        group (Group): Group that is assigned to review the task.
        endpoint (Endpoint): Endpoint object this given_access belongs to.

    """

    __tablename__ = 'given_accesses'

    # columns
    given_access_id = db.Column(db.Integer, primary_key=True)
    access_order = db.Column(db.Integer, nullable=True)
    endpoint_id = db.Column(db.ForeignKey('endpoints.endpoint_id'), nullable=False)
    user_id = db.Column(db.ForeignKey('users.user_id'), nullable=True)
    group_id = db.Column(db.ForeignKey('groups.group_id'), nullable=True)

    # relationships
    user = db.relationship('User', lazy='joined')
    group = db.relationship('Group', lazy='joined')


class Group(db.Model, BaseMixin):
    """This class represents the group model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        group_id (int): Unique ID of the group.
        name (str): Name of the group.
        description (str): String describing the group.
        users (List of User): Members of this group.

    """

    __tablename__ = 'groups'

    # columns
    group_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable=False, unique=True)
    description = db.Column(db.String(5000), nullable=True)


class Message(db.Model, BaseMixin):
    """This class represents the message model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        message_id (int): Unique ID of the message.
        message_date (datetime): Creation date.
        text (str): Message text.
        user_id (int): User ID of recipient.
        read_status (bool): Indicates whether recipient has read the message. True -> read,
            False -> unread.
        recipient (User): Recipient of the message as an object.

    """

    __tablename__ = 'messages'

    # columns
    message_id = db.Column(db.Integer, primary_key=True)
    message_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    text = db.Column(db.String(5000), nullable=False)
    recipient_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    read_status = db.Column(db.Boolean, nullable=False, default=False)


class Task_Field(db.Model, BaseMixin):
    """This class represents the task_field model. Contains the value of a field for a specific task.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        task_field_id (int): Unique ID of the task_field.
        value (str): Value of task_field.
        field_id (int): ID of field.
        task_id (int): ID of task the task_field belongs to.
        field (Field): Field object to which this task_field is assigned.
        task (task): Task object this task_field belongs to.

    """

    __tablename__ = 'tasks_fields'

    # columns
    task_field_id = db.Column(db.Integer, nullable=False, primary_key=True)
    value = db.Column(db.String(5000), nullable=False)
    field_id = db.Column(db.Integer, db.ForeignKey('fields.field_id'), nullable=False)
    task_id = db.Column(db.Integer, db.ForeignKey('tasks.task_id'), nullable=False)

    # relationships
    field = db.relationship('Field', lazy='joined')


class Task(db.Model, BaseMixin):
    """This class represents the task model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        task_id (int): Unique ID of the task.
        name (str): Task name.
        creator_id (int): User ID of creator of the task.
        task_status (str): Status of task (pending, withdrawn, completed, denied)
        creation_date (datetime): Creation date and time.
        endpoint_id (int): ID of endpoint.
        endpoint (Endpoint): Endpoint object this task belongs to.
        tasks_fields(List of Task_Field): Mapping of field required by endpoint to its value.
        creator (User): Creator of this task as a user object.
        comments (List of Comment): Comments submitted to this task.
        accesses (List of Access): Accesses assigned to this task as objects.

    """

    __tablename__ = 'tasks'

    # columns
    task_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))
    task_status = db.Column(db.String(50), nullable=False, default='pending')
    creation_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    endpoint_id = db.Column(db.Integer, db.ForeignKey('endpoints.endpoint_id'), nullable=False)

    # relationships
    tasks_fields = db.relationship('Task_Field', backref=db.backref(
        'task', lazy='joined'), lazy='dynamic')
    accesses = db.relationship('Access', backref=db.backref('task', lazy='joined'), lazy='dynamic')
    comments = db.relationship('Comment', backref=db.backref('task', lazy='joined'), lazy='dynamic')


class Token(db.Model, BaseMixin):
    """This class represents the token model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        token_id (int): Unique ID of the token.
        jti (str): JSON Web Token Identifier.
        token_type (str): Type of token (access or refresh).
        user_id (int): ID of user for whom the token was issued.
        revoked_status (bool): Indicates whether token was revoked. True -> revoked token,
            False -> valid token.
        expiration_date (datetime): The expiration date of the token.
        user (User): User for whom the token was issued as object.

    """

    __tablename__ = 'tokens'

    # columns
    token_id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(100), nullable=False, unique=True)
    token_type = db.Column(db.String(50), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=True)
    admin_id = db.Column(db.Integer, db.ForeignKey('admins.admin_id'), nullable=True)
    revoked_status = db.Column(db.Boolean, nullable=False)
    expiration_date = db.Column(db.DateTime, nullable=False)

    # relationships
    user = db.relationship('User', lazy='joined')
    admin = db.relationship('Admin', lazy='joined')

    @staticmethod
    def delete_expired_tokens():
        """Delete all expired tokens in database."""
        now = datetime.now()
        expired_tokens = Token.query.filter(Token.expiration_date < now).all()
        for token in expired_tokens:
            db.session.delete(token)
        db.session.commit()


class User(db.Model, BaseMixin):
    """This class represents the user model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        user_id (int): Unique ID of the user.
        name (str): Name of user.
        email_address (str): Email address of user.
        email_notification_status (bool): Indicates whether user receives e-mail notifications.
            True -> e-mail notifications turned on, False -> e-mail notifications turned off
        username (str): Username of user.
        language (str): Language of user.
        tasks (List of Task): Tasks created by user.
        groups (List of Group): Groups the suser is a member of.
        messages (List of Message): Messages assigned to this user,

    """

    __tablename__ = 'users'

    # columns
    user_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    email_address = db.Column(db.String(255), nullable=False)
    email_notification_status = db.Column(db.Boolean, nullable=False, default=True)
    username = db.Column(db.String(150), nullable=False, unique=True)
    language = db.Column(db.String(10), nullable=False, default='de')

    # relationships
    tasks = db.relationship('Task', backref='creator', lazy='dynamic')
    groups = db.relationship('Group', secondary=users_groups,
                             backref=db.backref('users', lazy='dynamic'), lazy='dynamic')
    messages = db.relationship('Message', backref=db.backref(
        'recipient', lazy='joined'), lazy='dynamic')

    def __eq__(self, other):
        """Evaluate if this user is equal to other given user.

        Args:
            other (User): Other user to compare to.

        Returns:
            bool: True -> users are equal, False -> users are not equal

        """
        return self.user_id == other.user_id

    def __hash__(self):
        """Hashes this user.

        Returns:
            int: Hash value of this user.

        """
        return hash((self.user_id))

    def get_unsigned_accesses_to_view(self, task):
        """Get all accesses which are next to sign involving this user of a task.

        Args:
            task (Task): Task of requested accesses.

        Returns:
            List of Access: All accesses which are next to sign involving this user.
                            **Note**: List has length 1 if mode is sequential.

        """
        # return empty list if task is not pending
        if task.task_status != 'pending':
            return []

        # sqlalchemy does not support _in with relations (yet), work around: use foreign IDs as keys
        user_groups = [group.group_id for group in self.groups.all()]

        # get query of unsigned accesses
        unsigned_accesses_query = task.accesses.filter(Access.access_status != 'signed')
        if not task.endpoint.sequential_status:
            # parallel task: all unsigned accesses involving this user can be viewed
            accesses = unsigned_accesses_query.filter(or_(Access.user == self,
                                                          Access.group_id.in_(user_groups))).all()
        else:
            # sequential task: if next access involves this user, this task can be viewed
            next_access = unsigned_accesses_query.order_by(Access.access_order).first()
            if not next_access:
                return []
            if next_access.user_id == self.user_id or next_access.group in self.groups:
                accesses = [next_access]
            else:
                return []

        if task.creator != self:
            return accesses
        else:
            # creator is not allowed to sign an access which can be signed by someone else
            creator_accesses = []
            for access in accesses:
                if (access.user and not access.group) or len(access.group.users.all()) == 1:
                    creator_accesses.append(access)
            return creator_accesses

    def get_accesses_to_sign(self, task):
        """Get all accesses of task that can be signed by this user.

        If task has to be downloaded first, check if requesting user has downloaded it.

        Args:
            task (Task): Task of requested accesses.

        Returns:
            (List of Access, Boolean or 'afterDownload'): Accesses that can be signed by this user
                and if user can sign it now (True), after download ('afterDownload')
                or not at all (False).

        """
        accesses = self.get_unsigned_accesses_to_view(task)
        return (accesses, self.can_sign(task, accesses))

    def can_sign(self, task, accesses):
        """Check if signing conditions are met.

        Args:
            task (Task): Task to sign.
            accesses (List of Access): Accesses the user is next to sign.

        Returns:
            bool or str: True -> user can sign the task, False -> user cannot sign the task. User
                cannot sign the task if a string is returned, the string may specify the cause.

        """
        # no accesses that can be viewed
        if not accesses:
            return False

        # check conditions of specified signature_type
        if task.endpoint.signature_type in ['checkboxAfterDownload', 'buttonAfterDownload']:
            for access in accesses:
                if access.user_id == self.user_id and access.access_status == 'downloaded':
                    return True
            else:
                return 'afterDownload'
        else:
            return True

    def sign_task(self, task):
        """Sign task.

        Args:
            task (Task): Task to be signed.

        Returns:
            bool: True -> status of accesses successfully set to 'signed'

        """
        (accesses, can_sign) = self.get_accesses_to_sign(task)
        if can_sign is not True:
            return False
        self.update_accesses(accesses, 'signed')
        return True

    def deny_task(self, task):
        """Deny task.

        Args:
            task (Task): Task to be denied.

        Returns:
            bool: True -> status of accesses successfully set to 'denied'

        """
        accesses = self.get_unsigned_accesses_to_view(task)
        if not accesses:
            return False
        self.update_accesses(accesses, 'denied')
        return True

    def download_task(self, task):
        """Download task.

        Args:
            user (User): User that downloads task.

        Returns:
            bool: True -> status of accesses successfully set to 'downloaded'

        """
        accesses = self.get_unsigned_accesses_to_view(task)
        if accesses:
            self.update_accesses(accesses, 'downloaded')
        return True

    def view_task(self, task):
        """View task. Only affects pending accesses.

        Args:
            user (User): User that views task.

        Returns:
            bool: True -> status of accesses successfully set to 'viewed'

        """
        accesses = self.get_unsigned_accesses_to_view(task)
        accesses_to_update = []
        # update access if status is pending or another member of the group has downloaded task
        for access in accesses:
            if access.access_status == 'pending':
                accesses_to_update.append(access)
            elif access.user != self:
                accesses_to_update.append(access)
        self.update_accesses(accesses_to_update, 'viewed')
        return True

    def update_accesses(self, accesses, event):
        """Update given accesses according to event.

        Args:
            accesses (List of Access): Accesses to update.
            event (str): Event that triggers updating.
                        Required to be 'signed', 'denied', 'viewed' or 'downloaded'
        """
        for access in accesses:
            access.access_status = event
            access.user = self
            access.event_date = datetime.utcnow()
            access.save()

    def get_query_tasks_owned(self):
        """Get query for all active tasks created by this user.

        Returns:
            Query: A query object for all created tasks of a user.

        """
        return self.tasks.filter_by(task_status='pending')

    def get_query_tasks_to_sign(self):
        """Get query for all tasks that can be signed by this user.

        Returns:
            Query: A query object for all tasks that can be signed by requesting user.

        """
        # sqlalchemy does not support _in with relations (yet), work around: use foreign IDs as keys
        group_ids = [group.group_id for group in self.groups.all()]

        # get all groups in which this user is the only member
        group_single_id = []
        for group in self.groups:
            if len(group.users.all()) == 1:
                group_single_id.append(group.group_id)

        # get all tasks the user will be able to sign
        involved = Task.query.filter(Task.task_status == 'pending').join(Task.accesses) \
                             .filter(or_(and_(Task.creator != self,
                                              and_(Access.access_status != 'signed',
                                                   or_(Access.user == self,
                                                       Access.group_id.in_(group_ids)))),
                                         and_(Task.creator == self,
                                              and_(Access.access_status != 'signed',
                                                   or_(and_(Access.user == self,
                                                            Access.group == None),  # noqa: E711
                                                       Access.group_id.in_(group_single_id))))))

        # all parallel tasks can be signed directly
        query_par = involved.filter(Task.endpoint.has(sequential_status=False))

        # user can sign sequential tasks if the access before has been signed (or if he is first)
        alias_access = aliased(Access)
        query_seq = involved.filter(Task.endpoint.has(sequential_status=True)) \
                            .join(alias_access, Task.accesses) \
                            .filter(or_(Access.access_order == 0,
                                    and_(alias_access.access_order == Access.access_order - 1,
                                         alias_access.access_status == 'signed')))
        return query_seq.union(query_par)

    def get_query_tasks_archived(self):
        """Get query for all archived tasks of this user.

        Archived tasks are tasks already signed by the user or another user of a group the
        requesting user is a member of or finished tasks created by this user.
        Only tasks, which are not withdrawn, can be archived.

        Returns:
            Query: A query object for all archived tasks for a user.

        """
        # sqlalchemy does not support _in with relations (yet), work around: use foreign IDs as keys
        group_ids = [group.group_id for group in self.groups.all()]

        task_query = Task.query.filter(Task.task_status != 'withdrawn') \
                         .filter(or_(and_(Task.creator == self,
                                          Task.task_status.in_(['completed', 'denied'])),
                                     Task.accesses.any(and_(or_(Access.user == self,
                                                                Access.group_id.in_(group_ids)),
                                                       Access.access_status.in_(['signed',
                                                                                'denied'])))))

        return task_query
