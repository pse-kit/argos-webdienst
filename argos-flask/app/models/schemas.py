"""Defines all marshmallow schemas for models."""

import dateutil.parser
from flask import current_app
from marshmallow import (ValidationError, fields, pre_load, post_dump, post_load,
                         validates_schema)
from marshmallow.validate import OneOf
from marshmallow_sqlalchemy import field_for

from app import ma
from app.models.models import (Access, Comment, Endpoint, Field, Given_Access,
                               Group, Message, Task, Task_Field, User)


class BaseSchema(ma.ModelSchema):
    """This class contains behavior needed in all schemas."""

    def handle_error(self, exc, data):
        """Raise custom BadRequest error if validation fails."""
        from app.api.v1.errors import BadRequest

        message = exc.messages['_schema'] if '_schema' in exc.messages else exc.messages
        raise BadRequest(message)


class Validator:
    """This class defines methods to validate the marshmallow schemas."""

    @staticmethod
    def user_exists(user_id):
        """Validate that user with given user_id exists.

        Args:
            user_id (int): Given user_id to validate.
        Raises:
            ValidationError: If user with given user_id does not exist.

        """
        if User.query.get(user_id) is None:
            raise ValidationError('User not found')

    @staticmethod
    def group_exists(group_id):
        """Validate that group with given group_id exists.

        Args:
            group_id (int): Given group_id to validate.
        Raises:
            ValidationError: If group with given group_id does not exist.

        """
        if Group.query.get(group_id) is None:
            raise ValidationError('Group not found')

    @staticmethod
    def endpoint_exists(endpoint_id):
        """Validate that endpoint with given endpoint_id exists.

        Args:
            endpoint_id (int): Given endpoint_id to validate.
        Raises:
            ValidationError: If endpoint with given endpoint_id does not exist.

        """
        if Endpoint.query.get(endpoint_id) is None \
                or not Endpoint.query.get(endpoint_id).instantiable_status:
            raise ValidationError('Endpoint not found')

    @staticmethod
    def field_exists(field_id):
        """Validate that field with given field_id exists.

        Args:
            field_id (int): Given field_id to validate.
        Raises:
            ValidationError: If field with given field_id does not exist.

        """
        if Field.query.get(field_id) is None:
            raise ValidationError('Field not found')

    @staticmethod
    def task_exists(task_id):
        """Validate that task with given task_id exists.

        Args:
            task_id (int): Given task_id to validate.
        Raises:
            ValidationError: If task with given task_id does not exist.

        """
        if Task.query.get(task_id) is None:
            raise ValidationError('Task not found')


class UserSchema(BaseSchema):
    """Defines a marshmallow schema for the user model.

    Dumped schema contains keys: user_id, name, email_address.

    Args:
        BaseSchema (Schema): Basic schema class.
    """

    user_id = field_for(User, 'user_id', dump_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = User
        fields = ('user_id', 'name', 'email_address')


class UserSchemaLDAP():
    """Defines user schema for Synchronizer."""

    def make_user(self, data):
        """Convert a JSON from LDAP to a user object.

        Args:
            data (dict): JSON String.

        Returns:
            User: Created user object.

        """
        sync_user_name_attr = current_app.config['SYNC_USER_NAME_ATTR']
        sync_user_username_attr = current_app.config['SYNC_USER_USERNAME_ATTR']
        return User(name=data[sync_user_name_attr][0], email_address=data['mail'][0],
                    username=data[sync_user_username_attr][0])


class GroupSchema(BaseSchema):
    """Defines a marshmallow schema for the group model.

    Dumped schema contains keys: group_id, name, description

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    group_id = field_for(Group, 'group_id', dump_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Group
        fields = ('name', 'group_id', 'description')


class GroupSchemaLDAP():
    """Defines group schema for Synchronizer."""

    def make_group(self, data):
        """Convert a JSON String from LDAP to a group object.

        Args:
            data (dict): JSON String.

        Returns:
            Group: Created group object.

        """
        sync_group_name_attr = current_app.config['SYNC_GROUP_NAME_ATTR']
        if 'description' in data:
            return Group(name=data[sync_group_name_attr][0], description=data['description'][0])
        else:
            return Group(name=data[sync_group_name_attr][0])


class AccessSchema(BaseSchema):
    """Defines a marshmallow schema for the access model.

        Dumped schema contains keys: user, group, access_order, access_status, event_date
        Loading schema needs keys: [access_order] (only if sequential), either user_id or group_id
        The reference to the task has to be set after loading if this schema is loaded directly
        (not as nested schema).

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    user = fields.Nested(UserSchema, dump_only=True)
    group = fields.Nested(GroupSchema, dump_only=True)
    access_status = field_for(Access, 'access_status', dump_only=True)
    user_id = field_for(Access, 'user_id', validate=Validator.user_exists, load_only=True)
    group_id = field_for(Access, 'group_id', validate=Validator.group_exists, load_only=True)
    event_date = field_for(Access, 'event_date', dump_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Access
        fields = ('user', 'group', 'access_status', 'user_id', 'group_id', 'event_date',
                  'access_order')

    @validates_schema
    def validates_schema(self, data):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If group_id and user_id are both set or if neither is set.

        """
        if ('group_id' in data and data['group_id'] is not None) == ('user_id' in data and
                                                                     data['user_id'] is not None):
            raise ValidationError('Either user or group has to be set.')


class GivenAccessSchema(BaseSchema):
    """Defines a marshmallow schema for the given access model.

        Dumped schema contains keys: user, group, access_order
        Loading schema needs keys: user_id, group_id, [access_order] (only if sequential)
        The reference to the endpoint has to be set after instantiation if this schema
        is loaded directly (not as nested schema).

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    user = fields.Nested(UserSchema, dump_only=True)
    group = fields.Nested(GroupSchema, dump_only=True)
    user_id = field_for(Given_Access, 'user_id', validate=Validator.user_exists, load_only=True)
    group_id = field_for(Given_Access, 'group_id', validate=Validator.group_exists, load_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Given_Access
        fields = ('user', 'group', 'user_id', 'group_id', 'access_order')

    @validates_schema
    def validates_schema(self, data):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If group_id and user_id are both set or if neither is set.

        """
        if ('group_id' in data and data['group_id'] is not None) == ('user_id' in data and
                                                                     data['user_id'] is not None):
            raise ValidationError('Either user or group has to be set.')


class CommentSchema(BaseSchema):
    """Defines a marshmallow schema for the comment model.

        Dumped schema contains keys: comment_id, comment_date, text, creator, task_id
        Loading schema needs keys: text, creator_id, task_id

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    creator = fields.Nested(UserSchema, dump_only=True)
    creator_id = field_for(Comment, 'creator_id', load_only=True, validate=Validator.user_exists)
    comment_id = field_for(Comment, 'comment_id', dump_only=True)
    comment_date = field_for(Comment, 'comment_date', dump_only=True)
    task_id = field_for(Comment, 'task_id', validate=Validator.task_exists)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Comment
        fields = ('creator', 'creator_id', 'comment_id', 'comment_date', 'text', 'task_id')


class FieldSchema(BaseSchema):
    """Defines a marshmallow schema for the field model.

        Dumped schema contains keys: field_id, name, field_type, required_status
        Loading schema needs keys: name, field_type, [required_status]
        The reference to the endpoint has to be set after instantiation if this schema
        is loaded directly (not as nested schema).

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    field_id = field_for(Field, 'field_id', dump_only=True)
    field_type = field_for(Field, 'field_type', validate=OneOf(Field.field_type_values))
    required_status = field_for(Field, 'required_status', missing=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Field
        fields = ('field_id', 'name', 'field_type', 'required_status')


class TaskFieldSchema(BaseSchema):
    """Defines a marshmallow schema for the task_field model.

        Dumped schema contains keys: name, field_type (of associated field), value
        Loading schema needs keys: field_id, value
        The reference to the creator and the task has to be set after instantiation if this schema
        is loaded directly (not as nested schema).

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    field = fields.Nested(FieldSchema, only=['field_type', 'name'], dump_only=True)
    field_id = field_for(Task_Field, 'field_id', validate=Validator.field_exists, load_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Task_Field
        fields = ('field', 'field_id', 'value')

    @validates_schema
    def validate_schema(self, data):
        """Check if values of field match field_type.

        Args:
            data (dict of str : obj): Schema to validate.

        """
        field = Field.query.get(data['field_id'])
        if field.field_type == 'checkbox' and data['value'] not in ('true', 'false'):
            raise ValidationError('Value does not match field_type')
        elif field.field_type == 'file':
            raise ValidationError('File fields are not allowed.')
        elif field.field_type == 'datetime':
            try:
                dateutil.parser.parse(data['value'])
            except ValueError:
                raise ValidationError('Date format of field invalid, use ISO-8601.')


class EndpointSchema(BaseSchema):
    """Defines a marshmallow schema for the endpoint model.

    Dumped schema contains keys: endpoint_id, name, description, sequential_status,
        deletable_status, instantiable_status, email_notification_status,
        signature_type, allow_additional_accesses_status, given_accesses, fields,
        additional_access_insert_before_status, active_tasks

    Loading schema needs keys: name, [description], sequential_status,
        deletable_status, instantiable_status, email_notification_status,
        signature_type, allow_additional_accesses_status, [additional_access_insert_before_status],
        given_accesses, fields

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    endpoint_id = field_for(Endpoint, 'endpoint_id', dump_only=True)
    signature_type = field_for(Endpoint, 'signature_type',
                               validate=OneOf(Endpoint.signature_type_values))
    instantiable_status = field_for(Endpoint, 'instantiable_status', dump_only=True)
    active_tasks = fields.Integer(dump_only=True)
    given_accesses = fields.Nested(GivenAccessSchema, many=True)
    fields = fields.Nested(FieldSchema, many=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Endpoint
        fields = ('name', 'description', 'signature_type', 'instantiable_status',
                  'sequential_status', 'deletable_status', 'email_notification_status',
                  'allow_additional_accesses_status', 'additional_access_insert_before_status',
                  'endpoint_id', 'given_accesses', 'fields', 'active_tasks')

    @pre_load
    def remove_empty_and_none(self, data):
        """Remove empty lists and none values to prevent loading not required attributes which are none.

        Args:
            data (dict of str : obj): Data to modify.

        Returns:
            dict of str : obj: Modified data.

        """
        return remove_empty_lists_none_values(data)

    @validates_schema
    def validate_schema(self, data):
        """Validate schema.

        Args:
            data (dict of str: obj): Data to validate.

        """
        self.validate_given_accesses(data)
        self.validate_fields(data)

    def validate_fields(self, data):
        """Validate fields of schema.

        Args:
            data (dict of str : obj): Data to validate.

        Raises:
            ValidationError: If more than one field of type file is specified or if no such field
                             is specified with afterDownload as signature_type.

        """
        # get fields of type file
        file_fields = ([field for field in data['fields'] if field.field_type == 'file']
                       if 'fields' in data else [])
        if len(file_fields) > 1:
            raise ValidationError('An endpoint can only define one field of type file.')
        if data['signature_type'] in ['checkboxAfterDownload', 'buttonAfterDownload']:
            # endpoints with signature_type afterDownload must have a required field of type file
            if len(file_fields) == 0 or not file_fields[0].required_status:
                raise ValidationError('A field of type file is required.')

    def validate_given_accesses(self, data):
        """Validate given_accesses before loading.

           Check if given_accesses have attribute access_order if mode of endpoint is sequential.
           Access_order has to be unique and between 1 and number of given_accesses.

        Args:
            data (dict Str : obj): Given schema to validate.

        Raises:
            ValidationError: If additional accesses are not allowed but no accesses are given.
                             No access_order is defined, access_order is invalid (sequential mode).

        """
        if not data['allow_additional_accesses_status'] and 'given_accesses' not in data:
            # No accesses are allowed at all.
            raise ValidationError('No accesses possible.')
        if data['sequential_status'] is True and 'given_accesses' in data:
            # check if given access_order is valid for sequential endpoints
            order = []
            for given_access in data['given_accesses']:
                if given_access.access_order is None:
                    raise ValidationError('Order of given accesses has to be defined')
                order.append(given_access.access_order)
            if set(order) != set(range(0, len(order))):
                raise ValidationError('Access order invalid.')

    @post_dump
    def add_active_tasks(self, data):
        """Add number of active tasks to dumped endpoint."""
        data['active_tasks'] = len(Task.query.filter_by(task_status='pending')
                                   .filter_by(endpoint_id=data['endpoint_id']).all())
        return data

    @post_dump(pass_original=True)
    def sort_given_accesses(self, data, orignal_data):
        """Sort given accesses in dumped endpoint by access order (sequential endpoint).

        Args:
            data (dict of str : obj): Dictionary containing dumped endpoint.
            orignal_data (Endpoint): Endpoint that has been dumped.

        Returns:
            dict of str : obj: Given data with sorted given accesses.

        """
        if orignal_data.sequential_status and 'given_accesses' in data:
            data['given_accesses'].sort(key=lambda access: access['access_order'])
        return data


class TaskSchema(BaseSchema):
    """Defines a marshmallow schema for the task model.

        Dumped schema contains keys: task_id, endpoint_id, name, creator, accesses, tasks_fields,
            creation_date, task_status

        Loading schema needs keys: endpoint_id, name, creator_id, accesses, tasks_fields,
            task_status

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    task_id = field_for(Task, 'task_id', dump_only=True)
    creator = fields.Nested(UserSchema, dump_only=True)
    creator_id = field_for(Task, 'creator_id', load_only=True, validate=Validator.user_exists)
    endpoint_id = field_for(Task, 'endpoint_id', validate=Validator.endpoint_exists)
    task_status = field_for(Task, 'task_status', dump_only=True)
    creation_date = field_for(Task, 'creation_date', dump_only=True)
    accesses = fields.Nested(AccessSchema, many=True)
    tasks_fields = fields.Nested(TaskFieldSchema, many=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Task
        fields = ('task_id', 'creator_id', 'creator', 'task_status', 'accesses', 'tasks_fields',
                  'endpoint_id', 'name', 'creation_date')

    @pre_load
    def remove_empty_and_none(self, data):
        """Remove empty lists and none values to prevent loading not required attributes which are none.

        Args:
            data (dict of str : obj): Data to modify.

        Returns:
            dict of str : obj: Modified data.

        """
        return remove_empty_lists_none_values(data)

    @validates_schema
    def validate_schema(self, data):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If data contains errors.

        """
        endpoint = Endpoint.query.get(data['endpoint_id'])
        if 'tasks_fields' in data:
            # validate given tasks_fields
            if not self.validate_tasks_fields(data['tasks_fields'], endpoint):
                raise ValidationError('Given fields do not match endpoint definition.')
        else:
            # Error if no tasks_fields are defined but endpoints has required fields (not files)
            endpoint_fields = endpoint.fields.filter_by(required_status=True) \
                                             .filter(Field.field_type != 'file').first()
            if endpoint_fields is not None:
                raise ValidationError('Given fields do not match endpoint definition.')

        if 'accesses' in data:
            # check that user does not assign himself to review a task
            for access in data['accesses']:
                if access.user_id and access.user_id == data['creator_id']:
                    raise ValidationError('You cannot assign yourself to review a task')
                elif access.group_id:
                    # user cannot assign a group of which he is the only member to sign a task
                    group = Group.query.get(access.group_id)
                    if len(group.users.all()) == 1 and (group.users.first().user_id ==
                                                        data['creator_id']):
                        raise ValidationError('You cannot assign yourself to review a task')

            # check if accesses match endpoint
            if not self.validate_accesses(data['accesses'], endpoint):
                raise ValidationError('Accesses do not match definition of endpoint.')
        elif not endpoint.given_accesses.all():
            # tasks with no accesses are not allowed
            raise ValidationError('No accesses defined.')

    @pre_load
    def remove_access_order_parallel(self, data):
        """Remove all access_orders for a parallel task before validation.

        Args:
            data (dict of str : obj): Data of task to load. Nested fields are already objects.

        Returns:
            dict of str : obj: Modified data.

        """
        # pre_load methods are called before validation
        Validator.endpoint_exists(data['endpoint_id'])
        endpoint = Endpoint.query.get(data['endpoint_id'])
        if not endpoint.sequential_status:
            if 'accesses' in data:
                for access in data['accesses']:
                    if 'access_order' in access:
                        access.pop('access_order')
        return data

    @pre_load
    def remove_fields_value_none(self, data):
        """Remove fields with none value before loading to ensure that those are ignored.

        Args:
            data (dict of str : obj): Data to modify.

        Returns:
            dict of str : obj: Modified data.

        """
        if 'tasks_fields' in data:
            for task_field in data['tasks_fields']:
                if 'value' in task_field and not task_field['value']:
                    data['tasks_fields'].remove(task_field)
        return data

    @post_load
    def make_instance(self, data):
        """Create a new Task according to specification in given data.

        Args:
            data (dict of str : obj): Specified attributes of a Task.

        Returns:
            Task: Created task.

        """
        task = super().make_instance(data)
        endpoint = Endpoint.query.get(data['endpoint_id'])
        self.add_given_accesses(task, endpoint)
        return task

    @post_dump(pass_original=True)
    def sort_accesses(self, data, orignal_data):
        """Sort accesses in dumped task by access order (sequential tasks).

        Args:
            data (dict of str : obj): Dictionary containing dumped task.
            orignal_data (Task): Task that has been dumped.

        Returns:
            dict of str : obj: Given data with sorted accesses.

        """
        if orignal_data.endpoint.sequential_status and 'accesses' in data:
            data['accesses'].sort(key=lambda access: access['access_order'])
        return data

    def add_given_accesses(self, task, endpoint):
        """Add given accesses specified by endpoint to accesses of task.

        Adjust access_order according to additional_access_insert_before_status of endpoint.
        No access_order is specified in parallel mode.

        Args:
            task (Task): Task accesses to add to.
            endpoint (Endpoint): Endpoint belonging to given task.

        """
        # parallel mode
        if not endpoint.sequential_status:
            for given_access in endpoint.given_accesses.all():
                Access(user=given_access.user, group=given_access.group, task=task, access_order=0)
        else:
            # sequential mode, given_accesses added after accesses of task
            if endpoint.additional_access_insert_before_status:
                access_order_offset = len(task.accesses.all())
                for given_access in endpoint.given_accesses.all():
                    Access(user=given_access.user, group=given_access.group, task=task,
                           access_order=given_access.access_order + access_order_offset)
            # sequential mode, given_accesses added before accesses of task
            else:
                access_order_offset = len(endpoint.given_accesses.all())
                for access in task.accesses.all():
                    access.access_order += access_order_offset
                for given_access in endpoint.given_accesses.all():
                    Access(user=given_access.user, group=given_access.group, task=task,
                           access_order=given_access.access_order)

    def validate_tasks_fields(self, tasks_fields, endpoint):
        """Check if tasks_fields belong to endpoint.

        Args:
            accesses (list of Task_Field): Not empty list of Task_Fields to validate.
            endpoint (Endpoint): Endpoint to validate against.

        Returns:
            bool: True -> validation successful, False -> validation unsuccessful

        """
        # get ids of tasks_fields of this task
        tasks_fields_id = [task_field.field_id for task_field in tasks_fields]
        set_tasks_fields_id = set(tasks_fields_id)

        # check if each field is only filled once
        if not len(set_tasks_fields_id) == len(tasks_fields_id):
            return False

        # check if tasks_fields belong to endpoint of task
        fields = endpoint.fields.all()
        fields_id = [field.field_id for field in fields]
        if not set_tasks_fields_id.issubset(fields_id):
            return False

        # check if all required fields are filled
        required_fields = endpoint.fields.filter_by(required_status=True) \
                                         .filter(Field.field_type != 'file').all()
        required_fields_id = [field.field_id for field in required_fields]
        if not set(required_fields_id).issubset(set_tasks_fields_id):
            return False
        return True

    def validate_accesses(self, accesses, endpoint):
        """Check if accesses match definition of endpoint.

        Args:
            accesses (list of Access): Not empty list of accesses to validate.
            endpoint (Endpoint): Endpoint to validate against.

        Returns:
            bool: True -> validation successful, False -> validation unsuccessful

        """
        # additional accesses not allowed
        if endpoint.allow_additional_accesses_status is False:
            return False

        # access order invalid (in sequential mode)
        if endpoint.sequential_status is True:
            order = []
            for access in accesses:
                if access.access_order is None:
                    return False
                order.append(access.access_order)
            if set(order) != set(range(0, len(order))):
                return False
        return True


class MessageSchema(BaseSchema):
    """Defines a marshmallow schema for the message model.

    Dumped schema contains keys: message_id, message_date, text, read_status

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    recipient_id = field_for(Message, 'recipient_id', load_only=True)
    message_id = field_for(Message, 'message_id', dump_only=True)
    message_date = field_for(Message, 'message_date', dump_only=True)
    read_status = field_for(Message, 'read_status', dump_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Message
        fields = ('recipient_id', 'message_id', 'message_date', 'text', 'read_status')


def remove_empty_lists_none_values(data):
    """Remove empty lists and none values.

    Args:
        data (dict of str : obj): Data to load.

    Returns:
        dict of str: obj: Modified data.

    """
    keys_to_remove = []
    for key in data:
        if data[key] is None or (isinstance(data[key], list) and len(data[key]) == 0):
            keys_to_remove.append(key)
    for key in keys_to_remove:
        data.pop(key)
    return data
