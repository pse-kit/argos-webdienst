"""Contains helper tables.

Simple relationships use helper tables instead of models.
"""

from app import db

# users_groups helper table
users_groups = db.Table('users_groups', db.Column('user_id', db.Integer, db.ForeignKey(
    'users.user_id')), db.Column('group_id', db.Integer, db.ForeignKey('groups.group_id')))
