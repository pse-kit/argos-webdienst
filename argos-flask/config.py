"""Configuation module.

Base class contains settings that are common to all configurations,
subclasses define specific settings.
"""

import datetime
import os

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))

# load .env file
DOTENV_PATH = os.path.join(basedir, '.env')
if os.path.exists(DOTENV_PATH):
    load_dotenv(DOTENV_PATH)


class Config:
    """Base class."""

    # Secret Key used by Flask and Flask-JWT-Extended
    # How to generate good secret keys: python -c 'import os; print(os.urandom(16))'
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'NEVER_USE_THIS_STRING_IN_PRODUCTION'

    # Disable Flask-SQLAlchemy event notification system
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Enable logging to logs/argos.log instead of STDERR
    LOG_TO_FILE = os.environ.get('LOG_TO_FILE') or False

    # The folder to which files are uploaded to
    UPLOAD_FOLDER = 'uploads'

    # The directory in which ZIPs are stored
    DOWNLOAD_FOLDER = 'files'

    # Specify the allowed file extensions
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'docx', 'xlsx', 'pptx'])

    # The maximum allowed request size
    MAX_CONTENT_LENGTH = 100 * 1024 * 1024  # 100 megabytes

    # The secret key configured in nginx.conf
    SECURE_LINK_SECRET = os.environ.get('SECURE_LINK_SECRET')

    # -------LDAP-CONFIG-------
    # Hostname of your LDAP Server
    LDAP_HOST = os.environ.get('LDAP_HOST') or 'localhost'

    # Port number of your LDAP server
    LDAP_PORT = int(os.environ.get('LDAP_PORT') or 389)

    # Specify whether the LDAP server connection should use SSL
    LDAP_USE_SSL = os.environ.get('LDAP_USE_SSL') or False

    # Base DN of your directory
    LDAP_BASE_DN = os.environ.get('LDAP_BASE_DN') or ''

    # Users DN to be prepended to the Base DN
    LDAP_USER_DN = os.environ.get('LDAP_USER_DN') or 'ou=users'

    # The RDN attribute for your user schema on LDAP
    LDAP_USER_RDN_ATTR = os.environ.get('LDAP_USER_RDN_ATTR') or 'uid'

    # Specify object filter to apply when searching for users
    LDAP_USER_OBJECT_FILTER = os.environ.get(
        'LDAP_USER_OBJECT_FILTER') or '(objectclass=inetOrgPerson)'

    # The Attribute you want users to authenticate to LDAP with
    LDAP_USER_LOGIN_ATTR = os.environ.get('LDAP_USER_LOGIN_ATTR') or 'uid'

    # Groups DN to be prepended to the Base DN
    LDAP_GROUP_DN = os.environ.get('LDAP_GROUP_DN') or 'ou=groups'

    # Specify object filter used when searching for groups
    LDAP_GROUP_OBJECT_FILTER = os.environ.get(
        'LDAP_GROUP_OBJECT_FILTER') or '(objectclass=groupofuniquenames)'

    # The attribute for group members on LDAP
    LDAP_GROUP_MEMBERS_ATTR = os.environ.get('LDAP_GROUP_MEMBERS_ATTR') or 'uniqueMember'

    # The Username to bind to LDAP with
    LDAP_BIND_USER_DN = os.environ.get('LDAP_BIND_USER_DN') or None

    # The Password to bind to LDAP with
    LDAP_BIND_USER_PASSWORD = os.environ.get('LDAP_BIND_USER_PASSWORD') or None

    # The LDAP user attribute to use as username for users in DB
    SYNC_USER_USERNAME_ATTR = os.environ.get('SYNC_USER_USERNAME_ATTR') or 'uid'

    # The LDAP user attribute to use as name for users in DB
    SYNC_USER_NAME_ATTR = os.environ.get('SYNC_USER_NAME_ATTR') or 'cn'

    # The LDAP group attribute to use as name for groups in DB
    SYNC_GROUP_NAME_ATTR = os.environ.get('SYNC_GROUP_NAME_ATTR') or 'cn'

    # -------JWT-CONFIG-------
    # How long a access token should live before it expires
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(minutes=15)

    # How long a refresh token should live before it expires
    JWT_REFRESH_TOKEN_EXPIRES = datetime.timedelta(days=1)

    # Enable token revoking
    JWT_BLACKLIST_ENABLED = True

    # Specify token types to check against the blacklist
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    # Include user claims in refresh tokens
    JWT_CLAIMS_IN_REFRESH_TOKEN = True

    # -------MAIL-CONFIG-------
    # The hostname or IP address of the SMTP server
    MAIL_SERVER = os.environ.get('MAIL_SERVER') or 'localhost'

    # The port of the SMTP server
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)

    # Enable STARTTLS
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') or False

    # Enable SSL/TLS
    MAIL_USE_SSL = os.environ.get('MAIL_USE_SSL') or False

    # The username of the email account
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or None

    # The password of the email account
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or None

    # Specify the prefix of subjects
    MAIL_SUBJECT_PREFIX = os.environ.get('MAIL_SUBJECT_PREFIX') or '[ARGOS]'

    # Specify the FROM email address to use
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER') or 'Argos <argos@localhost>'

    # Specify the link to the task detail page (task_id is appended) used in email templates
    TASK_DETAIL_PAGE = os.environ.get('TASK_DETAIL_PAGE') or 'http://example.com/tasks/'

    # -------CELERY-CONFIG-------
    # Specify the timezone for scheduled tasks
    CELERY_TIMEZONE = os.environ.get('CELERY_TIMEZONE') or 'Europe/Berlin'

    # The URL of the Celery message broker
    BROKER_URL = os.environ.get('BROKER_URL') or 'redis://localhost:6379/0'

    # The URL of the Celery result backend
    CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND') or 'redis://localhost:6379/0'

    # -------FLASK-BABEL-CONFIG-------
    # Specify supported languages in Jinja2 email templates/event messages
    SUPPORTED_LOCALES = ['en', 'de']

    # The fallback language (used if language of user is not in SUPPORTED_LOCALES)
    BABEL_DEFAULT_LOCALE = os.environ.get('BABEL_DEFAULT_LOCALE') or 'de'


class DevelopmentConfig(Config):
    """Development settings.

    Args:
        Config (Config): Base Configuration.

    """

    DEBUG = True

    # Use SQLite database if DEV_DATABASE_URL is not provided
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DEV_DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')


class TestingConfig(Config):
    """Testing settings.

    Args:
        Config (Config): Base Configuration.

    """

    TESTING = True

    # SQLite database is stored in memory if TEST_DATABASE_URL is not provided
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or 'sqlite://'

    # Celery tasks will be executed synchronous instead of being sent to the queue
    CELERY_ALWAYS_EAGER = True


class ProductionConfig(Config):
    """Production settings.

    Args:
        Config (Config): Base Configuration.

    """

    DEBUG = False
    TESTING = False

    # Use SQLite database if DATABASE_URL is not provided
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data.sqlite')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
