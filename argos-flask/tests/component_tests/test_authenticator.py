"""Tests the authenticator."""

import unittest

from ldap_test import LdapServer

from app import create_app, db, synchronizer
from app.api.v1.authenticator.helpers import (admin_required, user_required,
                                              check_if_token_revoked, revoke_token)
from app.api.v1.errors import Forbidden, InternalServerError
from app.models.models import Token, Admin
from tests.helper import (login_user, login_admin, refresh,
                          logout_access, logout_refresh, auth_header)


class AuthenticatorTestCase(unittest.TestCase):
    """This class represents the authenticator test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = LdapServer({
            'bind_dn': 'cn=admin,dc=argos,dc=com',
            'password': 'password',
            'base': {
                'objectclass': ['organization'],
                'dn': 'dc=argos,dc=com',
            },
            'entries': [{
                'objectclass': ['organizationalUnit'],
                'dn': 'ou=users,dc=argos,dc=com'
            }, {
                'objectclass': ['organizationalUnit'],
                'dn': 'ou=groups,dc=argos,dc=com'
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=maxmustermann,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'maxmustermann',
                               'mail': 'maxmustermann@argos.com',
                               'userPassword': 'test',
                               'cn': 'Max Mustermann',
                               'sn': 'Mustermann'}
            }, {
                'objectclass': ['groupOfUniqueNames'],
                'dn': 'cn=Vorstand,ou=groups,dc=argos,dc=com',
                'attributes': {'description': 'Beschreibung Vorstand',
                               'uniqueMember': ['uid=maxmustermann,ou=users,dc=argos,dc=com']}
            }],
        })
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = {
            'LDAP_HOST': 'localhost',
            'LDAP_PORT': int(self.server.config['port']),
            'LDAP_BASE_DN': self.server.config['base']['dn'],
            'LDAP_USE_SSL': False,
            'LDAP_USER_DN': 'ou=users',
            'LDAP_USER_RDN_ATTR': 'uid',
            'LDAP_USER_OBJECT_FILTER': '(objectclass=inetOrgPerson)',
            'LDAP_USER_LOGIN_ATTR': 'uid',
            'LDAP_GROUP_DN': 'ou = groups',
            'LDAP_GROUP_OBJECT_FILTER': '(objectclass=groupofuniquenames)',
            'LDAP_GROUP_MEMBERS_ATTR': 'uniqueMember',
            'LDAP_BIND_USER_DN': self.server.config['bind_dn'],
            'LDAP_BIND_USER_PASSWORD': self.server.config['password'],
            'SYNC_USER_USERNAME_ATTR': 'uid',
            'SYNC_USER_NAME_ATTR': 'cn',
            'SYNC_GROUP_NAME_ATTR': 'cn'
        }
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_login_missing_json(self):
        """Check if login request without JSON data is rejected with a bad request response."""
        response = self.client.post('/api/v1/login')
        self.assertEqual(response.status_code, 400)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Missing JSON data')

    def test_login_user_without_credentials(self):
        """Check if login request without credentials is rejected with an unauthorized response."""
        synchronizer.synchronize_ldap_to_db()
        response = self.client.post('/api/v1/login', json={})
        self.assertEqual(response.status_code, 401)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Username or password field empty')
        # assert no token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 0)

    def test_login_user_wrong_password(self):
        """Check if login request with wrong password is rejected with an unauthorized response."""
        synchronizer.synchronize_ldap_to_db()
        response = login_user(self.client, 'maxmustermann', '12345')
        self.assertEqual(response.status_code, 401)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Invalid username or password')
        # assert no token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 0)

    def test_login_user_correct_credentials(self):
        """Check if login request with correct credentials works."""
        synchronizer.synchronize_ldap_to_db()
        response = login_user(self.client, 'maxmustermann', 'test')
        self.assertEqual(response.status_code, 201)
        json_data = response.get_json()
        self.assertIsNotNone(json_data['access_token'])
        self.assertIsNotNone(json_data['refresh_token'])
        # assert tokens are created in db
        query = Token.query.all()
        self.assertEqual(len(query), 2)

    def test_login_user_not_in_db(self):
        """Internal Server Error is generated if user is not in db."""
        response = login_user(self.client, 'maxmustermann', 'test')
        self.assertEqual(response.status_code, 500)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'LDAP-Authentication successful, but user was not found '
                                           'in database. Run the Synchronizer to sync LDAP with '
                                           'database.')

    def test_login_admin_without_credentials(self):
        """Check if login request without credentials is rejected with an unauthorized response."""
        admin = Admin(name='Admin', email_address='admin@argos.com',
                      username='admin', password='12345')
        response = self.client.post('/api/v1/login?role=admin', json={})
        self.assertEqual(response.status_code, 401)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Username or password field empty')
        # assert no token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 0)
        admin.save()

    def test_login_admin_wrong_password(self):
        """Check if login request with wrong password is rejected with an unauthorized response."""
        admin = Admin(name='Admin', email_address='admin@argos.com',
                      username='admin', password='12345')
        admin.save()
        response = login_admin(self.client, 'admin', 'admin')
        self.assertEqual(response.status_code, 401)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Invalid username or password')
        # assert no token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 0)

    def test_login_admin_correct_credentials(self):
        """Check if login request with correct credentials works."""
        admin = Admin(name='Admin', email_address='admin@argos.com',
                      username='admin', password='12345')
        admin.save()
        response = login_admin(self.client, 'admin', '12345')
        self.assertEqual(response.status_code, 201)
        json_data = response.get_json()
        self.assertIsNotNone(json_data['access_token'])
        self.assertIsNotNone(json_data['refresh_token'])
        # assert tokens are created in db
        query = Token.query.all()
        self.assertEqual(len(query), 2)

    def test_login_invalid_role(self):
        """Check if request with invalid query string is denied."""
        response = self.client.post(
            '/api/v1/login?role=test',
            json={'username': 'username', 'password': 'password'})
        self.assertEqual(response.status_code, 400)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Invalid query string')

    def test_refresh_invalid_token(self):
        """Check if requesting a new access token with an invalid refresh token is denied."""
        refresh_token = '12345'
        response = refresh(self.client, refresh_token)
        self.assertEqual(response.status_code, 422)
        # assert no access token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 0)

    def test_refresh_wrong_token(self):
        """Check if requesting a new access token with an access token is denied."""
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        access_token = auth.get_json()['access_token']
        response = refresh(self.client, access_token)
        self.assertEqual(response.status_code, 422)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Only refresh tokens are allowed')
        # assert no access token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 2)

    def test_refresh_user_valid_token(self):
        """Check if requesting a new access token with a valid refresh token works."""
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        refresh_token = auth.get_json()['refresh_token']
        response = refresh(self.client, refresh_token)
        self.assertEqual(response.status_code, 201)
        json_data = response.get_json()
        self.assertIsNotNone(json_data['access_token'])
        # assert new access token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 3)

    def test_refresh_admin_valid_token(self):
        """Check if requesting a new access token with a valid refresh token works."""
        admin = Admin(name='Admin', email_address='admin@argos.com',
                      username='admin', password='12345')
        admin.save()
        auth = login_admin(self.client, 'admin', '12345')
        refresh_token = auth.get_json()['refresh_token']
        response = refresh(self.client, refresh_token)
        self.assertEqual(response.status_code, 201)
        json_data = response.get_json()
        self.assertIsNotNone(json_data['access_token'])
        # assert new access token is created in db
        query = Token.query.all()
        self.assertEqual(len(query), 3)

    def test_logout_access_invalid_token(self):
        """Check if invalidating an invalid access token is denied."""
        access_token = '12345'
        response = logout_access(self.client, access_token)
        self.assertEqual(response.status_code, 422)

    def test_logout_access_wrong_token(self):
        """Check if invalidating a refresh token is denied."""
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        refresh_token = auth.get_json()['refresh_token']
        response = logout_access(self.client, refresh_token)
        self.assertEqual(response.status_code, 422)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Only access tokens are allowed')

    def test_logout_access_valid_token(self):
        """Check if invalidating an access token works."""
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        access_token = auth.get_json()['access_token']
        response = logout_access(self.client, access_token)
        self.assertEqual(response.status_code, 200)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Successfully logged out')

    def test_logout_refresh_invalid_token(self):
        """Check if invalidating an invalid refresh token is denied."""
        refresh_token = '12345'
        response = logout_refresh(self.client, refresh_token)
        self.assertEqual(response.status_code, 422)

    def test_logout_refresh_wrong_token(self):
        """Check if invalidating an access token is denied."""
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        access_token = auth.get_json()['access_token']
        response = logout_refresh(self.client, access_token)
        self.assertEqual(response.status_code, 422)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Only refresh tokens are allowed')

    def test_logout_refresh_valid_token(self):
        """Check if invalidating a refresh token works."""
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        refresh_token = auth.get_json()['refresh_token']
        response = logout_refresh(self.client, refresh_token)
        self.assertEqual(response.status_code, 200)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Successfully logged out')

    def test_revoked_tokens_are_revoked(self):
        """Check if revoked tokens are truly revoked."""
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        access_token = auth.get_json()['access_token']
        refresh_token = auth.get_json()['refresh_token']
        logout_access(self.client, access_token)
        logout_refresh(self.client, refresh_token)
        response_access = logout_access(self.client, access_token)
        self.assertEqual(response_access.status_code, 401)
        json_data_access = response_access.get_json()
        self.assertEqual(json_data_access['msg'], 'Token has been revoked')
        response_refresh = logout_refresh(self.client, refresh_token)
        self.assertEqual(response_refresh.status_code, 401)
        json_data_refresh = response_refresh.get_json()
        self.assertEqual(json_data_refresh['msg'], 'Token has been revoked')

    def test_user_required_decorator(self):
        """Check if user_required decorator works."""
        # user only route
        @self.app.route("/user")
        @user_required
        def user_route():
            return "Hello User!"

        # request without auth header
        response = self.client.get('/user')
        self.assertEqual(response.status_code, 401)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Missing Authorization Header')

        # request from admin
        admin = Admin(name='Admin', email_address='admin@argos.com',
                      username='admin', password='12345')
        admin.save()
        response = login_admin(self.client, 'admin', '12345')
        admin_access_token = response.get_json()['access_token']
        self.assertRaises(Forbidden, self.client.get, '/user',
                          headers=auth_header(admin_access_token))

        # request from user
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        access_token = auth.get_json()['access_token']
        response = self.client.get('/user', headers=auth_header(access_token))
        self.assertEqual(response.status_code, 200)
        self.assertEqual('Hello User!', response.get_data(as_text=True))

    def test_admin_required_decorator(self):
        """Check if admin_required decorator works."""
        # admin only route
        @self.app.route("/admin")
        @admin_required
        def admin_route():
            return "Hello Admin!"

        # request without auth header
        response = self.client.get('/admin')
        self.assertEqual(response.status_code, 401)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Missing Authorization Header')

        # request from user
        synchronizer.synchronize_ldap_to_db()
        auth = login_user(self.client, 'maxmustermann', 'test')
        user_access_token = auth.get_json()['access_token']
        self.assertRaises(Forbidden, self.client.get, '/admin',
                          headers=auth_header(user_access_token))

        # request from admin
        admin = Admin(name='Admin', email_address='admin@argos.com',
                      username='admin', password='12345')
        admin.save()
        response = login_admin(self.client, 'admin', '12345')
        access_token = response.get_json()['access_token']
        response = self.client.get('/admin', headers=auth_header(access_token))
        self.assertEqual(response.status_code, 200)
        self.assertEqual('Hello Admin!', response.get_data(as_text=True))

    def test_revoke_invalid_token(self):
        """Check if internal server error is thrown if an invalid token is revoked."""
        with self.assertRaises(InternalServerError):
            revoke_token('123')

    def test_invalid_token_revoked_status(self):
        """Check if revoked_status of an invalid token is true."""
        decoded_token = {'jti': 'abc'}
        self.assertTrue(check_if_token_revoked(decoded_token))
