"""Tests the task handler."""
import io
import unittest

from flask import json

from app import create_app, db
from app.models.models import Endpoint, User, Admin, Task, Access, Group, Given_Access, Field
from tests.helper import create_access_token_admin, create_access_token_user, auth_header


class TaskHandlerTestCase(unittest.TestCase):
    """This class represents the task_handler test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_task_by_id_sequential(self):
        """Check if getting sequential task by ID works."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()

        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user_2 = User(name="user2", email_address='ab@c.com', email_notification_status=False,
                      username="username2", language="de_DE")
        user_2.save()
        user_3 = User(name="user3", email_address='ab@c.com', email_notification_status=False,
                      username="username3", language="de_DE")
        user_3.save()
        user_4 = User(name="user4", email_address='ab@c.com', email_notification_status=False,
                      username="username4", language="de_DE")
        user_4.save()

        group = Group(name='group')
        group.users.append(user_2)
        group.users.append(user_3)

        access_1 = Access(task=task, user=user_0, access_order=0)
        access_1.save()
        access_2 = Access(task=task, user=user_1, access_order=1)
        group.save()
        access_2.save()
        access_3 = Access(task=task, group=group, access_order=2)
        access_3.save()

        # mock requesting admin
        admin = Admin(name='admin', username='admin')
        admin.save()
        admin_token = create_access_token_admin(admin)

        self.assertTrue(access_valid(self.client, admin_token, task))  # admin can get task

        creator_token = create_access_token_user(creator)
        user0_token = create_access_token_user(user_0)
        user1_token = create_access_token_user(user_1)
        user2_token = create_access_token_user(user_2)
        user3_token = create_access_token_user(user_3)
        user4_token = create_access_token_user(user_4)

        self.assertTrue(access_valid(self.client, creator_token, task))  # creator can get task
        self.assertTrue(access_valid(self.client, user0_token, task))  # next user can get task
        self.assertFalse(access_valid(self.client, user1_token, task))  # others cannot get task
        self.assertFalse(access_valid(self.client, user4_token, task))  # others cannot get task

        access_1.access_status = 'signed'  # first user signs
        access_1.save()

        self.assertTrue(access_valid(self.client, user0_token, task))  # user can still access task
        self.assertTrue(access_valid(self.client, user1_token, task))  # next user can get task
        self.assertFalse(access_valid(self.client, user2_token, task))  # others cannot get task

        access_2.access_status = 'signed'  # user1 signs
        access_2.save()

        self.assertTrue(access_valid(self.client, user0_token, task))  # user can still get task
        self.assertTrue(access_valid(self.client, user1_token, task))  # user can still get task
        self.assertTrue(access_valid(self.client, user2_token, task))  # next user get task
        self.assertTrue(access_valid(self.client, user3_token, task))  # next user get task

        access_3.access_status = 'signed'  # user2 signs
        access_3.user = user_2

        self.assertTrue(access_valid(self.client, user2_token, task))  # user can still get task
        self.assertTrue(access_valid(self.client, user3_token, task))  # user3 can still get task
        self.assertFalse(access_valid(self.client, user4_token, task))  # others cannot get task

    def test_get_task_by_id_parallel(self):
        """Check if getting parallel task by ID works correctly."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox',
                            sequential_status=False, instantiable_status=True,
                            deletable_status=True, email_notification_status=True,
                            allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()

        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user_2 = User(name="user2", email_address='ab@c.com', email_notification_status=False,
                      username="username2", language="de_DE")
        user_2.save()
        user_3 = User(name="user3", email_address='ab@c.com', email_notification_status=False,
                      username="username3", language="de_DE")
        user_3.save()

        group = Group(name='group')
        group.users.append(user_1)
        group.users.append(user_2)
        group.save()

        access_1 = Access(task=task, user=user_0)
        access_1.save()
        access_2 = Access(task=task, group=group)
        access_2.save()

        # mock requesting admin
        admin = Admin(name='admin', username='admin')
        admin.save()
        admin_token = create_access_token_admin(admin)

        self.assertTrue(access_valid(self.client, admin_token, task))  # admin can get task

        creator_token = create_access_token_user(creator)
        user0_token = create_access_token_user(user_0)
        user1_token = create_access_token_user(user_1)
        user2_token = create_access_token_user(user_2)
        user3_token = create_access_token_user(user_3)

        self.assertTrue(access_valid(self.client, creator_token, task))  # creator can get task
        self.assertTrue(access_valid(self.client, user0_token, task))  # involved users can get task
        self.assertTrue(access_valid(self.client, user1_token, task))
        self.assertTrue(access_valid(self.client, user2_token, task))
        self.assertFalse(access_valid(self.client, user3_token, task))

        access_2.access_status = 'signed'  # user_1 signs
        access_2.user = user_1
        access_2.save()

        self.assertTrue(access_valid(self.client, user0_token, task))  # user can still access task
        self.assertTrue(access_valid(self.client, user1_token, task))  # user can still get task
        self.assertTrue(access_valid(self.client, user2_token, task))  # user can still get task

        access_1.access_status = 'signed'  # user_0 signs
        access_1.save()

        self.assertTrue(access_valid(self.client, user0_token, task))  # user can still get task
        self.assertTrue(access_valid(self.client, user1_token, task))  # user can still get task
        self.assertTrue(access_valid(self.client, user2_token, task))
        self.assertFalse(access_valid(self.client, user3_token, task))

    def test_get_tasks_tosign(self):
        """Check if getting tasks to sign works correctly."""
        # create sample data
        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user_2 = User(name="user2", email_address='ab@c.com', email_notification_status=False,
                      username="username2", language="de_DE")
        user_2.save()
        user1_token = create_access_token_user(user_1)

        endpoint_1 = Endpoint(name='endpoint_name_1', signature_type='checkbox',
                              sequential_status=False, instantiable_status=True,
                              deletable_status=True, email_notification_status=True,
                              allow_additional_accesses_status=True)
        endpoint_1.save()

        endpoint_2 = Endpoint(name='endpoint_name_2', signature_type='checkbox',
                              sequential_status=True, instantiable_status=True,
                              deletable_status=True, email_notification_status=True,
                              allow_additional_accesses_status=True)
        endpoint_2.save()
        task = Task(name='test1', creator=user_0, task_status='pending', endpoint=endpoint_1)
        task.save()
        task_1 = Task(name='test2', creator=user_0, task_status='pending', endpoint=endpoint_1)
        task_1.save()
        task_2 = Task(name='test3', creator=user_1, task_status='pending', endpoint=endpoint_2)
        task_2.save()
        task_3 = Task(name='test4', creator=user_1, task_status='pending', endpoint=endpoint_2)
        task_3.save()

        group_1 = Group(name='group_1')
        group_1.users.append(user_1)
        group_1.users.append(user_2)
        group_1.save()

        group_2 = Group(name='group_2')
        group_2.users.append(user_2)
        group_2.save()

        # accesses of parallel task
        access_1 = Access(task=task, user=user_1)
        access_1.save()
        access_2 = Access(task=task_1, user=user_2)
        access_2.save()
        access_3 = Access(task=task, group=group_2)
        access_3.save()

        # accesses of sequential task
        access_4 = Access(task=task_2, user=user_1, access_order=0)
        access_4.save()
        access_5 = Access(task=task_2, user=user_2, access_order=1)
        access_5.save()
        access_6 = Access(task=task_2, group=group_1, access_order=2)
        access_6.save()
        access_7 = Access(task=task_3, group=group_1, access_order=0)
        access_7.save()

        self.assertTrue(get_tasks_by_type_equals(self.client, 'TOSIGN', user1_token,
                                                 [task, task_2]))

        access_4.access_status = 'signed'
        access_4.user = user_1
        access_4.save()
        self.assertTrue(get_tasks_by_type_equals(self.client, 'TOSIGN', user1_token,
                                                 [task]))
        access_5.access_status = 'signed'
        access_5.user = user_2
        access_5.save()
        self.assertTrue(get_tasks_by_type_equals(self.client, 'TOSIGN', user1_token,
                                                 [task]))
        access_7.access_status = 'signed'
        access_7.user = user_2
        access_7.save()
        self.assertTrue(get_tasks_by_type_equals(self.client, 'TOSIGN', user1_token,
                                                 [task]))
        access_1.access_status = 'signed'
        access_1.user = user_1
        access_1.save()
        self.assertTrue(get_tasks_by_type_equals(self.client, 'TOSIGN', user1_token,
                                                 []))

    def test_get_tasks_by_type(self):
        """Check if getting tasks by type works correctly."""
        # create sample data
        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user_2 = User(name="user2", email_address='ab@c.com', email_notification_status=False,
                      username="username2", language="de_DE")
        user_2.save()
        user_3 = User(name="user3", email_address='ab@c.com', email_notification_status=False,
                      username="username3", language="de_DE")
        user_3.save()
        user_4 = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                      username='test_username', language='de_DE')
        user_4.save()
        user0_token = create_access_token_user(user_0)
        user1_token = create_access_token_user(user_1)

        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox',
                            sequential_status=False, instantiable_status=True,
                            deletable_status=True, email_notification_status=True,
                            allow_additional_accesses_status=True)
        endpoint.save()

        task = Task(name='test1', creator=user_0, task_status='pending', endpoint=endpoint)
        task.save()
        task_1 = Task(name='test2', creator=user_0, task_status='pending', endpoint=endpoint)
        task_1.save()
        task_2 = Task(name='test3', creator=user_1, task_status='completed', endpoint=endpoint)
        task_2.save()
        task_3 = Task(name='test4', creator=user_1, task_status='withdrawn', endpoint=endpoint)
        task_3.save()

        group = Group(name='group')
        group.users.append(user_1)
        group.users.append(user_2)
        group.save()

        access_1 = Access(task=task, user=user_1)
        access_1.save()
        access_2 = Access(task=task, user=user_2)
        access_2.save()
        access_3 = Access(task=task_1, group=group)
        access_3.save()

        access_1.access_status = 'signed'
        task.save()

        self.assertTrue(get_tasks_by_type_equals(self.client, 'OWNED', user0_token, [task, task_1]))

        access_3.access_status = 'signed'
        access_3.user = user_1
        task_1.task_status = 'completed'
        task_1.save()

        self.assertTrue(get_tasks_by_type_equals(self.client, 'ARCHIVED', user1_token,
                                                 [task, task_1, task_2]))

    def test_create_task(self):
        """Check if endpoint is created correctly."""
        # mock requesting user
        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_token = create_access_token_user(user_0)
        user_1 = User(name="user1", email_address='mail@mail.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox',
                            sequential_status=False, instantiable_status=True,
                            deletable_status=True, email_notification_status=True,
                            allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name='field', field_type='textfield', endpoint=endpoint)
        field.save()
        field_1 = Field(name='field', field_type='textfield', endpoint=endpoint,
                        required_status=False)
        field_1.save()

        group = Group(name='group')
        group.users.append(user_0)
        group.users.append(user_1)
        group.save()
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'name': 'name',
            'accesses': [{
                'user_id': user_1.user_id,
            }, {
                'group_id': group.group_id
            }],
            'tasks_fields': [{'field_id': 1, 'value': 'text'}]
        }
        response = self.client.post('/api/v1/tasks/', json=task_data,
                                    headers=auth_header(user_token))
        url = response.headers['Location']
        data = self.client.get(url, headers=auth_header(user_token)).get_data(as_text=True)
        self.assertEqual(json.loads(data)['task_id'], 1)

        # task does not contain required field
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'name': 'name',
            'accesses': [{
                'user_id': user_1.user_id,
            }, {
                'group_id': group.group_id
            }],
            'tasks_fields': [{'field_id': 2, 'value': 'text'}]
        }
        response = self.client.post('/api/v1/tasks/', json=task_data,
                                    headers=auth_header(user_token))
        self.assertEqual(response.status_code, 400)

    def test_delete_task(self):
        """Check if task is deleted correctly."""
        # mock requesting user
        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_token = create_access_token_user(user_0)
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox',
                            sequential_status=False, instantiable_status=True,
                            deletable_status=False, email_notification_status=True,
                            allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=user_0, task_status='pending', endpoint=endpoint)
        task.save()

        response = self.client.delete('/api/v1/tasks/%d' % task.task_id,
                                      headers=auth_header(user_token))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(task.task_status, 'withdrawn')

        task.task_status = 'completed'
        task.save()
        response = self.client.delete('/api/v1/tasks/%d' % task.task_id,
                                      headers=auth_header(user_token))
        self.assertEqual(response.status_code, 400)

        task.task_status = 'withdrawn'
        task.save()
        response = self.client.delete('/api/v1/tasks/%d' % task.task_id,
                                      headers=auth_header(user_token))
        self.assertEqual(response.status_code, 404)

    def test_sign_parallel_task(self):
        """Check if getting tasks to sign works correctly."""
        # create sample data
        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user_2 = User(name="user2", email_address='ab@c.com', email_notification_status=False,
                      username="username2", language="de_DE")
        user_2.save()
        user1_token = create_access_token_user(user_1)
        user2_token = create_access_token_user(user_2)

        endpoint_1 = Endpoint(name='endpoint_name_1', signature_type='checkbox',
                              sequential_status=False, instantiable_status=True,
                              deletable_status=True, email_notification_status=True,
                              allow_additional_accesses_status=True)
        endpoint_1.save()

        task = Task(name='test1', creator=user_0, task_status='pending', endpoint=endpoint_1)
        task.save()
        group_2 = Group(name='group_2')
        group_2.users.append(user_2)
        group_2.save()

        # accesses of parallel task
        access_1 = Access(task=task, user=user_1)
        access_1.save()
        access_2 = Access(task=task, group=group_2)
        access_2.save()
        self.assertTrue(update_task_status_code_valid(self.client, user1_token, task, 'signed'))
        self.assertFalse(update_task_status_code_valid(self.client, user1_token, task, 'signed'))
        self.assertTrue(update_task_status_code_valid(self.client, user2_token, task, 'signed'))
        self.assertEqual(Task.query.get(task.task_id).task_status, 'completed')

    def test_sequential_sign_task(self):
        """Check if getting tasks to sign works correctly."""
        # create sample data
        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user_2 = User(name="user2", email_address='ab@c.com', email_notification_status=False,
                      username="username2", language="de_DE")
        user_2.save()
        user1_token = create_access_token_user(user_1)
        user2_token = create_access_token_user(user_2)

        endpoint_1 = Endpoint(name='endpoint_name_2', signature_type='checkbox',
                              sequential_status=True, instantiable_status=True,
                              deletable_status=True, email_notification_status=True,
                              allow_additional_accesses_status=True)
        endpoint_1.save()
        task = Task(name='test1', creator=user_1, task_status='pending', endpoint=endpoint_1)
        task.save()
        group_1 = Group(name='group_1')
        group_1.users.append(user_1)
        group_1.users.append(user_2)
        group_1.save()
        group_2 = Group(name='group_2')
        group_2.users.append(user_2)
        group_2.save()

        # accesses of sequential task
        access_1 = Access(task=task, user=user_1, access_order=1)
        access_1.save()
        access_2 = Access(task=task, user=user_2, access_order=2)
        access_2.save()
        access_3 = Access(task=task, group=group_1, access_order=3)
        access_3.save()
        self.assertEqual(access_valid(self.client, user1_token, task), True)
        self.assertEqual(access_1.access_status, 'viewed')
        self.assertTrue(update_task_status_code_valid(self.client, user1_token, task, 'signed'))
        self.assertEqual(Access.query.get(access_1.access_id).access_status, 'signed')
        self.assertEqual(access_valid(self.client, user1_token, task), True)
        self.assertEqual(Access.query.get(access_1.access_id).access_status, 'signed')
        self.assertFalse(update_task_status_code_valid(self.client, user1_token, task, 'signed'))
        self.assertTrue(update_task_status_code_valid(self.client, user2_token, task, 'signed'))
        self.assertTrue(update_task_status_code_valid(self.client, user2_token, task, 'denied'))
        self.assertFalse(update_task_status_code_valid(self.client, user1_token, task, 'signed'))

    def test_sign_task_after_download(self):
        """Check if signing task after download works."""
        user_0 = User(name="user0", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_0.save()
        user_1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user0_token = create_access_token_user(user_0)
        user1_token = create_access_token_user(user_1)
        endpoint = Endpoint(name='endpoint', signature_type='checkboxAfterDownload',
                            sequential_status=True, instantiable_status=True,
                            deletable_status=True, email_notification_status=True,
                            allow_additional_accesses_status=True)
        access_1 = Given_Access(endpoint=endpoint, user=user_1, access_order=1)
        access_1.save()
        field = Field(name='file field', field_type='file', endpoint=endpoint)
        field.save()
        data = {
            'json': (io.BytesIO(b'{\n  "endpoint_id": 1,\n  "name": "name"\n}\n'), "test.json"),
            'file': (io.BytesIO(b'my file contents'), "test.txt")
        }
        response = self.client.post('/api/v1/tasks/', data=data, content_type='multipart/form-data',
                                    headers=auth_header(user0_token))
        task = Task.query.get(response.get_json()['task_id'])
        self.assertFalse(update_task_status_code_valid(self.client, user1_token, task, 'signed'))
        # download file
        self.client.get('/api/v1/tasks/1/files', headers=auth_header(user1_token))
        self.assertTrue(update_task_status_code_valid(self.client, user1_token, task, 'signed'))


def access_valid(client, token, task):
    """Check if access of task is valid.

    Args:
        client (TestClient): Testing client.
        token (str): Token of requestor.
        task (Task): Requested task.

    Returns:
        bool: True -> access valid, False -> access invalid

    """
    response = client.get('/api/v1/tasks/%d' % task.task_id,
                          headers=auth_header(token))
    return True if response.status_code == 200 else False


def get_tasks_by_type_equals(client, type, token, task_list):
    """Check if given task list is equal to response of getting tasks by type.

    Args:
        client (TestClient): Testing client.
        type (str): Type of tasks to get.
        token (str): Token of requestor.
        task_list (list of Tasks): List of tasks to compare response data to.

    Returns:
        bool: True -> task_list and tasks in response are equal
              False -> task_list and tasks in response are not equal

    """
    data = client.get('/api/v1/tasks/?type=%s' % type,
                      headers=auth_header(token))
    tasks_data = json.loads(data.get_data(as_text=True))['tasks']
    task_ids_response = set([task['task_id'] for task in tasks_data])
    return set([task.task_id for task in task_list]) == task_ids_response


def update_task_status_code_valid(client, token, task, event):
    """Check if updating task according to given event is valid.

    Args:
        client (TestClient): Testing client.
        token (str): Token of requestor.
        task (list of Task): Task to update.
        event (str): Given event. Either signed or denied.

    Returns:
        bool: True -> task_list and tasks in response are equal
              False -> task_list and tasks in response are not equal

    """
    response = client.put('/api/v1/tasks/%d/status' % task.task_id,
                          json={'event': event}, headers=auth_header(token))
    return True if response.status_code == 200 else False
