"""Tests the file handler."""

import io
import os
import unittest

from app import create_app, db
from app.models.models import User, Endpoint, Task, Given_Access, Field
from tests.helper import create_access_token_user, auth_header


class FileHandlerTestCase(unittest.TestCase):
    """This class represents the file_handler test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_save_file(self):
        """Check if file_handler works."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()

        field = Field(name='file field', field_type='file', endpoint=endpoint)
        field.save()
        user = User(name='John Doe', email_address='john@argos.com',
                    username='john', language='en')
        user.save()
        access_token_user = create_access_token_user(user)
        given_access = Given_Access(endpoint=endpoint, user=user)
        given_access.save()

        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        access_token_creator = create_access_token_user(creator)

        # create task
        data = {
            'json': (io.BytesIO(b'{\n  "endpoint_id": 1,\n  "name": "name"\n}\n'), "test.json"),
            'file': (io.BytesIO(b'my file contents'), "test.txt")
        }
        response = self.client.post('/api/v1/tasks/', data=data, content_type='multipart/form-data',
                                    headers=auth_header(access_token_creator))
        # check if file exists
        json_data = response.get_json()
        self.assertEqual(response.status_code, 201)
        filename = json_data['tasks_fields'][0]['value']
        self.assertTrue('test.txt' in filename)

        # file should be deleted after zip is created
        self.assertFalse(os.path.isfile('uploads/' + filename))

        # get download links
        response = self.client.get('/api/v1/tasks/1/files',
                                   headers=auth_header(access_token_creator))
        self.assertIsNotNone(response.get_json()['link'])
        response = self.client.get('/api/v1/tasks/1/files',
                                   headers=auth_header(access_token_user))
        self.assertIsNotNone(response.get_json()['link'])
        json_link = response.get_json()['link']
        path = json_link.split('?', 1)[0][1:]
        self.assertTrue(os.path.isfile(path))
        os.remove(path)
        response = self.client.get('/api/v1/tasks/1',
                                   headers=auth_header(access_token_creator))
        json_data = response.get_json()
        self.assertEqual(json_data['accesses'][0]['access_status'], 'downloaded')

        # no upload allowed if endpoint has no file field
        endpoint_no_file = Endpoint(name='endpoint_name', signature_type='button',
                                    sequential_status=False, instantiable_status=True,
                                    deletable_status=True, email_notification_status=True,
                                    allow_additional_accesses_status=True)
        endpoint_no_file.save()
        given_access = Given_Access(endpoint=endpoint_no_file, user=user)
        given_access.save()
        data = {
            'json': (io.BytesIO(b'{\n  "endpoint_id": 2,\n  "name": "name"\n}\n'), "test.json"),
            'file': (io.BytesIO(b'my file contents'), "test.txt")
        }
        response = self.client.post('/api/v1/tasks/', data=data, content_type='multipart/form-data',
                                    headers=auth_header(access_token_creator))
        self.assertEqual(response.status_code, 400)

    def test_upload_files(self):
        """Check if uploading multiple files works."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name='John Doe', email_address='john@argos.com',
                    username='john', language='en')
        user.save()
        given_access = Given_Access(endpoint=endpoint, user=user)
        given_access.save()
        field = Field(name='file field', field_type='file', endpoint=endpoint, required_status=True)
        field.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        access_token_creator = create_access_token_user(creator)

        # create task
        data = {
            'json': (io.BytesIO(b'{\n  "endpoint_id": 1,\n  "name": "name"\n}\n'), "test.json"),
            'file': [(io.BytesIO(b'my file contents'), "test.txt"),
                     (io.BytesIO(b'my file contents'), "test2.txt")]
        }
        response = self.client.post('/api/v1/tasks/', data=data, content_type='multipart/form-data',
                                    headers=auth_header(access_token_creator))
        # check if file exists
        json_data = response.get_json()
        self.assertEqual(response.status_code, 201)
        filename = json_data['tasks_fields'][0]['value']
        self.assertTrue('test.txt' in filename)
        self.assertEqual(json_data['tasks_fields'][0]['field']['name'], 'file field')
        filename = json_data['tasks_fields'][1]['value']
        self.assertTrue('test2.txt' in filename)
        self.assertEqual(json_data['tasks_fields'][1]['field']['name'], 'file field')

    def test_only_task_accesses_get_link(self):
        """Check if only users assigned to task get a link."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name='file field', field_type='file', endpoint=endpoint)
        field.save()

        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()

        user1 = User(name='Julee Clears', email_address='jclears0@argos.com',
                     username='jclears0', language='en')
        user1.save()

        # try to get download link
        access_token_user1 = create_access_token_user(user1)
        response = self.client.get('/api/v1/tasks/1/files',
                                   headers=auth_header(access_token_user1))
        self.assertTrue(response.status_code, 400)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'Access denied')

    def test_invalid_file(self):
        """Check if invalid requests are rejected."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name='file field', field_type='file', endpoint=endpoint)
        field.save()
        user = User(name='John Doe', email_address='john@argos.com',
                    username='john', language='en')
        user.save()
        given_access = Given_Access(endpoint=endpoint, user=user)
        given_access.save()

        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        access_token_creator = create_access_token_user(creator)

        data = {
            'json': (io.BytesIO(b'{\n  "endpoint_id": 1,\n  "name": "name"\n}\n'), "test.json"),
            'file': (io.BytesIO(b'my file contents'), "test.exe")
        }
        # try to create task with exe file
        response = self.client.post('/api/v1/tasks/', data=data, content_type='multipart/form-data',
                                    headers=auth_header(access_token_creator))
        self.assertTrue(response.status_code, 400)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'File extension not allowed: test.exe')
        self.assertEqual(Task.query.count(), 0)

        data = {
            'json': (io.BytesIO(b'{\n  "endpoint_id": 1,\n  "name": "name"\n}\n'), "test.json"),
            'file': (io.BytesIO(b'my file contents'), "test")
        }
        # try to create task with invalid file
        response = self.client.post('/api/v1/tasks/', data=data, content_type='multipart/form-data',
                                    headers=auth_header(access_token_creator))
        self.assertTrue(response.status_code, 400)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'], 'File extension not allowed: test')
        self.assertEqual(Task.query.count(), 0)

        data = {
            'json': (io.BytesIO(b'{\n  "endpoint_id": 1,\n  "name": "name"\n}\n'), "test.json")
        }
        # try to create task with no files although at least one file is required
        response = self.client.post('/api/v1/tasks/', data=data, content_type='multipart/form-data',
                                    headers=auth_header(access_token_creator))
        self.assertTrue(response.status_code, 400)
        json_data = response.get_json()
        self.assertEqual(json_data['msg'],
                         'Missing required file.')
        self.assertEqual(Task.query.count(), 0)
