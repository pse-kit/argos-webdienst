"""Tests the endpoint handler."""
import unittest

from flask import json

from app import create_app, db
from app.models.models import Endpoint, User, Admin
from app.models.schemas import EndpointSchema
from tests.helper import create_access_token_admin, create_access_token_user, auth_header


class EndpointHandlerTestCase(unittest.TestCase):
    """This class represents the endpoint_handler test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_endpoint_by_id(self):
        """Check if getting endpoint by ID works."""
        # mock requesting user
        user = User(name='name', email_address='a@b.com', email_notification_status=False,
                    username="test_username", language="de_DE")
        user.save()
        user_token = create_access_token_user(user)

        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        endpoint_data = EndpointSchema().dump(endpoint)
        endpoint_data.pop('active_tasks')
        response = self.client.get('/api/v1/endpoints/%d' % endpoint.endpoint_id,
                                   headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response, endpoint_data)
        # 404 Error if no endpoint with given endpoint_id exists.
        response = self.client.get('/api/v1/endpoints/%d' % 3, headers=auth_header(user_token))
        self.assertEqual(response.status_code, 404)

    def test_create_endpoint(self):
        """Check if endpoint is created correctly."""
        # mock requesting admin
        admin = Admin(name='admin', username='admin')
        admin.save()
        admin_token = create_access_token_admin(admin)

        endpoint_data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': True
        }
        response = self.client.post('/api/v1/endpoints/', json=endpoint_data,
                                    headers=auth_header(admin_token))
        url = response.headers['Location']
        data = self.client.get(url, headers=auth_header(admin_token)).get_data(as_text=True)
        self.assertEqual(json.loads(data)['endpoint_id'], 1)

    def test_set_endpoint_deprecated(self):
        """Check if instantiable_status of endpoint is set correctly."""
        # mock requesting admin
        admin = Admin(name='admin', username='admin')
        admin.save()
        admin_token = create_access_token_admin(admin)

        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        response = self.client.delete('/api/v1/endpoints/%d' % endpoint.endpoint_id,
                                      headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(endpoint.instantiable_status)
        # 404 Error if no endpoint with given endpoint_id exists.
        response = self.client.delete('/api/v1/endpoints/%d' % 3,
                                      headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 404)

    def test_get_endpoints_active(self):
        """Check if getting all active endpoints works."""
        user = User(name='name', email_address='a@b.com', email_notification_status=False,
                    username="test_username", language="de_DE")
        user.save()
        user_token = create_access_token_user(user)

        # create sample data
        endpoint1 = Endpoint(name='b', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)
        endpoint1.save()
        endpoint2 = Endpoint(name='a', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)
        endpoint2.save()
        endpoint3 = Endpoint(name='d', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)
        endpoint3.save()
        endpoint4 = Endpoint(name='c', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)
        endpoint4.save()
        endpoint5 = Endpoint(name='e', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)
        endpoint5.save()
        endpoint6 = Endpoint(name='f', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)
        endpoint6.save()
        endpoint6.instantiable_status = False

        limit = 2

        # admin can see active endpoints
        admin = Admin(name='admin', email_address='mail@mail.de', username='admin')
        admin_token = create_access_token_admin(admin)
        response = self.client.get('/api/v1/endpoints/?page=1&limit=%d' % limit,
                                   headers=auth_header(admin_token))
        json_response = json.loads(response.get_data(as_text=True))
        endpoints = json_response['endpoints']
        self.assertEqual(len(endpoints), limit)
        self.assertEqual(endpoints[0]['name'], 'a')
        self.assertEqual(endpoints[1]['name'], 'b')

        # get first page of endpoints
        response = self.client.get('/api/v1/endpoints/?page=1&limit=%d' % limit,
                                   headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        endpoints = json_response['endpoints']
        self.assertEqual(len(endpoints), limit)
        self.assertEqual(endpoints[0]['name'], 'a')
        self.assertEqual(endpoints[1]['name'], 'b')
        # get second page of endpoints
        response = self.client.get(json_response['next'], headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        endpoints = json_response['endpoints']
        self.assertEqual(len(endpoints), limit)
        self.assertEqual(endpoints[0]['name'], 'c')
        self.assertEqual(endpoints[1]['name'], 'd')
        # get third page of endpoints
        response = self.client.get(json_response['next'], headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        endpoints = json_response['endpoints']
        self.assertEqual(len(endpoints), 1)
        self.assertEqual(endpoints[0]['name'], 'e')
        # get previous page of endpoints
        response = self.client.get(json_response['previous'], headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        endpoints = json_response['endpoints']
        self.assertEqual(len(endpoints), limit)
        self.assertEqual(endpoints[0]['name'], 'c')
        self.assertEqual(endpoints[1]['name'], 'd')

        # default values for page and limit if given values are invalid
        response = self.client.get('/api/v1/endpoints/?page=-3&limit=dsfkh',
                                   headers=auth_header(user_token))
        self.assertEqual(response.status_code, 200)
