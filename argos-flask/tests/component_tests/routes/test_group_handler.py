"""Tests the group handler."""
import unittest

from flask import json

from app import create_app, db
from app.models.models import Group, User
from app.models.schemas import GroupSchema
from tests.helper import create_access_token_user, auth_header


class GroupHandlerTestCase(unittest.TestCase):
    """This class represents the group_handler test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_group_search(self):
        """Check if searching for groups by name works."""
        # mock requesting user
        user = User(name='name', email_address='a@b.com', email_notification_status=False,
                    username="test_username", language="de_DE")
        user.save()
        user_token = create_access_token_user(user)

        search_term = 'test'
        group_1 = Group(name='1_' + search_term + '_group')
        group_1.save()
        group_2 = Group(name=search_term + '_group')
        group_2.save()
        group_3 = Group(name='group')
        group_3.save()
        group_data = GroupSchema().dump([group_1, group_2], many=True)
        response = self.client.get('/api/v1/groups/search/?search=' + search_term,
                                   headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))['groups']
        for group in group_data:
            self.assertTrue(group in json_response)

        # error: no search term given
        response = self.client.get('/api/v1/groups/search/',
                                   headers=auth_header(user_token))
        self.assertEqual(response.status_code, 400)
