"""Tests the preference handler."""
import os
import unittest

from flask import json

from app import create_app, db, mail
from app.models.models import User, Endpoint
from tests.helper import create_access_token_user, auth_header


class PreferenceHandlerTestCase(unittest.TestCase):
    """This class represents the preference_handler test case."""

    @classmethod
    def setUpClass(cls):
        """Compile the translations."""
        if os.system('pybabel compile -d app/translations'):
            raise RuntimeError('compile command failed')

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_language(self):
        """Check if getting the language of a user works."""
        # mock requesting user
        user_1 = User(name='name', email_address='a@b.com', email_notification_status=False,
                      username="test_username", language="de_DE")
        user_1.save()
        user1_token = create_access_token_user(user_1)

        user_2 = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_2.save()
        user2_token = create_access_token_user(user_2)

        response = self.client.get('/api/v1/preferences/language/',
                                   headers=auth_header(user1_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['language'], 'de_DE')

        response = self.client.get('/api/v1/preferences/language/',
                                   headers=auth_header(user2_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['language'], 'de_DE')

    def test_set_language(self):
        """Check if setting the language of a user works."""
        # mock requesting user
        language_de = 'de_DE'
        user_1 = User(name='name', email_address='a@b.com', email_notification_status=False,
                      username="test_username", language=language_de)
        user_1.save()
        user1_token = create_access_token_user(user_1)
        user_id = user_1.user_id
        language = 'en'
        response = self.client.put('/api/v1/preferences/language/',
                                   json={'language': language},
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 200)
        user = User.query.get(user_id)
        self.assertEqual(user.language, language)

    def test_translation(self):
        """Check if correct language is used in mails/messages."""
        # create sample data
        endpoint = Endpoint(name='Test', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        endpoint_id = endpoint.endpoint_id
        creator = User(name='John Doe', email_address='john@argos.com',
                       username='john')
        creator.save()
        access_token_creator = create_access_token_user(creator)
        user = User(name='Max Mustermann', email_address='max@argos.com',
                    username='max', language='en')
        user.save()
        user_id = user.user_id
        access_token_user = create_access_token_user(user)

        # change language of creator to 'xyz'
        self.client.put('/api/v1/preferences/language/',
                        json={'language': 'xyz'},
                        headers=auth_header(access_token_creator))

        # create task
        with mail.record_messages() as outbox:
            self.client.post('/api/v1/tasks/',
                             json={"endpoint_id": endpoint_id, "name": 'Name',
                                   "accesses": [{"user_id": user_id}]},
                             headers=auth_header(access_token_creator))
        # mails/messages of creator should still be in default language
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].recipients[0], 'john@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Auftragserstellung erfolgreich')
        self.assertEqual(outbox[1].recipients[0], 'max@argos.com')
        self.assertEqual(outbox[1].subject, '[ARGOS] Task waiting for review')
        response = self.client.get('/api/v1/messages/',
                                   headers=auth_header(access_token_creator))
        json_data = response.get_json()
        self.assertTrue('Ihr Auftrag "Name" wurde erstellt.' in json_data['messages'][0]['text'])

        # change language of creator to 'en'
        self.client.put('/api/v1/preferences/language/',
                        json={'language': 'en'},
                        headers=auth_header(access_token_creator))

        # comment task
        with mail.record_messages() as outbox:
            self.client.post('/api/v1/tasks/1/comments',
                             json={"text": "comments"},
                             headers=auth_header(access_token_user))
        # creator should be notified
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'john@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task commented')
        response = self.client.get('/api/v1/messages/',
                                   headers=auth_header(access_token_creator))
        json_data = response.get_json()
        self.assertTrue('Your task "Name" has been commented by Max Mustermann.'
                        in json_data['messages'][0]['text'])

    def test_get_email_notification_status(self):
        """Check if getting the email_notification_status of a user works."""
        # mock requesting user
        status_1 = False
        status_2 = True
        user_1 = User(name='name', email_address='a@b.com', email_notification_status=status_1,
                      username="test_username", language="de_DE")
        user_1.save()
        user1_token = create_access_token_user(user_1)

        user_2 = User(name="user", email_address='m@mail.com', email_notification_status=status_2,
                      username="username", language="de_DE")
        user_2.save()
        user2_token = create_access_token_user(user_2)

        response = self.client.get('/api/v1/preferences/notifications/',
                                   headers=auth_header(user1_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['notification_status'], status_1)

        response = self.client.get('/api/v1/preferences/notifications/',
                                   headers=auth_header(user2_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['notification_status'], status_2)

    def test_set_email_notification_status(self):
        """Check if setting the email_notification_status of a user works."""
        # mock requesting user
        status = True
        user_1 = User(name='name', email_address='a@b.com', email_notification_status=False,
                      username="test_username")
        user_1.save()
        user1_token = create_access_token_user(user_1)
        user_id = user_1.user_id
        response = self.client.put('/api/v1/preferences/notifications/',
                                   json={'notification_status': True},
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 200)
        user = User.query.get(user_id)
        self.assertEqual(user.email_notification_status, status)

        # invalid notification status
        response = self.client.put('/api/v1/preferences/notifications/',
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 400)
