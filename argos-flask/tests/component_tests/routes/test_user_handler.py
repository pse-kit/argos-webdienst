"""Tests the user handler."""
import unittest

from flask import json

from app import create_app, db
from app.models.models import User
from app.models.schemas import UserSchema
from tests.helper import create_access_token_user, auth_header


class UserHandlerTestCase(unittest.TestCase):
    """This class represents the user_handler test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_user_search(self):
        """Check if searching for users by name, username and user_id works."""
        # create sample data
        search_term = 'test'
        user_1 = User(name=search_term, email_address='a@b.com', email_notification_status=False,
                      username="username1", language="de_DE")
        user_1.save()
        user_token = create_access_token_user(user_1)

        user_2 = User(name='name' + search_term, email_address='a@b.com',
                      email_notification_status=False, username=search_term, language="de_DE")
        user_2.save()
        user_3 = User(name='name', email_address='a@b.com', email_notification_status=False,
                      username='username', language="de_DE")
        user_3.save()
        user_data = UserSchema().dump([user_1, user_2], many=True)

        # search for string
        response = self.client.get('/api/v1/users/search/?search=' + search_term,
                                   headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        for user in user_data:
            self.assertTrue(user in json_response['users'])

        # search for number
        response = self.client.get('/api/v1/users/search/?search=3',
                                   headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertTrue([UserSchema().dump(user_3)], json_response['users'])

        # error: no search term given
        response = self.client.get('/api/v1/users/search/',
                                   headers=auth_header(user_token))
        self.assertEqual(response.status_code, 400)

    def test_user_current(self):
        """Check if getting user info works."""
        user = User(name='John Doe', email_address='a@b.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        user_id = user.user_id
        user_token = create_access_token_user(user)
        response = self.client.get('/api/v1/users/current', headers=auth_header(user_token))
        json_data = response.get_json()
        self.assertEqual(json_data['email_address'], 'a@b.com')
        self.assertEqual(json_data['name'], 'John Doe')
        self.assertEqual(json_data['user_id'], user_id)
