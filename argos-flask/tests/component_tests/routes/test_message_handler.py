"""Tests the message handler."""
import unittest

from flask import json

from app import create_app, db
from app.models.models import Message, User
from tests.helper import create_access_token_user, auth_header


class MessageHandlerTestCase(unittest.TestCase):
    """This class represents the message_handler test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_messages(self):
        """Check if getting all messages of a user works."""
        # mock requesting user
        user_1 = User(name='name', email_address='a@b.com', email_notification_status=False,
                      username="test_username", language="de_DE")
        user_1.save()
        user1_token = create_access_token_user(user_1)

        user_2 = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_2.save()
        user2_token = create_access_token_user(user_2)

        message1 = Message(text='a', recipient=user_1)
        message1.save()
        message2 = Message(text='b', recipient=user_1)
        message2.save()
        message3 = Message(text='c', recipient=user_1)
        message3.save()
        message4 = Message(text='d', recipient=user_2)
        message4.save()
        response = self.client.get('/api/v1/messages/', headers=auth_header(user1_token))
        json_response = json.loads(response.get_data(as_text=True))
        messages = json_response['messages']
        self.assertEqual(len(messages), 3)

        response = self.client.get('/api/v1/messages/', headers=auth_header(user2_token))
        json_response = json.loads(response.get_data(as_text=True))
        messages = json_response['messages']
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]['text'], 'd')

    def test_get_message_by_id(self):
        """Check if getting message by ID works."""
        # mock requesting user
        user_1 = User(name='name', email_address='a@b.com', email_notification_status=False,
                      username="test_username", language="de_DE")
        user_1.save()
        user_token = create_access_token_user(user_1)

        # create sample data
        user_2 = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                      username="username", language="de_DE")
        user_2.save()
        message1 = Message(text='a', recipient=user_1)
        message1.save()
        message2 = Message(text='b', recipient=user_2)
        message2.save()

        response = self.client.get('/api/v1/messages/%d' % message1.message_id,
                                   headers=auth_header(user_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['text'], message1.text)

        # 403 Error if user tries to access message which does not belong to him
        response = self.client.get('/api/v1/messages/%d' % message2.message_id,
                                   headers=auth_header(user_token))
        self.assertEqual(response.status_code, 403)

    def test_update_read_status(self):
        """Check if updating message read_status works."""
        # mock requesting user
        user_1 = User(name='name', email_address='a@b.com', email_notification_status=False,
                      username="test_username", language="de_DE")
        user_1.save()
        user_token = create_access_token_user(user_1)

        # create sample data
        message1 = Message(text='a', recipient=user_1)
        message1.save()

        response = self.client.put('/api/v1/messages/%d/status' % message1.message_id,
                                   headers=auth_header(user_token))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(message1.read_status)
