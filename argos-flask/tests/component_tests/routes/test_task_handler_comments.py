"""Tests the task handler for comments."""
import unittest

from flask import json

from app import create_app, db
from app.models.models import Endpoint, User, Task, Comment
from tests.helper import create_access_token_user, auth_header


class TaskHandlerCommentTestCase(unittest.TestCase):
    """This class represents the task_handler test case for comments."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_comments(self):
        """Check if getting comments of tasks works."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        comment_1 = Comment(text='abcdefg', task=task, creator=creator)
        comment_1.save()
        comment_2 = Comment(text='abcdefg', task=task, creator=creator)
        comment_2.save()

        creator_token = create_access_token_user(creator)
        response = self.client.get('/api/v1/tasks/%d/comments/' % task.task_id,
                                   headers=auth_header(creator_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(json_response['comments']), 2)

        response = self.client.get('/api/v1/tasks/%d/comments/%d' %
                                   (task.task_id, comment_1.comment_id),
                                   headers=auth_header(creator_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['comment_id'], comment_1.comment_id)

        # comment does not belong to task
        response = self.client.get('/api/v1/tasks/%d/comments/3' % task.task_id,
                                   headers=auth_header(creator_token))
        self.assertEqual(response.status_code, 400)

    def test_create_comments(self):
        """Check if creating a comment works."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()

        creator_token = create_access_token_user(creator)
        text = 'This is a comment.'
        comment_data = {
            'text': text,
        }
        response = self.client.post('/api/v1/tasks/%d/comments/' % task.task_id, json=comment_data,
                                    headers=auth_header(creator_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['text'], text)
        url = response.headers['Location']
        response = self.client.get(url, headers=auth_header(creator_token))
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['text'], text)
