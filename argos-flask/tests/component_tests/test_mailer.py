"""Tests the Mailer."""

import unittest

from flask import g

from app import create_app, db, mail
from app.mailer.email import get_locale
from app.models.models import Endpoint, User, Group, Given_Access
from tests.helper import create_access_token_user, auth_header


class MailerTestCase(unittest.TestCase):
    """This class represents the Mailer test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        try:
            self.app = create_app('testing', custom_config={'BABEL_DEFAULT_LOCALE': 'en'})
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config={'BABEL_DEFAULT_LOCALE': 'en'})
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_task_sequential_email(self):
        """Check if emails are sent for a sequential task."""
        # create sample data
        endpoint = Endpoint(name='Test', signature_type='button', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True,
                            additional_access_insert_before_status=True)
        endpoint.save()
        user1 = User(name='John Doe', email_address='john@argos.com',
                     username='john', language='en')
        user1.save()
        access_token_user1 = create_access_token_user(user1)
        given_access = Given_Access(endpoint=endpoint, user=user1, access_order=0)
        given_access.save()

        creator = User(name='Max Mustermann', email_address='max@argos.com',
                       username='max', language='en')
        creator.save()
        access_token_creator = create_access_token_user(creator)
        user2 = User(name='Julee Clears', email_address='jclears0@argos.com',
                     username='jclears0', language='en')
        user2.save()
        access_token_user2 = create_access_token_user(user2)
        user3 = User(name='Brigitta Snare', email_address='bsnare3@argos.com',
                     username='bsnare3', language='en')
        user3.save()
        user4 = User(name='Joanna Mardlin', email_address='jmardlin@argos.com',
                     username='jmardlin', language='en')
        user4.save()
        access_token_user4 = create_access_token_user(user4)
        group = Group(name='Group')
        group.users.append(user2)
        group.users.append(user3)
        group.save()

        with mail.record_messages() as outbox:
            self.client.post('/api/v1/tasks/',
                             json={"endpoint_id": endpoint.endpoint_id, "name": 'Name',
                                   "accesses":
                                       [{"group_id": group.group_id, "access_order": 0},
                                        {"user_id": user4.user_id, "access_order": 1}]},
                             headers=auth_header(access_token_creator))
        self.assertEqual(len(outbox), 3)
        self.assertEqual(outbox[0].recipients[0], 'max@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task creation successful')
        mail_jclears0 = next(mail for mail in outbox if mail.recipients[0] == 'jclears0@argos.com')
        self.assertEqual(mail_jclears0.subject, '[ARGOS] Task waiting for review')
        mail_bsnare3 = next(mail for mail in outbox if mail.recipients[0] == 'bsnare3@argos.com')
        self.assertEqual(mail_bsnare3.subject, '[ARGOS] Task waiting for review')

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "signed"},
                            headers=auth_header(access_token_user2))
        # next user should be notified
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'jmardlin@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task waiting for review')

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "signed"},
                            headers=auth_header(access_token_user4))
        # next user should be notified
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'john@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task waiting for review')

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "signed"},
                            headers=auth_header(access_token_user1))
        # creator should be notified
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'max@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task completed')

    def test_task_parallel_email(self):
        """Check if emails are sent for a parallel task."""
        # create sample data
        endpoint = Endpoint(name='Test', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user1 = User(name='John Doe', email_address='john@argos.com',
                     username='john', language='en')
        user1.save()
        access_token_user1 = create_access_token_user(user1)
        given_access = Given_Access(endpoint=endpoint, user=user1)
        given_access.save()

        creator = User(name='Max Mustermann', email_address='max@argos.com',
                       username='max', language='en')
        creator.save()
        user2 = User(name='Julee Clears', email_address='jclears0@argos.com',
                     username='jclears0', language='en')
        user2.save()
        access_token_user2 = create_access_token_user(user2)
        user3 = User(name='Brigitta Snare', email_address='bsnare3@argos.com',
                     username='bsnare3', language='en')
        user3.save()
        group = Group(name='Group')
        group.users.append(user2)
        group.users.append(user3)
        group.save()
        access_token = create_access_token_user(creator)

        with mail.record_messages() as outbox:
            self.client.post('/api/v1/tasks/',
                             json={"endpoint_id": endpoint.endpoint_id, "name": 'Name',
                                   "accesses": [{"group_id": group.group_id},
                                                {"user_id": user2.user_id}]},
                             headers=auth_header(access_token))

        # users should not get multiple emails
        self.assertEqual(len(outbox), 4)
        self.assertEqual(outbox[0].recipients[0], 'max@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task creation successful')

        # in parallel mode mails are not sent in a specific order
        mail_john = next(mail for mail in outbox if mail.recipients[0] == 'john@argos.com')
        self.assertEqual(mail_john.subject, '[ARGOS] Task waiting for review')
        self.assertTrue('group' not in mail_john.body)
        mail_julee = next(mail for mail in outbox if mail.recipients[0] == 'jclears0@argos.com')
        self.assertEqual(mail_julee.subject, '[ARGOS] Task waiting for review')
        self.assertTrue('group' in mail_julee.body)
        mail_brigitta = next(mail for mail in outbox if mail.recipients[0] == 'bsnare3@argos.com')
        self.assertEqual(mail_brigitta.subject, '[ARGOS] Task waiting for review')
        self.assertTrue('group' in mail_brigitta.body)

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "signed"},
                            headers=auth_header(access_token_user1))
        # creator should not be notified
        self.assertEqual(len(outbox), 0)

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "signed"},
                            headers=auth_header(access_token_user2))
        # creator should be notified
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'max@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task completed')

    def test_task_denied_email(self):
        """Check if email is sent after task denial."""
        endpoint = Endpoint(name='Test', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='John Doe', email_address='john@argos.com',
                       username='john', language='en')
        creator.save()
        access_token_creator = create_access_token_user(creator)
        user = User(name='Max Mustermann', email_address='max@argos.com',
                    username='max', language='en')
        user.save()
        access_token_user = create_access_token_user(user)
        self.client.post('/api/v1/tasks/',
                         json={"endpoint_id": endpoint.endpoint_id, "name": 'Name',
                               "accesses": [{"user_id": user.user_id}]},
                         headers=auth_header(access_token_creator))

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "denied"},
                            headers=auth_header(access_token_user))
        # creator should be notified
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'john@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task denied')

    def test_email_notifications_off(self):
        """Check if email notification settings work."""
        # create sample data
        endpoint = Endpoint(name='Test', signature_type='button', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True,
                            additional_access_insert_before_status=False)
        endpoint.save()
        creator = User(name='Max Mustermann', email_address='max@argos.com',
                       username='max', email_notification_status=False, language='abcd')
        creator.save()
        access_token_creator = create_access_token_user(creator)
        user1 = User(name='John Doe', email_address='john@argos.com',
                     username='john', email_notification_status=False, language='en')
        user1.save()
        access_token_user1 = create_access_token_user(user1)
        given_access = Given_Access(endpoint=endpoint, user=user1, access_order=0)
        given_access.save()
        user2 = User(name='Julee Clears', email_address='jclears0@argos.com', username='jclears0',
                     email_notification_status=False, language='en')
        user2.save()
        user3 = User(name='Brigitta Snare', email_address='bsnare3@argos.com',
                     username='bsnare3', language='en')
        user3.save()
        access_token_user3 = create_access_token_user(user3)
        group = Group(name='Group')
        group.users.append(user2)
        group.users.append(user3)
        group.save()

        with mail.record_messages() as outbox:
            self.client.post('/api/v1/tasks/',
                             json={"endpoint_id": endpoint.endpoint_id, "name": 'Name',
                                   "accesses": [{"group_id": group.group_id,
                                                 "access_order": 0}]},
                             headers=auth_header(access_token_creator))
        self.assertEqual(len(outbox), 1)
        # creator should recieve email regardless of email_notification_status
        self.assertEqual(outbox[0].recipients[0], 'max@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task creation successful')

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "signed"},
                            headers=auth_header(access_token_user1))
        # only bsnare3 wants to receive emails
        self.assertEqual(len(outbox), 1)
        mail_bsnare3 = next(mail for mail in outbox if mail.recipients[0] == 'bsnare3@argos.com')
        self.assertEqual(mail_bsnare3.subject, '[ARGOS] Task waiting for review')

        with mail.record_messages() as outbox:
            self.client.put('/api/v1/tasks/1/status',
                            json={"event": "signed"},
                            headers=auth_header(access_token_user3))
        # creator should be notified regardless of email_notification_status
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'max@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task completed')

    def test_task_commented_email(self):
        """Check if email is sent after task is commented."""
        endpoint = Endpoint(name='Test', signature_type='button', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='John Doe', email_address='john@argos.com',
                       username='john', language='en')
        creator.save()
        access_token_creator = create_access_token_user(creator)
        user = User(name='Max Mustermann', email_address='max@argos.com',
                    username='max', language='en')
        user.save()
        access_token_user = create_access_token_user(user)
        self.client.post('/api/v1/tasks/',
                         json={"endpoint_id": endpoint.endpoint_id, "name": 'Name',
                               "accesses": [{"user_id": user.user_id}]},
                         headers=auth_header(access_token_creator))

        with mail.record_messages() as outbox:
            self.client.post('/api/v1/tasks/1/comments',
                             json={"text": "comments"},
                             headers=auth_header(access_token_user))
        # creator should be notified
        self.assertEqual(len(outbox), 1)
        self.assertEqual(outbox[0].recipients[0], 'john@argos.com')
        self.assertEqual(outbox[0].subject, '[ARGOS] Task commented')

    def test_localeselector(self):
        """localeselector() should return None if no user is set or locale is not supported."""
        # -> Babel will use default locale
        self.assertIsNone(get_locale())
        user = User(name="user", email_address='mail@mail.com',
                    username="test_username", language="abc")
        g.user = user  # set user for localeselector
        self.assertIsNone(get_locale())
