"""Tests the message schema."""
import unittest

from app import create_app, db
from app.models.models import Message, User
from app.models.schemas import MessageSchema


class MessageSchemaTestCase(unittest.TestCase):
    """This class represents the message schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_load_message(self):
        """Checks if messages are loaded correctly by MessageSchema."""
        # create sample data
        user = User(name='username', email_address='mail@mail.com', email_notification_status=False,
                    username="test_username", language="D")
        user.save()
        message_text = 'test'
        message_data = {
            'text': message_text,
            'recipient_id': user.user_id,
        }
        schema = MessageSchema()
        message = schema.load(message_data)
        message.save()
        message = Message.query.first()
        self.assertEqual(message.text, message_text)

    def test_dump_message(self):
        """Checks if messages are dumped correctly by MessageSchema."""
        # create sample data
        user = User(name='username', email_address='mail@mail.com', email_notification_status=False,
                    username="test_username", language="D")
        user.save()
        message_text = 'abc'
        message = Message(text=message_text, recipient=user)
        message.save()
        schema = MessageSchema()
        message_string = schema.dump(message)
        self.assertTrue('message_id' in str(message_string))
        self.assertTrue(("'text': '%s'" % message_text) in str(message_string))
        self.assertTrue('read_status' in str(message_string))
        # dumped message does not contain user_id
        self.assertTrue('recipient_id' not in str(message_string))
