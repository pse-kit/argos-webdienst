"""Tests the endpoint schema."""
import unittest

from app import create_app, db
from app.api.v1.errors import BadRequest
from app.models.models import Given_Access, Endpoint, User, Group, Field
from app.models.schemas import EndpointSchema, GivenAccessSchema


class EndpointSchemaTestCase(unittest.TestCase):
    """This class represents the endpoint schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_endpoint(self):
        """Check if endpoints are dumped correctly by EndpointSchema."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        given_access_user = Given_Access(endpoint=endpoint, access_order=0, user=user)
        given_access_user.save()
        group = Group(name='group')
        group.save()
        given_access_group = Given_Access(endpoint=endpoint, access_order=1, group=group)
        given_access_group.save()
        given_access_schema = GivenAccessSchema()
        given_access_group_data = given_access_schema.dump(given_access_group)
        given_access_user_data = given_access_schema.dump(given_access_user)
        field = Field(name='field_name', field_type='textfield', endpoint=endpoint)
        field.save()
        schema = EndpointSchema()
        endpoint_data = schema.dump(endpoint)
        self.assertTrue(str(given_access_group_data) in str(endpoint_data))
        self.assertTrue(str(given_access_user_data) in str(endpoint_data))

        # endpoint with no given accesses
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox',
                            sequential_status=False, instantiable_status=True,
                            deletable_status=True, email_notification_status=True,
                            allow_additional_accesses_status=True)
        endpoint.save()
        endpoint_data = schema.dump(endpoint)
        self.assertTrue('given_acceses' not in endpoint_data)

    def test_load_endpoint(self):
        """Check if endpoints are loaded correctly by EndpointSchema."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()

        endpoint_data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': True,
            'fields': [{'name': 'field_name', 'field_type': 'textfield'}],
            'given_accesses': [{'user_id': user.user_id, 'access_order': 0}]
        }
        schema = EndpointSchema()
        endpoint = schema.load(endpoint_data)
        endpoint.save()
        endpoint = Endpoint.query.first()
        self.assertTrue(endpoint.name, 'endpoint')
        self.assertEqual(endpoint.fields.first().endpoint_id, endpoint.endpoint_id)
        self.assertEqual(endpoint.given_accesses.first().endpoint_id, endpoint.endpoint_id)

    def test_afterDownload_file_required(self):
        """Check that endpoint has field of type file if signature_type is afterDownload."""
        endpoint_schema = EndpointSchema()
        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkboxAfterDownload',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'fields': [
                {
                    'name': 'file_field',
                    'field_type': 'file',
                    'required_status': False
                }
            ],
            'given_accesses': None
        }
        # file field is not required
        self.assertRaises(BadRequest, lambda: endpoint_schema.load(endpoint_data))

        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkboxAfterDownload',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'fields': [
                {
                    'name': 'file_field',
                    'field_type': 'file',
                },
                {
                    'name': 'field',
                    'field_type': 'textfield',
                    'required_status': False
                }
            ]
        }
        endpoint_schema.load(endpoint_data)

        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkboxAfterDownload',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True
        }
        # no fields are given
        self.assertRaises(BadRequest, lambda: endpoint_schema.load(endpoint_data))

        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkboxAfterDownload',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'fields': [
                {
                    'name': 'field',
                    'field_type': 'textfield'
                }
            ]
        }
        # no field of type file defined
        self.assertRaises(BadRequest, lambda: endpoint_schema.load(endpoint_data))

        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkboxAfterDownload',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'fields': [
                {
                    'name': 'field',
                    'field_type': 'file'
                },
                {
                    'name': 'field',
                    'field_type': 'file'
                }
            ]
        }
        # more than one field of type file is specified
        self.assertRaises(BadRequest, lambda: endpoint_schema.load(endpoint_data))

    def test_validate_endpoint(self):
        """Check if BadRequest is raised by loading incorrect data."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        # unsupported signature type
        endpoint_data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'error',
            'allow_additional_accesses_status': True,
            'fields': [{'name': 'field_name', 'field_type': 'textfield'}],
            'given_accesses': [{'user_id': user.user_id, 'access_order': 0}]
        }
        self.assertRaises(BadRequest, lambda: EndpointSchema().load(endpoint_data))

        # no access_order given for endpoint with sequential mode
        endpoint_data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': True,
            'fields': [{'name': 'field_name', 'field_type': 'textfield'}],
            'given_accesses': [{'user_id': user.user_id}]
        }
        self.assertRaises(BadRequest, lambda: EndpointSchema().load(endpoint_data))

        # access_order invalid
        endpoint_data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': True,
            'fields': [{'name': 'field_name', 'field_type': 'textfield'}],
            'given_accesses': [{'user_id': user.user_id, 'access_order': 1},
                               {'user_id': user.user_id, 'access_order': 3}]
        }
        self.assertRaises(BadRequest, lambda: EndpointSchema().load(endpoint_data))
