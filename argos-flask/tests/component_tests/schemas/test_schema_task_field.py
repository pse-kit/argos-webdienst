"""Tests the task_field schema."""
import unittest

from app import create_app, db
from app.api.v1.errors import BadRequest
from app.models.models import Field, Endpoint, User, Task, Task_Field
from app.models.schemas import TaskFieldSchema


class TaskFieldSchemaTestCase(unittest.TestCase):
    """This class represents the task_field schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_task_field(self):
        """Checks if task_fields are dumped correctly by TaskFieldSchema."""
        # create sample data
        task_name = 'task_name'
        value = 'value'
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name='field_name', field_type='textfield', endpoint_id=endpoint.endpoint_id)
        field.save()
        user = User(name='creator_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        task = Task(name=task_name, creator=user, task_status='pending', endpoint=endpoint)
        task.save()
        task_field = Task_Field(value=value, field_id=field.field_id, task_id=task.task_id)
        task_field.save()
        schema = TaskFieldSchema()
        task_field_data = schema.dump(task_field)
        self.assertTrue(("'name': '%s'" % 'field_name') in str(task_field_data))
        self.assertTrue(("'field_type': '%s'" % 'textfield') in str(task_field_data))
        self.assertTrue("'value': '%s'" % value in str(task_field_data))

    def test_load_task_field(self):
        """Checks if task_fields are loaded correctly by TaskFieldSchema."""
        # create sample data
        task_name = 'task_name'
        value = 'value'
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name='field_name', field_type='textfield', endpoint_id=endpoint.endpoint_id)
        field.save()
        user = User(name='creator_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        task = Task(name=task_name, creator=user, task_status='pending', endpoint=endpoint)
        task.save()
        task_field_data = {
            'value': value,
            'field_id': field.field_id
        }
        task_field = TaskFieldSchema().load(task_field_data)
        task_field.task = task
        task_field.save()
        task_field = Task_Field.query.first()
        self.assertEqual(task_field.field, field)
        self.assertEqual(task_field.task, task)
        self.assertEqual(task_field.value, value)

    def test_validate_task_field(self):
        """Check if ValidationError is raised by loading incorrect data."""
        # field_id does not exist
        task_field_data = {
            'value': '10',
            'field_id': 5
        }
        self.assertRaises(BadRequest, lambda: TaskFieldSchema().load(task_field_data))

        # value does not match field_type
        endpoint1 = Endpoint(name='endpoint_name', signature_type='checkbox',
                             sequential_status=True, instantiable_status=True,
                             deletable_status=True, email_notification_status=True,
                             allow_additional_accesses_status=True)
        endpoint1.save()
        field_checkbox = Field(name='field_checkbox', field_type='checkbox',
                               endpoint_id=endpoint1.endpoint_id)
        field_checkbox.save()
        user = User(name='creator_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        task1 = Task(name='task_name', creator=user, task_status='pending', endpoint=endpoint1)
        task1.save()
        task_field_data = {
            'value': 'a',
            'field_id': field_checkbox.field_id
        }
        self.assertRaises(BadRequest, lambda: TaskFieldSchema().load(task_field_data))

        endpoint2 = Endpoint(name='endpoint_name', signature_type='checkbox',
                             sequential_status=True, instantiable_status=True,
                             deletable_status=True, email_notification_status=True,
                             allow_additional_accesses_status=True)
        endpoint2.save()
        field_datetime = Field(name='field_datetime', field_type='datetime',
                               endpoint_id=endpoint2.endpoint_id)
        field_datetime.save()
        task2 = Task(name='task_name', creator=user, task_status='pending', endpoint=endpoint2)
        task2.save()
        task_field_data = {
            'value': 'a',
            'field_id': field_datetime.field_id
        }
        self.assertRaises(BadRequest, lambda: TaskFieldSchema().load(task_field_data))
