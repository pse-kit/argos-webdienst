"""Tests the user schema."""
import unittest

from app import create_app, db
from app.models.models import User
from app.models.schemas import UserSchema


class UserSchemaTestCase(unittest.TestCase):
    """This class represents the user schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_user(self):
        """Checks if users are dumped correctly by UserSchema."""
        user = User(name='username', email_address='mail@mail.com', email_notification_status=False,
                    username='test_username', language='D')
        user.save()
        schema = UserSchema()
        result = schema.dump(user)
        self.assertTrue("'user_id': %d" % user.user_id in str(result))
        self.assertTrue("'name': '%s'" % user.name in str(result))
        self.assertTrue("'email_address': '%s'" % user.email_address in str(result))
