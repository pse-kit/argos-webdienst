"""Tests the group schema."""
import unittest

from app import create_app, db
from app.models.models import Group
from app.models.schemas import GroupSchema


class GroupSchemaTestCase(unittest.TestCase):
    """This class represents the group schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_group(self):
        """Checks if groups are dumped correctly by GroupSchema."""
        test_name = 'test'
        test_description = 'description'
        group = Group(name=test_name, description=test_description)
        group.save()
        schema = GroupSchema()
        result = schema.dump(group)
        self.assertTrue("'group_id'" in str(result))
        self.assertTrue("'name': '%s'" % group.name in str(result))
        self.assertTrue("'description': '%s'" % group.description in str(result))
