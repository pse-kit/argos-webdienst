"""Tests the comment schema."""
import unittest

from app import create_app, db
from app.api.v1.errors import BadRequest
from app.models.models import Comment, Endpoint, Task, User
from app.models.schemas import CommentSchema, UserSchema


class CommentSchemaTestCase(unittest.TestCase):
    """This class represents the comment schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_comment(self):
        """Check if comments are dumped correctly by CommentSchema."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        user_schema = UserSchema()
        user_data = user_schema.dump(user)
        comment_text = 'abcdefg'
        comment = Comment(text=comment_text, task=task, creator=user)
        comment.save()
        comment_data = CommentSchema().dump(comment)
        self.assertTrue('comment_id' in str(comment_data))
        self.assertTrue(("'text': '%s'" % comment_text) in str(comment_data))
        self.assertTrue("'comment_date" in str(comment_data))
        self.assertTrue("'creator': %s" % str(user_data) in str(comment_data))

    def test_load_comment(self):
        """Check if comments are dumped correctly by CommentSchema."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        comment_string = {
            'text': 'hello',
            'creator_id': user.user_id,
            'task_id': task.task_id
        }
        result = CommentSchema().load(comment_string)
        result.save()
        comment = Comment.query.first()
        self.assertTrue(comment.creator, user)

    def test_validate_comment(self):
        """Check if ValidationError is raised by loading incorrect data."""
        # create sample data
        comment_string = {
            'text': 'hello',
            'comment_date': 'abc'
        }
        self.assertRaises(BadRequest, lambda: CommentSchema().load(comment_string))

        # invalid task_id
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        comment_string = {
            'text': 'hello',
            'creator_id': creator.user_id,
            'task_id': 1
        }
        self.assertRaises(BadRequest, lambda: CommentSchema().load(comment_string))
