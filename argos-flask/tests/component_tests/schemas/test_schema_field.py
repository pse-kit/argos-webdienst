"""Tests the field schema."""
import unittest

from app import create_app, db
from app.api.v1.errors import BadRequest
from app.models.models import Field, Endpoint
from app.models.schemas import FieldSchema


class FieldSchemaTestCase(unittest.TestCase):
    """This class represents the field schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_field(self):
        """Checks if fields are dumped correctly by FieldSchema."""
        # create sample data
        endpoint_name = 'endpoint_name'
        field_name = 'field_name'
        field_type = 'textfield'
        endpoint = Endpoint(name=endpoint_name, signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name=field_name, field_type=field_type, endpoint=endpoint)
        field.save()
        schema = FieldSchema()
        field_data = schema.dump(field)
        self.assertTrue('field_id' in str(field_data))
        self.assertTrue(("'name': '%s'" % field_name) in str(field_data))
        self.assertTrue(("'field_type': '%s'" % field_type) in str(field_data))
        self.assertTrue(("'required_status': True") in str(field_data))

    def test_load_field(self):
        """Checks if fields are loaded correctly by FieldSchema."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field_name = 'name'
        field_type = 'textfield'

        field_data = {
            'name': field_name,
            'field_type': 'textfield'
        }
        field = FieldSchema().load(field_data)
        field.endpoint = endpoint
        field.save()
        field = Field.query.first()
        self.assertEqual(field.name, field_name)
        self.assertEqual(field.field_type, field_type)
        self.assertEqual(field.endpoint, endpoint)

    def test_validate_given_access(self):
        """Check if ValidationError is raised by loading incorrect data."""
        # create sample data
        field_data = {
            'name': 'abc',
            'field_type': 'test'
        }
        self.assertRaises(BadRequest, lambda: FieldSchema().load(field_data))
