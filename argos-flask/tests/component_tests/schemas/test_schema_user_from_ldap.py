"""Tests the user schema."""
import unittest

from app import create_app, db
from app.models.schemas import UserSchemaLDAP


class UserSchemaTestCase(unittest.TestCase):
    """This class represents the user schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_load_user(self):
        """Test for correct creation of group from LDAP."""
        schema = UserSchemaLDAP()
        user = schema.make_user({'cn': ['Effi Briest'],
                                 'mail': ['briest@example.com'],
                                 'objectClass': ['inetOrgPerson', 'top'],
                                 'sn': ['Briest'], 'uid': ['briesteffi'],
                                 'userPassword': [b'{MD5}X03MO1qnZdYdgyfeuILPmQ=='],
                                 'dn': 'uid=briesteffi,ou=users,dc=pse-kit,dc=de'})
        self.assertTrue(user.name == 'Effi Briest')
        self.assertTrue(user.email_address == 'briest@example.com')
