"""Tests the task schema."""
import unittest

from app import create_app, db
from app.api.v1.errors import BadRequest
from app.models.models import Access, Given_Access, Field
from app.models.models import Task_Field, Endpoint, User, Group, Task
from app.models.schemas import AccessSchema, TaskSchema, TaskFieldSchema, UserSchema, EndpointSchema


class TaskSchemaTestCase(unittest.TestCase):
    """This class represents the task schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_task(self):
        """Check if tasks are dumped correctly by TaskSchema."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        creator_data = UserSchema().dump(creator)
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field1 = Field(name='field1_name', field_type='textfield', endpoint=endpoint)
        field2 = Field(name='field2_name', field_type='datetime', endpoint=endpoint)
        field1.save()
        field2.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        accessUser = Access(access_status="pending", task=task, user=user, access_order=0)
        accessUser.save()
        group = Group(name='group')
        group.save()
        accessGroup = Access(access_status='pending', task=task, group=group, access_order=1)
        accessGroup.save()
        accesses = AccessSchema(many=True).dump([accessUser, accessGroup])
        task_field1 = Task_Field(field=field1, task=task, value="abcd")
        task_field1.save()
        task_field2 = Task_Field(field=field2, task=task, value='timestamp')
        task_field2.save()
        tasks_fields = TaskFieldSchema(many=True).dump([task_field1, task_field2])
        task_data = TaskSchema().dump(task)
        self.assertTrue(str(task_field) in str(task_data) for task_field in tasks_fields)
        self.assertTrue(str(access) in str(task_data) for access in accesses)
        self.assertTrue(str(creator_data) in str(task_data))

    def test_load_task(self):
        """Check if tasks are loaded correctly by TaskSchema."""
        # create sample data
        creator = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                       username="username", language="de_DE")
        creator.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username2", language="de_DE")
        user.save()

        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field1 = Field(name='field1_name', field_type='textfield', endpoint=endpoint)
        field1.save()
        value = 'abced'
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'name': 'name',
            'creator_id': creator.user_id,
            'tasks_fields': [{'field_id': field1.field_id, 'value': value}],
            'accesses': [{'user_id': user.user_id, 'access_order': 0},
                         {'user_id': user.user_id, 'access_order': 1}],
        }
        task = TaskSchema().load(task_data)
        task.save()
        task = Task.query.first()
        self.assertEqual(task.accesses.first().user_id, user.user_id)
        self.assertEqual(task.tasks_fields.first().value, value)
        self.assertEqual(task.endpoint, endpoint)

    def test_add_aditional_accesses(self):
        """Check if given accesses of endpoint are added to task correctly."""
        # Test task with parallel mode. (Access order is not considered.)
        creator = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                       username="username", language="de_DE")
        creator.save()
        user1 = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                     username="username1", language="de_DE")
        user2 = User(name="user1", email_address='mail@mail.com', email_notification_status=False,
                     username="username2", language="de_DE")
        user3 = User(name="user2", email_address='mail@mail.com', email_notification_status=False,
                     username="username3", language="de_DE")
        user1.save()
        user2.save()
        user3.save()
        group = Group(name="group")
        group.save()
        endpoint_par = Endpoint(name='endpoint_par', signature_type='checkbox',
                                sequential_status=False, instantiable_status=True,
                                deletable_status=True, email_notification_status=True,
                                allow_additional_accesses_status=True)
        endpoint_par.save()
        given_access = Given_Access(group_id=group.group_id, endpoint=endpoint_par)
        given_access.save()
        task_data = {
            'endpoint_id': endpoint_par.endpoint_id,
            'creator_id': user2.user_id,
            'name': 'name',
            'accesses': [{'user_id': user1.user_id, 'access_order': 10}]
        }
        task = TaskSchema().load(task_data)
        task.creator = creator
        task.save()
        accesses = task.accesses.all()
        self.assertEqual(len(accesses), 2)
        self.assertTrue(task.accesses.filter_by(user_id=user1.user_id).first() is not None)
        self.assertTrue(task.accesses.filter_by(group_id=group.group_id).first() is not None)

        # Test task with sequential mode. Insert additional accesses before given accesses.
        endpoint_seq_before = Endpoint(name='endpoint_seq_before', signature_type='checkbox',
                                       sequential_status=True, instantiable_status=True,
                                       deletable_status=True, email_notification_status=True,
                                       allow_additional_accesses_status=True)
        endpoint_seq_before.save()
        given_access1 = Given_Access(group=group, endpoint=endpoint_seq_before, access_order=0)
        given_access2 = Given_Access(user=user3, endpoint=endpoint_seq_before, access_order=1)
        given_access1.save()
        given_access2.save
        task_data = {
            'endpoint_id': endpoint_seq_before.endpoint_id,
            'name': 'name',
            'creator_id': user3.user_id,
            'accesses': [{'user_id': user1.user_id, 'access_order': 0},
                         {'user_id': user2.user_id, 'access_order': 1}]
        }
        task = TaskSchema().load(task_data)
        task.save()
        # helper dictionary for easier testing
        dict_access_order = {access.access_order: access for access in task.accesses.all()}
        self.assertEqual(len(dict_access_order), 4)
        self.assertEqual(dict_access_order.get(0).user, user1)
        self.assertEqual(dict_access_order.get(1).user, user2)
        self.assertEqual(dict_access_order.get(2).group, group)
        self.assertEqual(dict_access_order.get(3).user, user3)

        # Test task with sequential mode. Insert additional accesses after given accesses.
        endpoint_seq_after = Endpoint(name='endpoint_seq_after', signature_type='checkbox',
                                      sequential_status=True, instantiable_status=True,
                                      deletable_status=True, email_notification_status=True,
                                      allow_additional_accesses_status=True,
                                      additional_access_insert_before_status=False)
        endpoint_seq_after.save()
        given_access1 = Given_Access(group=group, endpoint=endpoint_seq_after, access_order=0)
        given_access2 = Given_Access(user=user3, endpoint=endpoint_seq_after, access_order=1)
        given_access1.save()
        given_access2.save
        task_data = {
            'endpoint_id': endpoint_seq_after.endpoint_id,
            'creator_id': user3.user_id,
            'name': 'name',
            'accesses': [{'user_id': user1.user_id, 'access_order': 0},
                         {'user_id': user2.user_id, 'access_order': 1}]
        }
        task = TaskSchema().load(task_data)
        task.save()
        # helper dictionary for easier testing
        dict_access_order = {access.access_order: access for access in task.accesses.all()}
        self.assertEqual(len(dict_access_order), 4)
        self.assertEqual(dict_access_order.get(2).user, user1)
        self.assertEqual(dict_access_order.get(3).user, user2)
        self.assertEqual(dict_access_order.get(0).group, group)
        self.assertEqual(dict_access_order.get(1).user, user3)

    def test_required_fields(self):
        """Check if required_status of fields are handled as expected."""
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        endpoint_schema = EndpointSchema()
        task_schema = TaskSchema()

        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkbox',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'given_accesses': [
                {
                    'user_id': user.user_id,
                    'access_order': 0
                },
            ],
            'fields': [
                {
                    'name': 'field_name',
                    'field_type': 'textfield',
                    'required_status': False
                },
                {
                    'name': 'field_2',
                    'field_type': 'checkbox',
                    'required_status': True
                },
                {
                    'name': 'file_test',
                    'field_type': 'file',
                    'required_status': True
                },
                {
                    'name': 'optional',
                    'field_type': 'textfield',
                    'required_status': False
                },
                {
                    'name': 'required',
                    'field_type': 'textfield',
                    'required_status': True
                }
            ]
        }

        endpoint = endpoint_schema.load(endpoint_data)
        endpoint.save()
        task_data = {
            'name': 'test_task',
            'endpoint_id': endpoint.endpoint_id,
            'tasks_fields': [
                {
                    'field_id': 2,
                    'value': 'true'
                },
                {
                    'field_id': 5,
                    'value': 'value'
                }
            ]
        }
        task_schema.load(task_data)

        task_data = {
            'name': 'test_task',
            'endpoint_id': endpoint.endpoint_id,
            'tasks_fields': [
                {
                    'field_id': 2,
                    'value': 'true'
                },
                {
                    'field_id': 5,
                    'value': 'value'
                }
            ]
        }
        task_schema.load(task_data)

        task_data = {
            'name': 'test_task',
            'endpoint_id': endpoint.endpoint_id,
            'tasks_fields': [
                {
                    'field_id': 2,
                    'value': 'true'
                },
                {
                    'field_id': 1,
                    'value': None
                },
                {
                    'field_id': 3,
                    'value': 'something'
                },
                {
                    'field_id': 5,
                    'value': 'test'
                }
            ]
        }
        # file fields are not allowed
        self.assertRaises(BadRequest, lambda: task_schema.load(task_data))

        task_data = {
            'name': 'test_task',
            'endpoint_id': endpoint.endpoint_id,
            'tasks_fields': [
                {
                    'field_id': 2,
                    'value': 'value'
                },
                {
                    'field_id': 1,
                    'value': None
                }
            ]
        }
        # missing required field
        self.assertRaises(BadRequest, lambda: task_schema.load(task_data))

    def test_validate_task(self):
        """Check if ValidationError is raised by loading incorrect data."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username2", language="de_DE")
        user.save()
        user_2 = User(name="user2", email_address='mail@mail.com', email_notification_status=False,
                      username="username3", language="de_DE")
        user_2.save()

        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox',
                            sequential_status=False, instantiable_status=True,
                            deletable_status=True, email_notification_status=True,
                            allow_additional_accesses_status=False)
        endpoint.save()
        # non-existent endpoint_id
        task_data = {
            'endpoint_id': 2,
            'creator_id': user.user_id,
            'name': 'name',
            'accesses': [{'user_id': user.user_id, 'access_order': 0}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        # non-existent endpoint_id
        task_data = {
            'endpoint_id': 2,
            'name': 'name',
        }

        # no additional accesses allowed
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'creator_id': user.user_id,
            'name': 'name',
            'accesses': [{'user_id': user_2.user_id}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        field1 = Field(name='field1_name', field_type='textfield', endpoint=endpoint)
        field1.save()

        endp2 = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                         instantiable_status=True, deletable_status=True,
                         email_notification_status=True, allow_additional_accesses_status=True)
        field2 = Field(name='field2_name', field_type='textfield',
                       endpoint=endp2, required_status=False)
        field2.save()

        # referenced field does not belong to endpoint of task
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'name': 'name',
            'creator_id': user.user_id,
            'tasks_fields': [{'field_id': 2, 'value': 'value'}],
            'accesses': [{'user_id': user_2.user_id, 'access_order': 1}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        # value for same field is given twice
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'name': 'name',
            'creator_id': user.user_id,
            'tasks_fields': [{'field_id': 1, 'value': 'value'},
                             {'field_id': 1, 'value': 'value'}],
            'accesses': [{'user_id': user_2.user_id, 'access_order': 1}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        # no accesses are defined
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'name': 'name',
            'creator_id': user.user_id,
            'tasks_fields': [{'field_id': 1, 'value': 'value'}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        # access_order invalid
        task_data = {
            'endpoint_id': endp2.endpoint_id,
            'creator_id': user.user_id,
            'name': 'name',
            'tasks_fields': [{'field_id': 2, 'value': 'value'}],
            'accesses': [{'user_id': user_2.user_id, 'access_order': 1},
                         {'user_id': user_2.user_id, 'access_order': 3}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        # user cannot add himself as access
        task_data = {
            'endpoint_id': endp2.endpoint_id,
            'creator_id': user.user_id,
            'name': 'name',
            'tasks_fields': [{'field_id': 2, 'value': 'value'}],
            'accesses': [{'user_id': user.user_id, 'access_order': 1}]
        }

        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        group = Group(name='group')
        group.users.append(user)
        group.save()

        # user cannot add a group of which he is the only member as access
        task_data = {
            'endpoint_id': endpoint.endpoint_id,
            'creator_id': user.user_id,
            'name': 'name',
            'tasks_fields': [{'field_id': 1, 'value': 'value'}],
            'accesses': [{'group_id': group.group_id, 'access_order': 1}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))

        # sequential status but no access order given
        task_data = {
            'endpoint_id': endp2.endpoint_id,
            'creator_id': user.user_id,
            'name': 'name',
            'tasks_fields': [{'field_id': 2, 'value': 'value'}],
            'accesses': [{'user_id': user_2.user_id}]
        }
        self.assertRaises(BadRequest, lambda: TaskSchema().load(task_data))
