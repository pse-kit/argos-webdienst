"""Tests the given_access schema."""
import unittest

from app import create_app, db
from app.api.v1.errors import BadRequest
from app.models.models import Given_Access, Endpoint, User, Group
from app.models.schemas import GivenAccessSchema


class GivenAccessSchemaTestCase(unittest.TestCase):
    """This class represents the given_access schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_given_access(self):
        """Checks if given_accesses are dumped correctly by GivenAccessSchema."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        given_access = Given_Access(endpoint=endpoint, user=user)
        given_access.save()
        schema = GivenAccessSchema()
        given_access_data = schema.dump(given_access)
        self.assertTrue(("'user_id': %d" % user.user_id) in str(given_access_data))
        self.assertTrue(("'name': '%s'" % user.name) in str(given_access_data))

    def test_load_given_access(self):
        """Checks if given_access are loaded correctly by GivenAccessSchema."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        access_order = 0
        given_access_data = {
            'access_order': access_order,
            'user_id': user.user_id
        }
        schema = GivenAccessSchema()
        given_access = schema.load(given_access_data)
        given_access.endpoint = endpoint
        given_access.save()
        given_access = Given_Access.query.first()
        self.assertEqual(given_access.access_order, access_order)
        self.assertEqual(given_access.user, user)
        self.assertEqual(given_access.endpoint, endpoint)

    def test_validate_given_access(self):
        """Check if ValidationError is raised by loading incorrect data."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        # non-existent user_id
        given_access_data = {
            'access_order': 1,
            'user_id': 4
        }
        self.assertRaises(BadRequest, lambda: GivenAccessSchema().load(given_access_data))
        # non-existent group_id
        given_access_data = {
            'access_order': 1,
            'group_id': 4
        }
        self.assertRaises(BadRequest, lambda: GivenAccessSchema().load(given_access_data))
        # both group_id and user_id are set
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        group = Group(name='group')
        group.save()
        given_access_data = {
            'access_order': 1,
            'group_id': group.group_id,
            'user_id': user.user_id
        }
        self.assertRaises(BadRequest, lambda: GivenAccessSchema().load(given_access_data))
        # neither group_id nor user_id is set
        given_access_data = {
            'access_order': 1,
        }
        self.assertRaises(BadRequest, lambda: GivenAccessSchema().load(given_access_data))
