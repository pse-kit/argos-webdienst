"""Tests the access schema."""
import unittest

from app import create_app, db
from app.api.v1.errors import BadRequest
from app.models.models import Access, Endpoint, User, Group, Task
from app.models.schemas import AccessSchema


class AccessSchemaTestCase(unittest.TestCase):
    """This class represents the access schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dump_access(self):
        """Check if accesses are dumped correctly by AccessSchema."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='task', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        access = Access(access_status="pending", task=task, user=user)
        access.save()
        schema = AccessSchema()
        access_data = schema.dump(access)
        self.assertTrue(("'user_id': %d" % user.user_id) in str(access_data))
        self.assertTrue(("'access_status': 'pending'") in str(access_data))

    def test_load_access(self):
        """Check if accesses are loaded correctly by AccessSchema."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='task', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        access_order = 0
        access_data = {
            'access_order': access_order,
            'user_id': user.user_id
        }
        schema = AccessSchema()
        access = schema.load(access_data)
        access.task = task
        access.save()
        access = Access.query.first()
        self.assertEqual(access.access_order, access_order)
        self.assertEqual(access.user, user)
        self.assertEqual(access.task, task)

    def test_validate_access(self):
        """Check if ValidationError is raised by loading incorrect data."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=False,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='task', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        group = Group(name='group')
        group.save()
        # not existent user_id
        access_data = {
            'user_id': 4
        }
        self.assertRaises(BadRequest, lambda: AccessSchema().load(access_data))
        # not existent group_id
        access_data = {
            'group_id': 4
        }
        self.assertRaises(BadRequest, lambda: AccessSchema().load(access_data))
        # both group_id and user_id are set
        access_data = {
            'group_id': group.group_id,
            'user_id': user.user_id
        }
        self.assertRaises(BadRequest, lambda: AccessSchema().load(access_data))
        # neither group_id nor user_id is set
        access_data = {
            'access_order': 1
        }
        self.assertRaises(BadRequest, lambda: AccessSchema().load(access_data))
