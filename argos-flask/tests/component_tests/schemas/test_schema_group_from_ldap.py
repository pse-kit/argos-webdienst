"""Tests the group_ldp schema."""
import unittest

from app import create_app, db
from app.models.schemas import GroupSchemaLDAP


class UserSchemaTestCase(unittest.TestCase):
    """This class represents the group schema test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_load_group(self):
        """Test for correct creation of group from LDAP."""
        schema = GroupSchemaLDAP()
        group = schema.make_group({'cn': ['Vorstand'], 'description': ['Beschreibung Vorstand'],
                                   'objectClass': ['groupOfUniqueNames', 'top'],
                                   'uniqueMember': ['cn=leitmori,ou=users,dc=pse-kit,dc=de'],
                                   'dn': 'cn=Vorstand,ou=groups,dc=pse-kit,dc=de'})
        self.assertEqual(group.name, 'Vorstand')
        self.assertEqual(group.description, 'Beschreibung Vorstand')
