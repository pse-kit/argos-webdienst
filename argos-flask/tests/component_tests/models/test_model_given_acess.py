"""Tests the given_access model."""
import unittest

from app import create_app, db
from app.models.models import Given_Access, Endpoint, User, Group


class Given_AccessModelTestCase(unittest.TestCase):
    """This class represents the given_access model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new given_access works."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        # check for success
        given_access = Given_Access(endpoint=endpoint, user=user)
        given_access.save()
        self.assertTrue(given_access.given_access_id > 0)

    def test_endpoint_relationship(self):
        """Check whether relationship to endpoint works."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        given_access = Given_Access(endpoint=endpoint, user=user)
        given_access.save()
        query = Given_Access.query.first()
        # test relationship
        self.assertEqual(query.endpoint, endpoint)

    def test_user_relationship(self):
        """Check whether relationship to user works."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        given_access = Given_Access(endpoint=endpoint, user=user)
        given_access.save()
        query = Given_Access.query.first()
        # test relationship
        self.assertEqual(query.user, user)

    def test_group_relationship(self):
        """Check whether relationship to group works."""
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        group = Group(name='group')
        group.save()
        given_access = Given_Access(endpoint=endpoint, group=group)
        given_access.save()
        query = Given_Access.query.first()
        # test relationship
        self.assertEqual(query.group, group)
