"""Tests the message model."""
import unittest

from app import create_app, db
from app.models.models import Message, User


class MessageModelTestCase(unittest.TestCase):
    """This class represents the message model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new message works."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        message = Message(text='abcdefg', recipient=user)
        message.save()
        # check for success
        self.assertTrue(message.message_id > 0)
        self.assertTrue(message.message_date is not None)

    def test_user_relationship(self):
        """Checks if backref from message to user works."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        message = Message(text='abcdefg', recipient=user)
        message.save()
        query = Message.query.first()
        # test relationship
        self.assertEqual(user, query.recipient)
