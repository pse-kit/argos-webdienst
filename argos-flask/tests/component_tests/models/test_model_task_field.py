"""Tests the task_field model."""
import unittest

from app import create_app, db
from app.models.models import Endpoint, User, Task, Task_Field, Field


class TaskFieldModelTestCase(unittest.TestCase):
    """This class represents the task_field model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Checks if creating a new task_field works."""
        task_name = 'task_name'
        value = 'value'
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name='field_name', field_type='textfield', endpoint_id=endpoint.endpoint_id)
        field.save()
        user = User(name='creator_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        task = Task(name=task_name, creator=user, task_status='pending', endpoint=endpoint)
        task.save()
        task_field = Task_Field(value=value, field_id=field.field_id, task_id=task.task_id)
        task_field.save()
        self.assertTrue(task_field.task_field_id > 0)
        t = Task.query.filter_by(name=task_name).first()
        self.assertEqual(task_field, t.tasks_fields[0])
