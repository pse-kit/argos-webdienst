"""Tests the field model."""
import unittest

from app import create_app, db
from app.models.models import Field, Endpoint


class FieldModelTestCase(unittest.TestCase):
    """This class represents the field model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Checks if creating a new field works."""
        endpoint_name = 'endpoint_name'
        field_name = 'field_name'
        endpoint = Endpoint(name=endpoint_name, signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field = Field(name=field_name, field_type='textfield', endpoint=endpoint)
        field.save()
        self.assertTrue(field.field_id > 0)
        f = Field.query.first()
        self.assertEqual(f, field)

    def test_endpoint_relationship(self):
        """Check whether endpoint relationship works."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        Field(name='field1_name', field_type='textfield', endpoint=endpoint)
        f = Field.query.first()
        # test relationship
        self.assertEqual(f.endpoint, endpoint)
