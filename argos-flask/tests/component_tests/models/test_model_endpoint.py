"""Tests the endpoint model."""
import unittest

from app import create_app, db
from app.models.models import Given_Access, Field, Endpoint, User, Group, Task


class EndpointModelTestCase(unittest.TestCase):
    """This class represents the endpoint model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new endpoint works."""
        endpoint = Endpoint(name="test", signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True,
                            additional_access_insert_before_status=True)
        endpoint.save()
        self.assertTrue(endpoint.endpoint_id > 0)
        query = Endpoint.query.first()
        self.assertEqual(endpoint, query)

    def test_tasks_relationship(self):
        """Check whether tasks relationship works."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user1 = User(name="user1", email_address='mail@mail.com', email_notification_status=False,
                     username="username1", language="de_DE")
        user1.save()
        user2 = User(name="user2", email_address='a@b.com', email_notification_status=False,
                     username="username2", language="de_DE")
        user2.save()
        user2.save()
        task1 = Task(name='test', creator=user1, endpoint=endpoint)
        task1.save()
        task2 = Task(name='test', creator=user2, endpoint=endpoint)
        task1.save()
        query = Endpoint.query.first()
        # test relationship
        self.assertEqual(task1, query.tasks[0])
        self.assertEqual(task2, query.tasks[1])

    def test_given_accesses_relationship(self):
        """Check whether given_accesses relationship works."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        given_access_user = Given_Access(endpoint=endpoint, access_order=0, user=user)
        given_access_user.save()
        group = Group(name='group')
        group.save()
        given_access_group = Given_Access(endpoint=endpoint, access_order=1, group=group)
        given_access_group.save()
        query = Endpoint.query.first()
        # test relationship
        self.assertEqual(given_access_user, query.given_accesses[0])
        self.assertEqual(given_access_group, query.given_accesses[1])

    def test_fields_endpoint(self):
        """Checks if backref from endpoint to fields works."""
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field1 = Field(name='field1_name', field_type='textfield', endpoint=endpoint)
        field2 = Field(name='field2_name', field_type='textfield', endpoint=endpoint)
        field1.save()
        field2.save()
        self.assertTrue(field1.field_id > 0)
        self.assertTrue(field2.field_id > 0)
        self.assertTrue(field1.field_id < field2.field_id)
        e = Endpoint.query.filter_by(name='endpoint_name').first()
        self.assertEqual(field1, e.fields[0])
        self.assertEqual(field2, e.fields[1])
