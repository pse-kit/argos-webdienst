"""Tests the user model."""
import unittest

from app import create_app, db
from app.models.models import Message, Endpoint, User, Group, Task


class UserModelTestCase(unittest.TestCase):
    """This class represents the user model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Checks if creating a new user works."""
        nameExpected = 'test'
        user = User(name=nameExpected, email_address='a@b.com', email_notification_status=False,
                    username="test_username", language="de_DE")
        user.save()
        self.assertTrue(user.user_id > 0)
        query = User.query.first()
        self.assertEqual(user, query)

    def test_tasks_relationship(self):
        """Checks if backref from user to tasks works."""
        # create sample data
        creator_name = 'user_name'
        task1_name = 'task1'
        task2_name = 'task2'
        user = User(name=creator_name, email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        endpoint1 = Endpoint(name='endpoint1', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)
        endpoint2 = Endpoint(name='endpoint2', signature_type='checkbox', sequential_status=True,
                             instantiable_status=True, deletable_status=True,
                             email_notification_status=True, allow_additional_accesses_status=True)

        task1 = Task(name=task1_name, creator=user, task_status='pending', endpoint=endpoint1)
        task2 = Task(name=task2_name, creator=user, task_status='pending', endpoint=endpoint2)
        task1.save()
        task2.save()
        # test relationship
        self.assertTrue(task1.task_id > 0)
        self.assertTrue(task2.task_id > 0)
        self.assertTrue(task1.task_id < task2.task_id)
        u = User.query.filter_by(name=creator_name).first()
        self.assertEqual(u.tasks.filter_by(name=task1_name).first(), task1)
        self.assertEqual(u.tasks.filter_by(name=task2_name).first(), task2)

    def test_groups_relationship(self):
        """Check whether access to group works from a user."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="test_username", language="de_DE")
        user.save()
        group = Group(name="test", description="description")
        group.users.append(user)
        query = User.query.first()
        # test relationship
        self.assertEqual(query.groups[0], group)

    def test_messages_relationship(self):
        """Check whether access to messages works from a user."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="test_username", language="de_DE")
        user.save()
        message1 = Message(text='abcdefg', recipient=user)
        message1.save()
        message2 = Message(text='abcdefghj', recipient=user)
        message2.save()
        query = User.query.first()
        # test relationship
        self.assertEqual(query.messages[0], message1)
        self.assertEqual(query.messages[1], message2)
