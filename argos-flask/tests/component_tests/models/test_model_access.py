"""Tests the access model."""
import datetime
import unittest

from app import create_app, db
from app.models.models import Access, Endpoint, User, Group, Task


class AccessModelTestCase(unittest.TestCase):
    """This class represents the access model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new access works."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='task', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        timestamp = datetime.datetime.utcnow()
        # test access
        access = Access(access_status="pending", task=task, event_date=timestamp)
        access.save()
        self.assertTrue(access.access_id > 0)

    def test_task_relationship(self):
        """Check whether relationship to task works."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='task', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        timestamp = datetime.datetime.utcnow()
        access = Access(access_status="pending", task=task, event_date=timestamp)
        access.save()
        query = Access.query.first()
        # test relationship
        self.assertEqual(query.task, task)

    def test_user_relationship(self):
        """Check whether relationship to user works."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='task', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        access = Access(access_status="pending", task=task, user=user)
        access.save()
        query = Access.query.first()
        # test relationship
        self.assertEqual(query.user, user)

    def test_group_relationship(self):
        """Check whether relationship to group works."""
        # create sample data
        endpoint = Endpoint(name='endpoint', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        creator_name = 'user_name'
        creator = User(name=creator_name, email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        task = Task(name='task', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        group = Group(name='group')
        group.save()
        access = Access(access_status='signed', task=task, group=group)
        access.save()
        # test relationship
        query = Access.query.first()
        self.assertEqual(query.group, group)
