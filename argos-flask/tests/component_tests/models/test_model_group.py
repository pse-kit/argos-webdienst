"""Tests the group model."""
import unittest

from app import create_app, db
from app.models.models import User, Group


class GroupModelTestCase(unittest.TestCase):
    """This class represents the group model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new group works."""
        group = Group(name="test", description="description")
        group.save()
        self.assertTrue(group.group_id > 0)
        query = Group.query.first()
        self.assertEqual(query, group)

    def test_users_relationship(self):
        """Check whether access to user in a group works."""
        group = Group(name="test", description="description")
        group.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="test_username", language="de_DE")
        user.groups.append(group)
        user.save()
        query = Group.query.first()
        self.assertEqual(query.users[0], user)
