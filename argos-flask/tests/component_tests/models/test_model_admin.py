"""Tests the admin model."""
import unittest

from app import create_app, db
from app.models.models import Admin


class AdminModelTestCase(unittest.TestCase):
    """This class represents the admin model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new admin works."""
        admin = Admin(name='admin', email_address='a@b.com',
                      username='admin_username', password='abc')
        admin.save()
        self.assertTrue(admin.admin_id > 0)
        query = Admin.query.first()
        self.assertEqual(admin, query)
        self.assertTrue(admin.password_hash is not None)
        with self.assertRaises(AttributeError):
            admin.password

    def test_password(self):
        """Check if password verification works."""
        admin = Admin(name='admin', email_address='a@b.com', username='admin_username',
                      password='abc')
        self.assertTrue(admin.verify_password('abc'))
        self.assertFalse(admin.verify_password('12345'))

    def test_password_salts_are_random(self):
        """Check if salts are random."""
        admin1 = Admin(password='abc')
        admin2 = Admin(password='abc')
        self.assertTrue(admin1.password_hash != admin2.password_hash)
