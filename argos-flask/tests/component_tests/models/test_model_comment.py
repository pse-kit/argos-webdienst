"""Tests the comment model."""
import unittest

from app import create_app, db
from app.models.models import Comment, Endpoint, User, Task


class CommentModelTestCase(unittest.TestCase):
    """This class represents the comment model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new comment works."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        comment = Comment(text='abcdefg', task=task, creator=user)
        comment.save()
        # check for success
        self.assertTrue(comment.comment_id > 0)
        self.assertTrue(comment.comment_date is not None)

    def test_task_relationship(self):
        """Checks if backref from comment to task works."""
        # create sample data
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=user, task_status='pending', endpoint=endpoint)
        task.save()
        creator = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                       username="username", language="de_DE")
        creator.save()
        comment = Comment(text='abcdefg', task=task, creator=creator)
        comment.save()
        query = Comment.query.first()
        # test relationship
        self.assertEqual(query.task, task)

    def test_creator_relationship(self):
        """Checks if backref from comment to user works."""
        # create sample data
        user = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                    username='test_username', language='de_DE')
        user.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=user, task_status='pending', endpoint=endpoint)
        task.save()
        creator = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                       username="username", language="de_DE")
        creator.save()
        comment = Comment(text='abcdefg', task=task, creator=creator)
        comment.save()
        query = Comment.query.first()
        # test relationship
        self.assertEqual(query.creator, creator)
