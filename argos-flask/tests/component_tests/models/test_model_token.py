"""Tests the token model."""
import datetime
import unittest

from app import create_app, db
from app.models.models import Token, User, Admin


class TokenModelTestCase(unittest.TestCase):
    """This class represents the token model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Check if creating a new token works."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        token = Token(jti='abcd', token_type='access', user=user,
                      revoked_status=False, expiration_date=datetime.datetime.utcnow())
        token.save()
        # check for success
        self.assertTrue(token.token_id > 0)

    def test_user_relationship(self):
        """Check if backref from token to user works."""
        # create sample data
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        token = Token(jti='abcd', token_type='access', user=user, revoked_status=False,
                      expiration_date=datetime.datetime.utcnow())
        token.save()
        query = Token.query.first()
        # check for success
        self.assertEqual(user, query.user)

    def test_admin_relationship(self):
        """Check if backref from token to admin works."""
        # create sample data
        admin = Admin(name='admin', email_address='a@b.com',
                      username='admin_username', password='abc')
        admin.save()
        token = Token(jti='abcd', token_type='access', admin=admin, revoked_status=False,
                      expiration_date=datetime.datetime.utcnow())
        token.save()
        query = Token.query.first()
        # check for success
        self.assertEqual(admin, query.admin)
