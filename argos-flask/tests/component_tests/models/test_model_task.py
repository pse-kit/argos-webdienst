"""Tests the task model."""
import datetime
import unittest

from app import create_app, db
from app.models.models import Access, Task, Endpoint, User, Group, Comment, Field, Task_Field


class TaskModelTestCase(unittest.TestCase):
    """This class represents the task model test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_create(self):
        """Checks if creating a new task and relationship to endpoint works."""
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        self.assertTrue(task.task_id > 0)
        query = Task.query.first()
        self.assertEqual(task, query)
        self.assertEqual(query.endpoint, endpoint)
        self.assertEqual(query.creator, creator)

    def test_accesses_relationship(self):
        """Check whether accesses relationship works."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        user = User(name="user", email_address='mail@mail.com', email_notification_status=False,
                    username="username", language="de_DE")
        user.save()
        accessUser = Access(access_status="pending", task=task, user=user)
        accessUser.save()
        group = Group(name='group')
        group.save()
        accessGroup = Access(access_status='signed', task=task, group=group)
        accessGroup.save()
        query = Task.query.first()
        # test relationship
        self.assertEqual(accessUser, query.accesses[0])
        self.assertEqual(accessGroup, query.accesses[1])

    def test_comments_relationship(self):
        """Check whether comments relationship works."""
        # create sample data
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        commentator1 = User(name="user1", email_address='b@b.com', email_notification_status=False,
                            username="username1", language="de_DE")
        commentator1.save()
        comment1 = Comment(text='abcdefgh', task=task, creator=commentator1)
        comment1.save()
        commentator2 = User(name="user2", email_address='ab@c.com', email_notification_status=False,
                            username="username2", language="de_DE")
        commentator2.save()
        comment2 = Comment(text='abcdefg', task=task, creator=commentator2)
        comment2.save()
        query = Task.query.first()
        # test relationship
        self.assertEqual(query.comments[0], comment1)
        self.assertEqual(query.comments[1], comment2)

    def test_creator_relationship(self):
        """Check whether user relationship works."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        query = Task.query.first()
        # test relationship
        self.assertEqual(query.creator, creator)

    def test_tasks_fields_relationship(self):
        """Check whether tasks_fields works."""
        # create sample data
        endpoint = Endpoint(name='endpoint_name', signature_type='checkbox', sequential_status=True,
                            instantiable_status=True, deletable_status=True,
                            email_notification_status=True, allow_additional_accesses_status=True)
        endpoint.save()
        field1 = Field(name='field1_name', field_type='textfield', endpoint=endpoint)
        field2 = Field(name='field2_name', field_type='datetime', endpoint=endpoint)
        field1.save()
        field2.save()
        creator = User(name='user_name', email_address='a@b.com', email_notification_status=False,
                       username='test_username', language='de_DE')
        creator.save()
        task = Task(name='test', creator=creator, task_status='pending', endpoint=endpoint)
        task.save()
        task_field1 = Task_Field(field=field1, task=task, value="abcd")
        task_field1.save()
        timestamp = datetime.datetime.utcnow()
        task_field2 = Task_Field(field=field2, task=task, value=timestamp)
        task_field2.save()
        query = Task.query.first()
        # test relationship
        self.assertEqual(query.tasks_fields[0], task_field1)
        self.assertEqual(query.tasks_fields[1], task_field2)
