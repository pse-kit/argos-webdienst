"""Tests the synchronizer."""

import unittest

import ldap3
from ldap_test import LdapServer

from app import create_app, db, synchronizer


class SynchronizerInvalidLDAPTestCase(unittest.TestCase):
    """This class represents the synchronzier test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = LdapServer({
            'bind_dn': 'cn=admin,dc=argos,dc=com',
            'password': 'password',
            'base': {
                'objectclass': ['organization'],
                'dn': 'dc=argos,dc=com',
            },
            'entries': [{
                'objectclass': ['organizationalUnit'],
                'dn': 'ou=users,dc=argos,dc=com'
            }, {
                'objectclass': ['organizationalUnit'],
                'dn': 'ou=groups,dc=argos,dc=com'
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=maxmustermann,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'maxmustermann',
                               'cn': 'Max Mustermann',
                               'sn': 'Mustermann'}
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=jclears0,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'jclears0',
                               'mail': 'jclears0@argos.com',
                               'cn': 'Julee Clears',
                               'sn': 'Clears'}
            }, {
                'objectclass': ['groupOfUniqueNames'],
                'dn': 'cn=Vorstand,ou=groups,dc=argos,dc=com',
                'attributes': {'description': 'Beschreibung Vorstand',
                               'uniqueMember': ['uid=jclears0,ou=users,dc=argos,dc=com']}
            }
            ],
        })
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def test_synchronizer_invalid_sever_credentials(self):
        """Check if invalid credentials exception is thrown."""
        # Define test variables and initialize app
        custom_config = {
            'LDAP_HOST': 'localhost',
            'LDAP_PORT': int(self.server.config['port']),
            'LDAP_BASE_DN': self.server.config['base']['dn'],
            'LDAP_USE_SSL': False,
            'LDAP_USER_DN': 'ou=users',
            'LDAP_USER_RDN_ATTR': 'uid',
            'LDAP_USER_OBJECT_FILTER': '(objectclass=inetOrgPerson)',
            'LDAP_USER_LOGIN_ATTR': 'uid',
            'LDAP_GROUP_DN': 'ou = groups',
            'LDAP_GROUP_OBJECT_FILTER': '(objectclass=groupofuniquenames)',
            'LDAP_GROUP_MEMBERS_ATTR': 'uniqueMember',
            'LDAP_BIND_USER_DN': self.server.config['bind_dn'],
            'LDAP_BIND_USER_PASSWORD': '12345',
            'SYNC_USER_USERNAME_ATTR': 'uid',
            'SYNC_USER_NAME_ATTR': 'cn',
            'SYNC_GROUP_NAME_ATTR': 'cn'
        }
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        with self.assertLogs('app.synchronizer.ldap_sync', level='DEBUG') as cm:
            with self.assertRaises(ldap3.core.exceptions.LDAPInvalidCredentialsResult):
                synchronizer.synchronize_ldap_to_db()

        # check if errors are logged
        self.assertTrue(s for s in cm.output if 'Authentication was not successful' in s)

        # clean up
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_synchronizer_invalid_ldap_attributes(self):
        """Check if invalid credentials exception is thrown."""
        # Define test variables and initialize app
        custom_config = {
            'LDAP_HOST': 'localhost',
            'LDAP_PORT': int(self.server.config['port']),
            'LDAP_BASE_DN': self.server.config['base']['dn'],
            'LDAP_USE_SSL': False,
            'LDAP_USER_DN': 'ou=users',
            'LDAP_USER_RDN_ATTR': 'uid',
            'LDAP_USER_OBJECT_FILTER': '(objectclass=inetOrgPerson)',
            'LDAP_USER_LOGIN_ATTR': 'uid',
            'LDAP_GROUP_DN': 'ou = groups',
            'LDAP_GROUP_OBJECT_FILTER': '(objectclass=groupofuniquenames)',
            'LDAP_GROUP_MEMBERS_ATTR': 'uniqueMember',
            'LDAP_BIND_USER_DN': self.server.config['bind_dn'],
            'LDAP_BIND_USER_PASSWORD': self.server.config['password'],
            'SYNC_USER_USERNAME_ATTR': 'uid',
            'SYNC_USER_NAME_ATTR': 'cn',
            'SYNC_GROUP_NAME_ATTR': 'info'
        }
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

        with self.assertLogs('app.synchronizer.ldap_sync', level='DEBUG') as cm:
            synchronizer.synchronize_ldap_to_db()
        # check if errors are logged
        self.assertTrue(s for s in cm.output if 'Error creating group' in s)
        self.assertTrue(s for s in cm.output if 'Error creating user' in s)

        # clean up
        db.session.remove()
        db.drop_all()
        self.app_context.pop()
