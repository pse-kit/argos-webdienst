"""Tests the synchronizer."""

import unittest

from ldap_test import LdapServer

from app import create_app, db, synchronizer
from app.models.models import Group, User


class SynchronizerTestCase(unittest.TestCase):
    """This class represents the synchronzier test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = LdapServer({
            'bind_dn': 'cn=admin,dc=argos,dc=com',
            'password': 'password',
            'base': {
                'objectclass': ['organization'],
                'dn': 'dc=argos,dc=com',
            },
            'entries': [{
                'objectclass': ['organizationalUnit'],
                'dn': 'ou=users,dc=argos,dc=com'
            }, {
                'objectclass': ['organizationalUnit'],
                'dn': 'ou=groups,dc=argos,dc=com'
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=maxmustermann,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'maxmustermann',
                               'mail': 'maxmustermann@argos.com',
                               'cn': 'Max Mustermann',
                               'sn': 'Mustermann'}
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=jclears0,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'jclears0',
                               'mail': 'jclears0@argos.com',
                               'cn': 'Julee Clears',
                               'sn': 'Clears'}
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=jmardlin1,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'jmardlin1',
                               'mail': 'jmardlin1@argos.com',
                               'cn': 'Joanna Mardlin',
                               'sn': 'Mardlin'}
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=bsnare3,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'bsnare3',
                               'mail': 'bsnare3@argos.com',
                               'cn': 'Brigitta Snare',
                               'sn': 'Snare'}
            }, {
                'objectclass': ['inetOrgPerson'],
                'dn': 'uid=kaggissh,ou=users,dc=argos,dc=com',
                'attributes': {'uid': 'kaggissh',
                               'mail': 'kaggissh@argos.com',
                               'cn': 'Kendell Aggiss',
                               'sn': 'Aggiss'}
            }, {
                'objectclass': ['groupOfUniqueNames'],
                'dn': 'cn=Vorstand,ou=groups,dc=argos,dc=com',
                'attributes': {'description': 'Beschreibung Vorstand',
                               'uniqueMember': ['uid=maxmustermann,ou=users,dc=argos,dc=com',
                                                'uid=kaggissh,ou=users,dc=argos,dc=com']}
            }, {
                'objectclass': ['groupOfUniqueNames'],
                'dn': 'cn=IT-Abteilung,ou=groups,dc=argos,dc=com',
                'attributes': {'description': 'Die IT-Abteilung',
                               'uniqueMember': ['uid=bsnare3,ou=users,dc=argos,dc=com',
                                                'uid=kaggissh,ou=users,dc=argos,dc=com']}
            }, {
                'objectclass': ['groupOfUniqueNames'],
                'dn': 'cn=Marketing,ou=groups,dc=argos,dc=com',
                'attributes': {'uniqueMember': ['uid=jclears0,ou=users,dc=argos,dc=com',
                                                'uid=kaggissh,ou=users,dc=argos,dc=com',
                                                'uid=jmardlin1,ou=users,dc=argos,dc=com',
                                                'uid=bsnare3,ou=users,dc=argos,dc=com']}
            }

            ],
        })
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = {
            'LDAP_HOST': 'localhost',
            'LDAP_PORT': int(self.server.config['port']),
            'LDAP_BASE_DN': self.server.config['base']['dn'],
            'LDAP_USE_SSL': False,
            'LDAP_USER_DN': 'ou=users',
            'LDAP_USER_RDN_ATTR': 'uid',
            'LDAP_USER_OBJECT_FILTER': '(objectclass=inetOrgPerson)',
            'LDAP_USER_LOGIN_ATTR': 'uid',
            'LDAP_GROUP_DN': 'ou = groups',
            'LDAP_GROUP_OBJECT_FILTER': '(objectclass=groupofuniquenames)',
            'LDAP_GROUP_MEMBERS_ATTR': 'uniqueMember',
            'LDAP_BIND_USER_DN': self.server.config['bind_dn'],
            'LDAP_BIND_USER_PASSWORD': self.server.config['password'],
            'SYNC_USER_USERNAME_ATTR': 'uid',
            'SYNC_USER_NAME_ATTR': 'cn',
            'SYNC_GROUP_NAME_ATTR': 'cn'
        }
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_synchronizer_user_creation(self):
        """Check if syncing LDAP users to db works."""
        synchronizer.synchronize_ldap_to_db()

        jclears0 = User.query.filter_by(username='jclears0').first()
        self.assertEqual(jclears0.username, 'jclears0')
        self.assertEqual(jclears0.name, 'Julee Clears')
        self.assertEqual(jclears0.email_address, 'jclears0@argos.com')

        maxmustermann = User.query.filter_by(username='maxmustermann').first()
        self.assertEqual(maxmustermann.username, 'maxmustermann')
        self.assertEqual(maxmustermann.name, 'Max Mustermann')
        self.assertEqual(maxmustermann.email_address, 'maxmustermann@argos.com')

        jmardlin1 = User.query.filter_by(username='jmardlin1').first()
        self.assertEqual(jmardlin1.username, 'jmardlin1')
        self.assertEqual(jmardlin1.name, 'Joanna Mardlin')
        self.assertEqual(jmardlin1.email_address, 'jmardlin1@argos.com')

        bsnare3 = User.query.filter_by(username='bsnare3').first()
        self.assertEqual(bsnare3.username, 'bsnare3')
        self.assertEqual(bsnare3.name, 'Brigitta Snare')
        self.assertEqual(bsnare3.email_address, 'bsnare3@argos.com')

        kaggissh = User.query.filter_by(username='kaggissh').first()
        self.assertEqual(kaggissh.username, 'kaggissh')
        self.assertEqual(kaggissh.name, 'Kendell Aggiss')
        self.assertEqual(kaggissh.email_address, 'kaggissh@argos.com')

        # user count should not change
        synchronizer.synchronize_ldap_to_db()
        self.assertEqual(User.query.count(), 5)

    def test_synchronizer_group_creation(self):
        """Check if syncing LDAP groups to db works."""
        synchronizer.synchronize_ldap_to_db()

        vorstand = Group.query.filter_by(name='Vorstand').first()
        self.assertEqual(vorstand.name, 'Vorstand')
        self.assertEqual(vorstand.description, 'Beschreibung Vorstand')

        it_abteilung = Group.query.filter_by(name='IT-Abteilung').first()
        self.assertEqual(it_abteilung.name, 'IT-Abteilung')
        self.assertEqual(it_abteilung.description, 'Die IT-Abteilung')

        marketing = Group.query.filter_by(name='Marketing').first()
        self.assertEqual(marketing.name, 'Marketing')
        self.assertTrue(marketing.description is None)

        self.assertEqual(Group.query.count(), 3)

    def test_synchronizer_memberships(self):
        """Check if syncing LDAP memberships to db works."""
        synchronizer.synchronize_ldap_to_db()

        jclears0 = User.query.filter_by(username='jclears0').first()
        maxmustermann = User.query.filter_by(username='maxmustermann').first()
        jmardlin1 = User.query.filter_by(username='jmardlin1').first()
        bsnare3 = User.query.filter_by(username='bsnare3').first()
        kaggissh = User.query.filter_by(username='kaggissh').first()

        vorstand = Group.query.filter_by(name='Vorstand').first()
        it_abteilung = Group.query.filter_by(name='IT-Abteilung').first()
        marketing = Group.query.filter_by(name='Marketing').first()
        vorstand_users = vorstand.users.all()
        it_abteilung_users = it_abteilung.users.all()
        marketing_users = marketing.users.all()

        self.assertTrue(maxmustermann in vorstand_users)
        self.assertTrue(kaggissh in vorstand_users)
        self.assertEqual(len(vorstand_users), 2)

        self.assertTrue(bsnare3 in it_abteilung_users)
        self.assertTrue(kaggissh in it_abteilung_users)
        self.assertEqual(len(it_abteilung_users), 2)

        self.assertTrue(jclears0 in marketing_users)
        self.assertTrue(jmardlin1 in marketing_users)
        self.assertTrue(bsnare3 in marketing_users)
        self.assertTrue(kaggissh in marketing_users)
        self.assertEqual(len(marketing_users), 4)
