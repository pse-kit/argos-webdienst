"""Tests the CLI."""

import unittest
from datetime import datetime, timedelta

from app import create_app, db
from app.models.models import Admin, Token


class CLITestCase(unittest.TestCase):
    """This class represents the CLI test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        try:
            self.app = create_app('testing')
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.runner = self.app.test_cli_runner()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_add_admin_missing_argument(self):
        """Check for error if argument is missing."""
        result = self.runner.invoke(args=['add_admin'])
        self.assertTrue('Error: Missing argument' in result.output)
        result = self.runner.invoke(args=['add_admin', 'Test0', 'a@b.com', 'admin'])
        self.assertTrue('Error: Missing argument' in result.output)

    def test_add_admin_invalid_name(self):
        """Check for error if name is invalid."""
        result = self.runner.invoke(
            args=['add_admin', 'Test0', 'Test', 'a@b.com', 'admin', 'admin'])
        self.assertEqual(result.output, 'Error: Invalid name!\n')
        result = self.runner.invoke(
            args=['add_admin', '8(S()/(§(=)$', ')§$%§(', 'a@b.com', 'admin', 'admin'])
        self.assertEqual(result.output, 'Error: Invalid name!\n')

    def test_add_admin_invalid_email(self):
        """Check for error if email adress is invalid."""
        result = self.runner.invoke(
            args=['add_admin', 'Armin', 'Admin', 'a@bcom', 'admin', 'admin'])
        self.assertEqual(result.output, 'Error: Invalid Email Address!\n')
        result = self.runner.invoke(
            args=['add_admin', 'Armin', 'Admin', 'ab.com', 'admin', 'admin'])
        self.assertEqual(result.output, 'Error: Invalid Email Address!\n')

    def test_add_admin_valid(self):
        """Check if admin creation works."""
        first_name = 'Max'
        last_name = 'Mustermann'
        email_address = 'maxmustermann@argos.com'
        username = 'max'
        password = 'test'
        result = self.runner.invoke(
            args=['add_admin', first_name, last_name, email_address, username, password])
        self.assertEqual(result.output, 'Admin creation successful\n')
        admin = Admin.query.filter_by(username='max').first()
        self.assertIsNotNone(admin)
        self.assertEqual(admin.name, f"{first_name} {last_name}")
        self.assertEqual(admin.email_address, email_address)
        self.assertEqual(admin.username, username)
        self.assertTrue(admin.verify_password(password))

    def test_add_admin_username_taken(self):
        """Check for error if username is already taken."""
        result = self.runner.invoke(
            args=['add_admin', 'Armin', 'Admin', 'a@b.com', 'admin', 'admin'])
        self.assertEqual(result.output, 'Admin creation successful\n')
        result = self.runner.invoke(
            args=['add_admin', 'Max', 'Mustermann', 'ab@b.com', 'admin', 'admin'])
        self.assertEqual(result.output, 'Error: Username is already taken!\n')

    def test_clear_expired_tokens(self):
        """Check if clear_expired_tokens command works as expected."""
        # create some expired tokens
        token1 = Token(jti='BJFCGhdoUN', token_type='access',
                       revoked_status=True,
                       expiration_date=datetime.now() - timedelta(days=2))
        token1.save()
        token2 = Token(jti='6KgQrHRZ8d', token_type='refresh',
                       revoked_status=False,
                       expiration_date=datetime.now() - timedelta(minutes=1))
        token2.save()

        # create some fresh tokens
        token3 = Token(jti='TeU6mSoY89', token_type='access',
                       revoked_status=False,
                       expiration_date=datetime.now() + timedelta(days=1))
        token3.save()
        token4 = Token(jti='FSGmzMvgZX', token_type='refresh',
                       revoked_status=False,
                       expiration_date=datetime.now() + timedelta(minutes=10))
        token4.save()
        self.assertEqual(Token.query.count(), 4)

        result = self.runner.invoke(args=['clear_expired_tokens'])
        self.assertEqual(result.output, 'Deletion of expired tokens successful\n')
        self.assertEqual(Token.query.count(), 2)
        self.assertIsNotNone(Token.query.filter_by(jti='FSGmzMvgZX').first())
        self.assertIsNotNone(Token.query.filter_by(jti='TeU6mSoY89').first())
