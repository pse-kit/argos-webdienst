"""Contains frequently needed functionality."""

from flask_jwt_extended import create_access_token, create_refresh_token

from app.api.v1.authenticator.helpers import (Requestor, add_admin_token_to_database,
                                              add_user_token_to_database)


def login_user(client, username, password):
    """Login request with provided user login credentials.

    Args:
        client (flask.testing.FlaskClient): Test client.
        username (str): Username.
        password (str): Password.

    Returns:
        Response: Response object

    """
    return client.post('/api/v1/login', json={
        'username': username, 'password': password})


def login_admin(client, username, password):
    """Login request with provided admin login credentials.

    Args:
        client (flask.testing.FlaskClient): Test client.
        username (str): Username.
        password (str): Password.

    Returns:
        Response: Response object

    """
    return client.post('/api/v1/login?role=admin', json={
        'username': username, 'password': password})


def refresh(client, refresh_token):
    """Refresh request with provided refresh token.

    Args:
        client (flask.testing.FlaskClient): Test client.
        refresh_token (str): Refresh Token.

    Returns:
        Response: Response object

    """
    return client.post('/api/v1/refresh', headers=auth_header(refresh_token))


def logout_access(client, access_token):
    """Logout request. Invalidates provided access token.

    Args:
        client (flask.testing.FlaskClient): Test client.
        access_token (str): Access token.

    Returns:
        Response: Response object

    """
    return client.delete('/api/v1/logout/access', headers=auth_header(access_token))


def logout_refresh(client, refresh_token):
    """Logout request. Invalidates provided refresh token.

    Args:
        client (flask.testing.FlaskClient): Test client.
        refresh_token (str): Refresh Token.

    Returns:
        Response: Response object

    """
    return client.delete('/api/v1/logout/refresh', headers=auth_header(refresh_token))


def auth_header(token):
    """Create auth header with provided token.

    Args:
        token (str): JWT.

    Returns:
        dict: Authorization header.

    """
    return {'Authorization': 'Bearer ' + token}


def create_access_token_user(user):
    """Create new refresh and access_token for a user."""
    # create JWTs and add them to db
    requestor = Requestor(user.user_id, 'user')
    access_token = create_access_token(identity=requestor)
    refresh_token = create_refresh_token(identity=requestor)
    add_user_token_to_database(access_token)
    add_user_token_to_database(refresh_token)
    return access_token


def create_access_token_admin(admin):
    """Create new refresh and access_token for a admin."""
    # create JWTs and add them to db
    requestor = Requestor(admin.admin_id, 'admin')
    access_token = create_access_token(identity=requestor)
    refresh_token = create_refresh_token(identity=requestor)
    add_admin_token_to_database(access_token)
    add_admin_token_to_database(refresh_token)
    return access_token
