"""Tests the user handler."""
import unittest

from app import create_app, db, synchronizer
from tests.helper import auth_header
from tests.integration_tests.helper import (get_custom_config, login_get_access_token_user,
                                            setup_ldap, ROUTES)


class UserHandlerTestCase(unittest.TestCase):
    """This class represents the user_handler test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = setup_ldap()
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = get_custom_config(self.server)
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()
        synchronizer.synchronize_ldap_to_db()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_user_search(self):
        """Check if searching for users by name, username and user_id works."""
        access_token = login_get_access_token_user(self.client, 'maxmustermann')

        # single user by name
        response = self.client.get(ROUTES['search_user'] + '?search=mus',
                                   headers=auth_header(access_token))
        expected = {
            'users': [
                {
                    'email_address': 'maxmustermann@argos.com',
                    'name': 'Max Mustermann',
                    'user_id': 5
                }
            ]
        }
        self.assertEqual(response.get_json(), expected)

        # single user by id
        response = self.client.get(ROUTES['search_user'] + '?search=5',
                                   headers=auth_header(access_token))
        expected = {
            'users': [
                {
                    'email_address': 'maxmustermann@argos.com',
                    'name': 'Max Mustermann',
                    'user_id': 5
                }
            ]
        }
        self.assertEqual(response.get_json(), expected)

        response = self.client.get(ROUTES['search_user'] + '?search=ar',
                                   headers=auth_header(access_token))
        expected = {
            'users': [
                {
                    'email_address': 'bsnare3@argos.com',
                    'name': 'Brigitta Snare',
                    'user_id': 1
                },
                {
                    'email_address': 'jclears0@argos.com',
                    'name': 'Julee Clears',
                    'user_id': 2
                },
                {
                    'email_address': 'jmardlin1@argos.com',
                    'name': 'Joanna Mardlin',
                    'user_id': 3
                }
            ]
        }
        self.assertDictEqual(response.get_json(), expected)

        # no user found
        response = self.client.get(ROUTES['search_user'] + '?search=xyz',
                                   headers=auth_header(access_token))
        expected = {
            'users': []
        }
        self.assertDictEqual(response.get_json(), expected)

        # empty search term given
        response = self.client.get(ROUTES['search_user'] + '?search=',
                                   headers=auth_header(access_token))
        expected = {
            'users': []
        }
        self.assertDictEqual(response.get_json(), expected)

        # no search term given
        response = self.client.get(ROUTES['search_user'],
                                   headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

    def test_user_current(self):
        """Check if getting user info works."""
        access_token = login_get_access_token_user(self.client, 'bsnare3')

        response = self.client.get(ROUTES['current_user'],
                                   headers=auth_header(access_token))
        expected = {
            'email_address': 'bsnare3@argos.com',
            'name': 'Brigitta Snare',
            'user_id': 1
        }
        self.assertDictEqual(response.get_json(), expected)
