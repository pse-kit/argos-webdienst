"""Tests the endpoint handler."""
import unittest

from app import create_app, db, synchronizer
from tests.helper import auth_header
from tests.integration_tests.helper import (get_custom_config, login_get_access_token_admin,
                                            login_get_access_token_user, create_admin, setup_ldap,
                                            ROUTES)


class EndpointHandlerTestCase(unittest.TestCase):
    """This class represents the endpoint_handler test case."""

    # create minimalistic endpoint data
    endpoint_data = {
        'name': 'endpoint',
        'description': 'description',
        'sequential_status': True,
        'deletable_status': True,
        'email_notification_status': True,
        'signature_type': 'checkbox',
        'allow_additional_accesses_status': True
    }

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = setup_ldap()
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = get_custom_config(self.server)
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()
        synchronizer.synchronize_ldap_to_db()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create_endpoint(self):
        """Check if creating an endpoint works."""
        create_admin(name='admin', username='admin')
        access_token = login_get_access_token_admin(self.client, 'admin')

        # create endpoint
        data = {
            "name": "Endpoint",
            "email_notification_status": True,
            "deletable_status": False,
            "sequential_status": True,
            "signature_type": "buttonAfterDownload",
            "allow_additional_accesses_status": False,
            "additional_access_insert_before_status": True,
            "given_accesses": [
                {
                    "group_id": 3,
                    "access_order": 0
                },
                {
                    "user_id": 1,
                    "group_id": None,
                    "access_order": 1
                }
            ],
            "fields": [
                {
                    "field_type": "checkbox",
                    "name": "name",
                    "required_status": False
                },
                {
                    "field_type": "textfield",
                    "name": "name"
                },
                {
                    "field_type": "file",
                    "name": "field_file",
                    "required_status": True
                }
            ],
            "description": "description"
        }
        response = self.client.post(ROUTES['endpoints'], json=data,
                                    headers=auth_header(access_token))
        expected = {
            "endpoint_id": 1,
            "name": "Endpoint",
            "instantiable_status": True,
            "email_notification_status": True,
            "deletable_status": False,
            "sequential_status": True,
            "signature_type": "buttonAfterDownload",
            "allow_additional_accesses_status": False,
            "additional_access_insert_before_status": True,
            "given_accesses": [
                {
                    "group": {
                        "group_id": 3,
                        "name": "Vorstand",
                        "description": "Beschreibung Vorstand"
                    },
                    "user": None,
                    "access_order": 0
                },
                {
                    "group": None,
                    "user": {
                        "user_id": 1,
                        "name": "Brigitta Snare",
                        "email_address": "bsnare3@argos.com"
                    },
                    "access_order": 1
                }
            ],
            "fields": [
                {
                    "field_id": 1,
                    "field_type": "checkbox",
                    "name": "name",
                    "required_status": False
                },
                {
                    "field_id": 2,
                    "field_type": "textfield",
                    "name": "name",
                    "required_status": True
                },
                {
                    "field_id": 3,
                    "field_type": "file",
                    "name": "field_file",
                    "required_status": True
                }
            ],
            "active_tasks": 0,
            "description": "description"
        }
        self.assertDictEqual(response.get_json(), expected)

        # given link in header works
        url = response.headers['Location']
        response = self.client.get(url, headers=auth_header(access_token))
        self.assertDictEqual(response.get_json(), expected)

        # bad data: no additional accesses allowed but none given
        data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': False
        }
        response = self.client.post(ROUTES['endpoints'], json=data,
                                    headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

        # bad data: invalid access order
        data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': False,
            "given_accesses": [
                {
                    "group_id": 3,
                    "access_order": 0
                },
                {
                    "user_id": 1,
                    "group_id": None,
                    "access_order": 2
                }
            ]
        }
        response = self.client.post(ROUTES['endpoints'], json=data,
                                    headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

        # bad data: both user_id and group_id set
        data = {
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': False,
            "given_accesses": [
                {
                    "group_id": 3,
                    "access_order": 0
                },
                {
                    "user_id": 1,
                    "group_id": 2,
                    "access_order": 2
                }
            ]
        }
        response = self.client.post(ROUTES['endpoints'], json=data,
                                    headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

        # BadRequest error if JSON is missing
        response = self.client.post(ROUTES['endpoints'],
                                    headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

    def test_get_endpoint_by_id(self):
        """Check if getting an endpoint by ID works."""
        create_admin(name='admin', username='admin')
        access_token_admin = login_get_access_token_admin(self.client, 'admin')
        access_token_user = login_get_access_token_user(self.client, 'bsnare3')

        self.client.post(ROUTES['endpoints'], json=self.endpoint_data,
                         headers=auth_header(access_token_admin))

        response = self.client.get(ROUTES['endpoints'] + "/1", json=self.endpoint_data,
                                   headers=auth_header(access_token_admin))

        expected = {
            'endpoint_id': 1,
            'instantiable_status': True,
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'fields': [],
            'given_accesses': [],
            'active_tasks': 0
        }
        self.assertDictEqual(response.get_json(), expected)

        response = self.client.get(ROUTES['endpoints'] + "/1", json=self.endpoint_data,
                                   headers=auth_header(access_token_user))

        expected = {
            'endpoint_id': 1,
            'instantiable_status': True,
            'name': 'endpoint',
            'description': 'description',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': True,
            'signature_type': 'checkbox',
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'given_accesses': [],
            'fields': [],
        }
        self.assertDictEqual(response.get_json(), expected)

        # error if ID does not exist
        response = self.client.get(ROUTES['endpoints'] + "/3", json=self.endpoint_data,
                                   headers=auth_header(access_token_admin))
        self.assertEqual(response.status_code, 404)

    def test_set_deprecated(self):
        """Check if setting endpoint to deprecated works."""
        create_admin(name='admin', username='admin')
        access_token_admin = login_get_access_token_admin(self.client, 'admin')
        access_token_user = login_get_access_token_user(self.client, 'bsnare3')

        self.client.post(ROUTES['endpoints'], json=self.endpoint_data,
                         headers=auth_header(access_token_admin))

        response = self.client.delete(ROUTES['endpoints'] + "/1", json=self.endpoint_data,
                                      headers=auth_header(access_token_admin))
        self.assertEqual(response.status_code, 200)

        # deleted endpoint cannot be deleted again
        response = self.client.delete(ROUTES['endpoints'] + "/1", json=self.endpoint_data,
                                      headers=auth_header(access_token_admin))
        self.assertEqual(response.status_code, 404)

        # endpoint is deleted and cannot be seen
        response = self.client.get(ROUTES['endpoints'] + "/1", json=self.endpoint_data,
                                   headers=auth_header(access_token_admin))
        self.assertEqual(response.status_code, 404)

        response = self.client.get(ROUTES['endpoints'] + "/1", json=self.endpoint_data,
                                   headers=auth_header(access_token_user))
        self.assertEqual(response.status_code, 404)

        # error if ID is invalid
        response = self.client.get(ROUTES['endpoints'] + "/3", json=self.endpoint_data,
                                   headers=auth_header(access_token_admin))
        self.assertEqual(response.status_code, 404)

    def test_get_endpoints_active(self):
        """Check if setting endpoint to deprecated works."""
        create_admin(name='admin', username='admin')
        access_token_admin = login_get_access_token_admin(self.client, 'admin')
        access_token_user = login_get_access_token_user(self.client, 'bsnare3')

        for i in range(42):
            self.client.post(ROUTES['endpoints'], json=self.endpoint_data,
                             headers=auth_header(access_token_admin))

        # request 10 endpoints
        limit = 10
        response = self.client.get(ROUTES['endpoints'] + "?page=1&limit=%d" % limit,
                                   json=self.endpoint_data, headers=auth_header(access_token_admin))
        json = response.get_json()
        self.assertEqual(len(json['endpoints']), 10)
        # sorted by endpoint id
        for i in range(limit):
            self.assertEqual(json['endpoints'][i]['endpoint_id'], i + 1)

        response = self.client.get(ROUTES['endpoints'] + "?page=1&limit=%d" % limit,
                                   json=self.endpoint_data, headers=auth_header(access_token_user))
        json = response.get_json()
        self.assertEqual(len(json['endpoints']), 10)
        # sorted by endpoint id
        for i in range(limit):
            self.assertEqual(json['endpoints'][i]['endpoint_id'], i + 1)

        # url of next and previous pages (previous is empty)
        self.assertEqual(json['next'], ROUTES['endpoints'] + "?page=2&limit=%d" % limit)
        self.assertEqual(json['previous'], None)

        # last page has two endpoints
        response = self.client.get(ROUTES['endpoints'] + "?page=5&limit=%d" % limit,
                                   json=self.endpoint_data, headers=auth_header(access_token_admin))
        json = response.get_json()
        self.assertEqual(len(json['endpoints']), 2)
        # next page is empty
        self.assertEqual(json['next'], None)

        # default values for page and limit if given values are invalid
        response = self.client.get(ROUTES['endpoints'] + '?page=-3&limit=dsfkh',
                                   headers=auth_header(access_token_admin))
        json = response.get_json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json['endpoints']), 20)  # default limit: 20
        self.assertEqual(json['endpoints'][0]['endpoint_id'], 1)  # default page: 1

        # empty list if page number too high
        response = self.client.get(ROUTES['endpoints'] + '?page=20&limit=20',
                                   headers=auth_header(access_token_admin))
        json = response.get_json()
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(json['endpoints'], [])
