"""Tests the task handler."""
import unittest

from app import create_app, db, synchronizer
from tests.helper import auth_header
from tests.integration_tests.helper import (get_custom_config, login_get_access_token_admin,
                                            login_get_access_token_user, create_admin, setup_ldap,
                                            ROUTES)


class TaskHandlerTestCase(unittest.TestCase):
    """This class represents the task_handler test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = setup_ldap()
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = get_custom_config(self.server)
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()
        synchronizer.synchronize_ldap_to_db()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create_task(self):
        """Check if creating a task works."""
        create_admin(name='admin', username='admin')
        access_token_admin = login_get_access_token_admin(self.client, 'admin')
        access_token_user = login_get_access_token_user(self.client, 'bsnare3')

        # create endpoint
        endpoint_data = {
            "name": "Endpoint",
            "email_notification_status": True,
            "deletable_status": False,
            "sequential_status": True,
            "signature_type": "button",
            "allow_additional_accesses_status": True,
            "additional_access_insert_before_status": True,
            "given_accesses": [
                {
                    "group_id": 3,
                    "access_order": 0
                },
                {
                    "user_id": 1,
                    "group_id": None,
                    "access_order": 1
                }
            ],
            "fields": [
                {
                    "field_type": "checkbox",
                    "name": "name",
                    "required_status": False
                },
                {
                    "field_type": "textfield",
                    "name": "name"
                }
            ],
            "description": "description"
        }
        response = self.client.post(ROUTES['endpoints'], json=endpoint_data,
                                    headers=auth_header(access_token_admin))

        task_data = {
            "endpoint_id": 1,
            "name": "Task",
            "accesses": [
                {
                    "group_id": 2,
                    "access_order": 0
                },
                {
                    "user_id": 4,
                    "access_order": 1
                }
            ],
            "tasks_fields": [
                {
                    "field_id": 2,
                    "value": "text"
                }
            ]
        }
        response = self.client.post(ROUTES['tasks'], json=task_data,
                                    headers=auth_header(access_token_user))
        expected = {
            'endpoint_id': 1,
            'name': 'Task',
            'task_id': 1,
            'task_status': 'pending',
            'accesses': [
                {
                    'access_order': 0,
                    'access_status': 'pending',
                    'group': {'description': None,
                              'group_id': 2,
                              'name': 'Marketing'},
                    'user': None,
                    'event_date': None
                },
                {
                    'access_order': 1,
                    'access_status': 'pending',
                    'group': None,
                    'user': {
                        'email_address': 'kaggissh@argos.com',
                        'name': 'Kendell Aggiss',
                        'user_id': 4
                    },
                    'event_date': None
                },
                {
                    'access_order': 2,
                    'access_status': 'pending',
                    'group': {'description': 'Beschreibung Vorstand',
                              'group_id': 3,
                              'name': 'Vorstand'},
                    'user': None,
                    'event_date': None
                },
                {
                    'access_order': 3,
                    'access_status': 'pending',
                    'group': None,
                    'user': {
                        'email_address': 'bsnare3@argos.com',
                        'name': 'Brigitta Snare',
                        'user_id': 1
                    },
                    'event_date': None
                }
            ],
            'creator':
                {
                    'email_address': 'bsnare3@argos.com',
                    'name': 'Brigitta Snare',
                    'user_id': 1
                },
            'tasks_fields': [
                {
                    'field':
                        {
                            'field_type': 'textfield',
                            'name': 'name'
                        },
                    'value': 'text'
                }
            ]
        }
        json = response.get_json()
        self.assertTrue('creation_date' in json)

        # delete creation date, correct time cannot be verified
        del json['creation_date']
        self.assertDictEqual(json, expected)

        # given link in header works
        url = response.headers['Location']
        response = self.client.get(url, headers=auth_header(access_token_user))
        self.assertEqual(response.get_json()['task_id'], 1)

        # creator got a message
        response = self.client.get(ROUTES['messages'], json=task_data,
                                   headers=auth_header(access_token_user))
        expected = {
            'message_id': 1,
            'read_status': False,
            'text': 'Ihr Auftrag "Task" wurde erstellt.'
        }
        # get first message
        json = response.get_json()['messages'][0]
        self.assertTrue('message_date' in json)

        # delete message date, correct time cannot be verified
        del json['message_date']
        self.assertDictEqual(json, expected)

        # users that have to sign next got a message (but not the creator)
        for username in ['jclears0', 'kaggissh', 'jmardlin1']:
            access_token_signee = login_get_access_token_user(self.client, username)
            response = self.client.get(ROUTES['messages'], json=task_data,
                                       headers=auth_header(access_token_signee))
            # get first message
            json = response.get_json()['messages'][0]
            self.assertEqual(json['text'], 'Brigitta Snare hat Sie mit der Überprüfung des ' +
                             'Auftrags "Task" betraut.')

        # BadRequest error if JSON is missing
        response = self.client.post(ROUTES['tasks'],
                                    headers=auth_header(access_token_user))
        self.assertEqual(response.status_code, 400)
