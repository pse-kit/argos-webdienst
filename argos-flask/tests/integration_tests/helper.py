"""This module contains helper methods for integration tests."""

from ldap_test import LdapServer

from app.models.models import Admin
from tests.helper import login_user, login_admin

ROUTES = {
    'search_user': '/api/v1/users/search',
    'current_user': '/api/v1/users/current',
    'search_group': '/api/v1/groups/search',
    'preferences_language': '/api/v1/preferences/language/',
    'preferences_notifications': '/api/v1/preferences/notifications/',
    'endpoints': '/api/v1/endpoints',
    'tasks': '/api/v1/tasks',
    'messages': '/api/v1/messages',
}

PASSWORD = 'test'


def get_custom_config(server):
    """Get custom configuration for in-memory LDAP server.

    Args:
        server (LdapServer): In-memory LDAP server.

    Returns:
        dict of str : obj : Dictionary containing custom configuration for the flask application.

    """
    return {
        'LDAP_HOST': 'localhost',
        'LDAP_PORT': int(server.config['port']),
        'LDAP_BASE_DN': server.config['base']['dn'],
        'LDAP_USE_SSL': False,
        'LDAP_USER_DN': 'ou=users',
        'LDAP_USER_RDN_ATTR': 'uid',
        'LDAP_USER_OBJECT_FILTER': '(objectclass=inetOrgPerson)',
        'LDAP_USER_LOGIN_ATTR': 'uid',
        'LDAP_GROUP_DN': 'ou = groups',
        'LDAP_GROUP_OBJECT_FILTER': '(objectclass=groupofuniquenames)',
        'LDAP_GROUP_MEMBERS_ATTR': 'uniqueMember',
        'LDAP_BIND_USER_DN': server.config['bind_dn'],
        'LDAP_BIND_USER_PASSWORD': server.config['password'],
        'SYNC_USER_USERNAME_ATTR': 'uid',
        'SYNC_USER_NAME_ATTR': 'cn',
        'SYNC_GROUP_NAME_ATTR': 'cn'
    }


LDAP_DATA = {
    'bind_dn': 'cn=admin,dc=argos,dc=com',
    'password': 'password',
    'base': {
        'objectclass': ['organization'],
        'dn': 'dc=argos,dc=com',
    },
    'entries': [{
        'objectclass': ['organizationalUnit'],
        'dn': 'ou=users,dc=argos,dc=com'
    }, {
        'objectclass': ['organizationalUnit'],
        'dn': 'ou=groups,dc=argos,dc=com'
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=maxmustermann,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'maxmustermann',
                       'mail': 'maxmustermann@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'Max Mustermann',
                       'sn': 'Mustermann'}
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=jclears0,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'jclears0',
                       'mail': 'jclears0@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'Julee Clears',
                       'sn': 'Clears'}
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=jmardlin1,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'jmardlin1',
                       'mail': 'jmardlin1@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'Joanna Mardlin',
                       'sn': 'Mardlin'}
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=bsnare3,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'bsnare3',
                       'mail': 'bsnare3@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'Brigitta Snare',
                       'sn': 'Snare'}
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=kaggissh,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'kaggissh',
                       'mail': 'kaggissh@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'Kendell Aggiss',
                       'sn': 'Aggiss'}
    }, {
        'objectclass': ['groupOfUniqueNames'],
        'dn': 'cn=Vorstand,ou=groups,dc=argos,dc=com',
        'attributes': {'description': 'Beschreibung Vorstand',
                       'uniqueMember': ['uid=maxmustermann,ou=users,dc=argos,dc=com',
                                        'uid=kaggissh,ou=users,dc=argos,dc=com']}
    }, {
        'objectclass': ['groupOfUniqueNames'],
        'dn': 'cn=IT-Abteilung,ou=groups,dc=argos,dc=com',
        'attributes': {'description': 'Die IT-Abteilung',
                       'uniqueMember': ['uid=bsnare3,ou=users,dc=argos,dc=com',
                                        'uid=kaggissh,ou=users,dc=argos,dc=com']}
    }, {
        'objectclass': ['groupOfUniqueNames'],
        'dn': 'cn=Marketing,ou=groups,dc=argos,dc=com',
        'attributes': {'uniqueMember': ['uid=jclears0,ou=users,dc=argos,dc=com',
                                        'uid=kaggissh,ou=users,dc=argos,dc=com',
                                        'uid=jmardlin1,ou=users,dc=argos,dc=com',
                                        'uid=bsnare3,ou=users,dc=argos,dc=com']}
    }

    ],
}


def setup_ldap(ldap_data=LDAP_DATA):
    """Run in-memory LDAP server."""
    server = LdapServer(ldap_data)
    return server


def login_get_access_token_user(client, username):
    """Login user and return access token.

    It is assumed that all users have the same password for testing reasons.
    Args:
        client (flask.testing.FlaskClient): Test client.
        username (str): Username of user to login.

    Returns:
        str: Access token of logged in user.

    """
    response = login_user(client, username, PASSWORD)
    return response.get_json()['access_token']


def create_admin(name, username):
    """Create a new admin.

    All created admins have the same password for testing reasons.
    Args:
        name (str): Name of new admin.
        username (str): Username of new admin.

    Returns:
        Admin: The created Admin.

    """
    admin = Admin(name=name, username=username, password=PASSWORD)
    admin.save()


def login_get_access_token_admin(client, admin_username):
    """Login admin and return access token.

    It is assumed that all admins have the same password for testing reasons.
    Args:
        client (flask.testing.FlaskClient): Test client.
        admin_username (str): Username of admin to login.

    Returns:
        str: Access token of logged in admin.

    """
    response = login_admin(client, admin_username, PASSWORD)
    return response.get_json()['access_token']
