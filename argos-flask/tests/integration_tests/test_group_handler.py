"""Tests the group handler."""
import unittest

from app import create_app, db, synchronizer
from tests.helper import auth_header
from tests.integration_tests.helper import (get_custom_config, login_get_access_token_user,
                                            setup_ldap, ROUTES)


class GroupHandlerTestCase(unittest.TestCase):
    """This class represents the group_handler test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = setup_ldap()
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = get_custom_config(self.server)
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()
        synchronizer.synchronize_ldap_to_db()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_group_search(self):
        """Check if searching for groups by name works."""
        access_token = login_get_access_token_user(self.client, 'maxmustermann')

        # single group
        response = self.client.get(ROUTES['search_group'] + '?search=Vor',
                                   headers=auth_header(access_token))
        expected = {
            'groups': [
                {
                    'name': 'Vorstand',
                    'description': 'Beschreibung Vorstand',
                    'group_id': 3
                }
            ]
        }
        self.assertEqual(response.get_json(), expected)

        # several groups
        response = self.client.get(ROUTES['search_group'] + '?search=ng',
                                   headers=auth_header(access_token))
        expected = {
            'groups': [
                {
                    'name': 'IT-Abteilung',
                    'description': 'Die IT-Abteilung',
                    'group_id': 1
                },
                {
                    'name': 'Marketing',
                    'description': None,
                    'group_id': 2
                }
            ]
        }
        self.assertEqual(response.get_json(), expected)

        # empty search term given
        response = self.client.get(ROUTES['search_group'] + '?search=',
                                   headers=auth_header(access_token))
        expected = {
            'groups': []
        }
        self.assertDictEqual(response.get_json(), expected)

        # no group found
        response = self.client.get(ROUTES['search_group'] + '?search=xyz',
                                   headers=auth_header(access_token))
        expected = {
            'groups': []
        }
        self.assertDictEqual(response.get_json(), expected)

        # no search term given
        response = self.client.get(ROUTES['search_group'],
                                   headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)
