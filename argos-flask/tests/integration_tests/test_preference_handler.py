"""Tests the preference handler."""
import unittest

from app import create_app, db, synchronizer
from tests.helper import auth_header
from tests.integration_tests.helper import (get_custom_config, login_get_access_token_user,
                                            setup_ldap, ROUTES)


class PreferenceHandlerTestCase(unittest.TestCase):
    """This class represents the preference_handler test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = setup_ldap()
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = get_custom_config(self.server)
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()
        synchronizer.synchronize_ldap_to_db()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_language(self):
        """Check if getting the language of current user works."""
        access_token = login_get_access_token_user(self.client, 'maxmustermann')

        # default language should be 'de'
        response = self.client.get(ROUTES['preferences_language'],
                                   headers=auth_header(access_token))
        expected = {'language': 'de'}
        self.assertEqual(response.get_json(), expected)
        self.assertEqual(response.status_code, 200)

    def test_set_language(self):
        """Check if setting the language of user works."""
        access_token = login_get_access_token_user(self.client, 'maxmustermann')

        # no json
        response = self.client.put(ROUTES['preferences_language'],
                                   headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

        # invalid json
        self.client.put(ROUTES['preferences_language'],
                        json={'abc': 'xyz'},
                        headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

        # change language to 'en'
        self.client.put(ROUTES['preferences_language'],
                        json={'language': 'en'},
                        headers=auth_header(access_token))
        response = self.client.get(ROUTES['preferences_language'],
                                   headers=auth_header(access_token))
        expected = {'language': 'en'}
        self.assertEqual(response.get_json(), expected)
        self.assertEqual(response.status_code, 200)

    def test_get_notifications_setting(self):
        """Check if getting the notifications setting of current user works."""
        access_token = login_get_access_token_user(self.client, 'maxmustermann')

        # default language should be 'true'
        response = self.client.get(ROUTES['preferences_notifications'],
                                   headers=auth_header(access_token))
        expected = {'notification_status': True}
        self.assertEqual(response.get_json(), expected)
        self.assertEqual(response.status_code, 200)

    def test_set_notifications_setting(self):
        """Check if changing the notifications setting of user works."""
        access_token = login_get_access_token_user(self.client, 'maxmustermann')

        # no json
        response = self.client.put(ROUTES['preferences_notifications'],
                                   headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

        # invalid json
        self.client.put(ROUTES['preferences_notifications'],
                        json={'abc': 'xyz'},
                        headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)
        self.client.put(ROUTES['preferences_notifications'],
                        json={'email_notification_status': 'xyz'},
                        headers=auth_header(access_token))
        self.assertEqual(response.status_code, 400)

        # turn notifications off
        self.client.put(ROUTES['preferences_notifications'],
                        json={'notification_status': False},
                        headers=auth_header(access_token))
        response = self.client.get(ROUTES['preferences_notifications'],
                                   headers=auth_header(access_token))
        expected = {'notification_status': False}
        self.assertEqual(response.get_json(), expected)
        self.assertEqual(response.status_code, 200)
