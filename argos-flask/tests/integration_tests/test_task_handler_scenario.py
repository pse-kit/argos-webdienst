"""Tests the task handler."""
import unittest

from app import create_app, db, synchronizer
from tests.helper import auth_header
from tests.integration_tests.helper import (get_custom_config, login_get_access_token_admin,
                                            login_get_access_token_user, create_admin, setup_ldap,
                                            ROUTES, PASSWORD)

LDAP_DATA = {
    'bind_dn': 'cn=admin,dc=argos,dc=com',
    'password': 'password',
    'base': {
        'objectclass': ['organization'],
        'dn': 'dc=argos,dc=com',
    },
    'entries': [{
        'objectclass': ['organizationalUnit'],
        'dn': 'ou=users,dc=argos,dc=com'
    }, {
        'objectclass': ['organizationalUnit'],
        'dn': 'ou=groups,dc=argos,dc=com'
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=username1,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'username1',
                       'mail': 'maxmustermann@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'user1',
                       'sn': 'user1'}
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=username2,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'username2',
                       'mail': 'jclears0@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'user2',
                       'sn': 'user2'}
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=username3,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'username3',
                       'mail': 'jmardlin1@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'user3',
                       'sn': 'user3'}
    }, {
        'objectclass': ['inetOrgPerson'],
        'dn': 'uid=username4,ou=users,dc=argos,dc=com',
        'attributes': {'uid': 'username4',
                       'mail': 'bsnare3@argos.com',
                       'userPassword': PASSWORD,
                       'cn': 'name4',
                       'sn': 'name4'}
    }, {
        'objectclass': ['groupOfUniqueNames'],
        'dn': 'cn=group,ou=groups,dc=argos,dc=com',
        'attributes': {'uniqueMember': ['uid=username1,ou=users,dc=argos,dc=com',
                                        'uid=username2,ou=users,dc=argos,dc=com',
                                        'uid=username3,ou=users,dc=argos,dc=com',
                                        'uid=username4,ou=users,dc=argos,dc=com']}
    }

    ]
}


class TaskHandlerScenarioTestCase(unittest.TestCase):
    """This class represents the task_handler test case."""

    @classmethod
    def setUpClass(cls):
        """Run in-memory LDAP server."""
        cls.server = setup_ldap(LDAP_DATA)
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        """Stop in-memory LDAP server."""
        cls.server.stop()

    def setUp(self):
        """Define test variables and initialize app."""
        custom_config = get_custom_config(self.server)
        try:
            self.app = create_app('testing', custom_config)
        except ValueError:
            # bug in LDAP3 Manager: https://github.com/nickw444/flask-ldap3-login/issues/40
            self.app = create_app('testing', custom_config)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()
        synchronizer.synchronize_ldap_to_db()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_task_completed_scenario(self):
        """Simulate cycle of task."""
        create_admin('Admin', 'admin')
        admin_token = login_get_access_token_admin(self.client, 'admin')
        user1_token = login_get_access_token_user(self.client, 'username1')
        user2_token = login_get_access_token_user(self.client, 'username2')
        user3_token = login_get_access_token_user(self.client, 'username3')
        user4_token = login_get_access_token_user(self.client, 'username4')

        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkbox',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'given_accesses': [
                {
                    'group_id': 1,
                    'access_order': 0
                },
                {
                    'user_id': 1,
                    'access_order': 1
                },
                {
                    'user_id': 2,
                    'access_order': 2
                }

            ],
            'fields': [
                {
                    'name': 'field_name',
                    'field_type': 'textfield',
                    'required_status': False
                },
                {
                    'name': 'field_2',
                    'field_type': 'checkbox',
                    'required_status': True
                }
            ]
        }
        response = self.client.post(ROUTES['endpoints'], json=endpoint_data,
                                    headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 201)
        endpoint_id = response.get_json()['endpoint_id']

        task_data = {
            'name': 'test_task',
            'endpoint_id': endpoint_id,
            'accesses': [
                {
                    'user_id': 2,
                    'access_order': 0
                },
                {
                    'user_id': 3,
                    'group_id': None,
                    'access_order': 1
                }
            ],
            'tasks_fields': [
                {
                    'field_id': 2,
                    'value': 'true'
                },
                {
                    'field_id': 1,
                    'value': None
                }
            ]
        }

        # user1 creates task successfully
        response = self.client.post(ROUTES['tasks'], json=task_data,
                                    headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 201)

        # admin sees task
        response = self.client.get(ROUTES['tasks'],
                                   headers=auth_header(admin_token))
        self.assertEqual(len(response.get_json()['tasks']), 1)

        # user1 receives message that the task has been created
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user1_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text, 'Ihr Auftrag "test_task" wurde erstellt.')

        # user1 owns task
        response = self.client.get(ROUTES['tasks'] + '?type=OWNED',
                                   headers=auth_header(user1_token))
        self.assertEqual(response.get_json()['tasks'][0]['name'],
                         'test_task')

        # invalid type
        response = self.client.get(ROUTES['tasks'] + '?type=TYPE',
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 400)

        # user3 cannot sign the task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user3_token))
        self.assertEqual(response.status_code, 403)

        # user2 receives message that he can sign the task
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user2_token))
        message = response.get_json()['messages'][0]
        text = 'user1 hat Sie mit der Überprüfung des Auftrags "test_task" betraut.'
        self.assertEqual(message['text'], text)

        # user2 reads message
        response = self.client.put(ROUTES['messages'] + '/%d/status' % message['message_id'],
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 200)

        # user2 message is deleted after reading
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user2_token))
        messages = response.get_json()['messages']
        self.assertEqual(messages, [])

        # user2 comments
        response = self.client.post(ROUTES['tasks'] + '/1/comments', json={'text': 'comment'},
                                    headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 201)

        # user1 can see that comment
        response = self.client.get(ROUTES['tasks'] + '/1/comments/1',
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 200)
        comment = response.get_json()
        self.assertEqual(comment['text'], 'comment')

        # user2 can see that comment
        response = self.client.get(ROUTES['tasks'] + '/1/comments/1',
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 200)
        comment = response.get_json()
        self.assertEqual(comment['text'], 'comment')

        # user1 receives a message that his task has been commented
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user1_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text, 'Ihr Auftrag "test_task" wurde von user2 kommentiert.')

        # Bad Request, if no event given or invalid event or task does not exist
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': None},
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 400)
        response = self.client.put(ROUTES['tasks'] + '/1/status',
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 400)
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'event'},
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 400)
        response = self.client.put(ROUTES['tasks'] + '/3/status', json={'event': 'signed'},
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 403)

        # user2 signs the task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 200)

        # user1 receives a message that his task has been signed
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user1_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text, 'Ihr Auftrag "test_task" wurde von user2 signiert.')

        # user2 sees signed task under archived
        response = self.client.get(ROUTES['tasks'] + '?type=ARCHIVED',
                                   headers=auth_header(user2_token))
        self.assertEqual(response.get_json()['tasks'][0]['name'],
                         'test_task')

        # user3 receives a message that he can sign a task
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user3_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text,
                         'user1 hat Sie mit der Überprüfung des Auftrags "test_task" betraut.')

        # user3 can see the comment
        response = self.client.get(ROUTES['tasks'] + '/1/comments/1',
                                   headers=auth_header(user3_token))
        self.assertEqual(response.status_code, 200)
        comment = response.get_json()
        self.assertEqual(comment['text'], 'comment')

        # user3 signs the task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user3_token))
        self.assertEqual(response.status_code, 200)

        # user4 can see the task
        response = self.client.get(ROUTES['tasks'] + '/1/',
                                   headers=auth_header(user4_token))
        self.assertEqual(response.status_code, 200)

        # user3 can sign the task
        response = self.client.get(ROUTES['tasks'] + '?type=TOSIGN',
                                   headers=auth_header(user3_token))
        self.assertEqual(response.get_json()['tasks'][0]['name'],
                         'test_task')

        # user4 can sign the task
        response = self.client.get(ROUTES['tasks'] + '?type=TOSIGN',
                                   headers=auth_header(user4_token))
        self.assertEqual(response.get_json()['tasks'][0]['name'],
                         'test_task')

        # user3 signs the task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user3_token))
        self.assertEqual(response.status_code, 200)

        # user4 can still see the task
        response = self.client.get(ROUTES['tasks'] + '/1/',
                                   headers=auth_header(user4_token))
        self.assertEqual(response.status_code, 200)

        # user4 can see the task under archived
        response = self.client.get(ROUTES['tasks'] + '?type=ARCHIVED',
                                   headers=auth_header(user4_token))
        self.assertEqual(response.get_json()['tasks'][0]['name'],
                         'test_task')

        # admin deletes endpoint
        response = self.client.delete(ROUTES['endpoints'] + '/1/',
                                      headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 200)

        # admin can still see endpoint
        response = self.client.get(ROUTES['endpoints'] + '/1/',
                                   headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 200)

        # admin cannot delete endpoint again
        response = self.client.delete(ROUTES['endpoints'] + '/1/',
                                      headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 400)

        # task to endpoint cannot be created
        response = self.client.post(ROUTES['tasks'], json=task_data,
                                    headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 400)

        # user3 only sees one task under archived
        response = self.client.get(ROUTES['tasks'] + '?type=ARCHIVED',
                                   headers=auth_header(user3_token))
        self.assertEqual(len(response.get_json()['tasks']), 1)

        # user1 signs the task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 200)

        # user2 signs the task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 200)

        # user1 receives message that task has been completed
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user1_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text, 'Ihr Auftrag "test_task" wurde abgeschlossen.')

        # user2 cannot comment
        response = self.client.post(ROUTES['tasks'] + '/1/comments', json={'text': 'comment'},
                                    headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 400)

        # user1 sees task under archived
        response = self.client.get(ROUTES['tasks'] + '?type=ARCHIVED',
                                   headers=auth_header(user1_token))
        self.assertEqual(len(response.get_json()['tasks']), 1)

        # user1 does not see task under owned
        response = self.client.get(ROUTES['tasks'] + '?type=OWNED',
                                   headers=auth_header(user3_token))
        self.assertEqual(len(response.get_json()['tasks']), 0)

        # user1 can delete task
        response = self.client.delete(ROUTES['tasks'] + '/1',
                                      headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 200)

        # user1 receives a message that his task has been deleted
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user1_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text, 'Ihr Auftrag "test_task" wurde gelöscht.')

        # user1 does not see task under archived
        response = self.client.get(ROUTES['tasks'] + '?type=ARCHIVED',
                                   headers=auth_header(user3_token))
        self.assertEqual(len(response.get_json()['tasks']), 0)

        # user1 cannot get task
        response = self.client.get(ROUTES['tasks'] + '/1',
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 403)

        # user2 cannot get task
        response = self.client.get(ROUTES['tasks'] + '/1',
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 403)

        # user1 cannot see comments of task
        response = self.client.get(ROUTES['tasks'] + '/1/comments',
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 403)

        # admin cannot see endpoint
        response = self.client.get(ROUTES['endpoints'] + '/1',
                                   headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 404)

        # admin cannot see task
        response = self.client.get(ROUTES['tasks'] + '/1',
                                   headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 404)

    def test_task_denied_withdrawn_scenario(self):
        """Simulate cycle of task."""
        create_admin('Admin', 'admin')
        admin_token = login_get_access_token_admin(self.client, 'admin')
        user1_token = login_get_access_token_user(self.client, 'username1')
        user2_token = login_get_access_token_user(self.client, 'username2')
        user3_token = login_get_access_token_user(self.client, 'username3')
        user4_token = login_get_access_token_user(self.client, 'username4')

        endpoint_data = {
            'name': 'test_endpoint',
            'signature_type': 'checkbox',
            'sequential_status': True,
            'deletable_status': True,
            'email_notification_status': False,
            'allow_additional_accesses_status': True,
            'additional_access_insert_before_status': True,
            'given_accesses': [
                {
                    'group_id': 1,
                    'access_order': 0
                },
                {
                    'user_id': 1,
                    'access_order': 1
                },
                {
                    'user_id': 2,
                    'access_order': 2
                }

            ]
        }
        response = self.client.post(ROUTES['endpoints'], json=endpoint_data,
                                    headers=auth_header(admin_token))
        self.assertEqual(response.status_code, 201)
        endpoint_id = response.get_json()['endpoint_id']

        task_data_to_deny = {
            'name': 'deny_task',
            'endpoint_id': endpoint_id
        }

        task_data_to_withdraw = {
            'name': 'withdraw_task',
            'endpoint_id': endpoint_id
        }

        # user1 creates task successfully
        response = self.client.post(ROUTES['tasks'], json=task_data_to_deny,
                                    headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 201)

        # user4 signs the task to deny
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user4_token))
        self.assertEqual(response.status_code, 200)

        # user3 cannot deny task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'denied'},
                                   headers=auth_header(user3_token))
        self.assertEqual(response.status_code, 400)

        # user1 signs the task_to_deny
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 200)

        # user2 denies task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'denied'},
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 200)

        # user1 receives a message that his task has been deleted
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user1_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text, 'Ihr Auftrag "deny_task" wurde von user2 abgelehnt.')

        # user2 cannot sign task
        response = self.client.put(ROUTES['tasks'] + '/1/status', json={'event': 'signed'},
                                   headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 400)

        # user1 cannot see task under owned tasks
        response = self.client.get(ROUTES['tasks'] + '?type=OWNED',
                                   headers=auth_header(user1_token))
        self.assertEqual(len(response.get_json()['tasks']), 0)
        response = self.client.get(ROUTES['tasks'] + '/1',
                                   headers=auth_header(user2_token))

        # task to be deleted does not exist
        response = self.client.delete(ROUTES['tasks'] + '/4',
                                      headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 403)

        # user1 can delete task
        response = self.client.delete(ROUTES['tasks'] + '/1',
                                      headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 200)

        # task does not exist
        response = self.client.get(ROUTES['tasks'] + '/1',
                                   headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 403)

        # user2 creates task successfully
        response = self.client.post(ROUTES['tasks'], json=task_data_to_withdraw,
                                    headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 201)

        # user1 cannot withdraw task
        response = self.client.delete(ROUTES['tasks'] + '/1',
                                      headers=auth_header(user1_token))
        self.assertEqual(response.status_code, 404)

        # user2 withdraws task
        response = self.client.delete(ROUTES['tasks'] + '/2',
                                      headers=auth_header(user2_token))
        self.assertEqual(response.status_code, 200)

        # user2 receives a message that his task has been withdrawn
        response = self.client.get(ROUTES['messages'],
                                   headers=auth_header(user2_token))
        message_text = response.get_json()['messages'][0]['text']
        self.assertEqual(message_text, 'Ihr Auftrag "withdraw_task" wurde zurückgezogen.')

        # user4 cannot sign task
        response = self.client.put(ROUTES['tasks'] + '/2/status', json={'event': 'signed'},
                                   headers=auth_header(user4_token))
        self.assertEqual(response.status_code, 403)
