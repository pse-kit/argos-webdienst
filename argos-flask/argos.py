"""This module creates the application instance."""

import os

# enable code coverage
COV = None
if os.environ.get('FLASK_COVERAGE'):  # pragma: no cover
    import coverage
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()

from app import create_app  # noqa: E402

# use config fron env variable or default configuration if not defined
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
