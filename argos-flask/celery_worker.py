"""This module contains the celery configuration."""

import os
from datetime import timedelta

from celery import Celery

from app import create_app
from app.synchronizer.celery_synchronizer import sync_with_celery


def create_celery(app):
    """Create a celery object with given config.

    Args:
        app (flask.Flask): Flask application object.

    Returns:
        celery: Celery application.

    """
    # create new celery object
    celery = Celery(app.import_name,
                    backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['BROKER_URL'])

    # update the rest of the Celery config from the Flask config
    celery.conf.update(app.config)

    # create subclass of the task that wraps the task execution in an application context
    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


flask_app = create_app(os.getenv('FLASK_CONFIG') or 'default')
celery = create_celery(flask_app)


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    """Add entry to beat schedule list.

    Args:
        sender (Sender): Celery sender.
    """
    sender.add_periodic_task(timedelta(minutes=15), sync_with_celery, name='Sync LDAP to db')
